package com.aquarium.engine;

import com.aquarium.engine.configuration.SmallCubeWorldFactory;
import com.aquarium.engine.configuration.XMLWorldFactory;
import com.aquarium.engine.handler.parameters.AverageAmountParameter;
import com.aquarium.entities.support.WorldHolder;
import com.aquarium.graph.DirectionPlotDiagram;
import com.aquarium.entities.*;
import com.aquarium.entities.Observer;
import com.aquarium.graph.ObserversPlotDiagram;
import com.aquarium.math.PointCan;
import com.aquarium.webapp.NAServer;
import com.aquarium.webapp.servlets.ReloadWorldServlet;
import com.google.common.collect.Sets;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.util.*;
import java.util.concurrent.*;


/**
 * should be faster
 */

public class IncrementalEngine {

    static final Logger log = LogManager.getLogger("IncrementalEngine");

    public static final int THREADS = 8;
    public static final float IMPULSE_MOVE_FACTOR = 0.1f;
    public static final int AMOUNT_OF_IMAGES = 100;
    static double WORLD_DEAD_LINE = 1000000d * 1000000d;

    World world;
    ExecutorService threads = Executors.newFixedThreadPool(THREADS);
    ExecutorService drawThreads = Executors.newFixedThreadPool(1);
    private static String initialConfig = null;

    IncrementalEngine(World w) {
        world = w;
    }

    static public void main(String[] args) throws Exception {
        NAServer server = new NAServer();
        try {
            server.init();
        } catch (Throwable e) {
            log.error("Main error web app init!!! ", e);
            return;
        }
        try {
            runMonitorableWorld();
        } catch (Throwable e) {
            log.error("Main error in runMonitorableWorld!!! ", e);
        }
        try {
            server.stop();
        } catch (Throwable e) {
            log.error("Main error in stop web app!!! ", e);
        }
    }

    public static void runMonitorableWorld() throws Exception {
        World world;
        do {
            try {
                world = initWorld();
            } catch (Exception e) {
                log.error("World initialization failed with new configuration: " + e.getMessage(), e);
                log.error("Started to fail over the old configuration");
                System.setProperty(ReloadWorldServlet.CONFIG_FILE, initialConfig);
                world = initWorld();
            }
            world.needToBeRestarted(false);
            WorldHolder.world = world;
            new IncrementalEngine(world).runWorld();
        } while (world.isNeedToBeRestarted());
    }

    public static World initWorld() {
        World world;
        if (System.getProperty(ReloadWorldServlet.CONFIG_FILE) != null) {
            if (initialConfig == null) {
                initialConfig = System.getProperty(ReloadWorldServlet.CONFIG_FILE);
            }
            world = new XMLWorldFactory(System.getProperty(ReloadWorldServlet.CONFIG_FILE)).createWorld();
        } else {
            world = new SmallCubeWorldFactory().createWorld();
        }
        return world;
    }

    private void runWorld() throws Exception {
        Iterable<Bit> bits = world.getBits();
        Collection<Set<Bit>> groups = divideBits(bits, THREADS);
        Monitor m = new Monitor();
        double lastDraw = 0;
        drawSurfacePicture(0);
        makeVideo();
        int kinoCount = 0;
//        drawObserversPicture();

        while (world.getTime() < WORLD_DEAD_LINE && !world.stopTheWorld()) {
            Set<Future> futures = new HashSet<Future>(THREADS);
            float dt = world.getBitSize() / (2 * m.getMaxVelocity());
//            log.info("Time: " + world.getTime() + " Delta time: " + m.dt + " impact bits: " + m.candidates[0].getId() + ", " + m.candidates[1].getId());
            log.info("Time: " + world.getTime() + " Delta time: " + dt);
            //all move

            log.info("Time: " + world.getTime() + " Energy: " + countEnergy(world.getBits()) + " dt: " + dt + " max velocity: " + m.getMaxVelocity() + " max velocity bit: " + m.getMaxVelocityBit());
            world.addTime(dt);
            world.bitsMovementStarted();
            m.maxVelocity = 0;
            for (Set<Bit> group : groups) {
                futures.add(freeMove(dt, group));
            }

            Iterator<Future> moversIt = futures.iterator();
            int moveCount = 0;
            while (moversIt.hasNext()) {
                Future future = moversIt.next();
                future.get();
                moveCount++;
                log.info("Time: " + world.getTime() + " move count: " + moveCount);
            }
            futures.clear();
            world.bitsMovementFinished();

            log.info("Time: " + world.getTime() + " bits in black box count BEFORE flush: " + world.getBlackBox().getBitsSize());

            for (Observer observer : world.getObservers()) {
                futures.add(handleObserver(observer));
            }

            for (Modificator modificator : world.getModificators()) {
                futures.add(handleModificator(dt, modificator));
            }

            int observersCount = 0;
            for (Future future : futures) {
                future.get();
                observersCount++;
                log.info("Time: " + world.getTime() + " observers / modificators count: " + observersCount);
            }
            futures.clear();

            world.getBlackBox().calculateStatistics();

            //flush observers
            world.flushObservers();


            log.info("Time: " + world.getTime() + " Energy after Transfomers: " + world.getBlackBox().getEnergy());
            log.info("Time: " + world.getTime() + " bits in black box count AFTER flush: " + world.getBlackBox().getBitsSize());


            //impact handle
            for (Set<Bit> group : groups) {
                futures.add(findImpactCandidates(group, m));
            }

            int impactsCount = 0;
            for (Future future : futures) {
                future.get();
                impactsCount++;
                log.info("Time: " + world.getTime() + " impact executor number : " + impactsCount);
            }
            log.info("Time: " + world.getTime() + " impacts count: " + m.amountChangedBits());

            futures.clear();

            m.clearChangedBits();
            log.info("Time: " + world.getTime() + " clearChangedBits was done");

            //move modificators
            for (Modificator modificator : world.getModificators()) {
                if (modificator.isMoveable()) {
                    Point impuls = modificator.getImpuls();
                    impuls = new Point(impuls.getX() * dt * IMPULSE_MOVE_FACTOR,
                            impuls.getY() * dt * IMPULSE_MOVE_FACTOR,
                            impuls.getZ() * dt * IMPULSE_MOVE_FACTOR);
                    Point newPosition = PointCan.increasePoint(modificator.getPosition(), impuls);
                    modificator.setPosition(newPosition);
                }
            }


            //correct world boundaries
            int outcount = 0;
            Iterable<Bit> outComers = world.getBitsFromOutside();
            Set<Bit> newOutCommers = new HashSet<Bit>((int) (world.getOutCommers().size() * 1.2));
            for (Bit bit : outComers) {
                world.returnBitBack(bit);
                m.setMaxVelocity(bit);
                newOutCommers.add(bit);
                outcount++;
            }
            world.setOutCommers(newOutCommers);
            log.info("Time: " + world.getTime() + " Out Count: " + outcount + ", Energy return bits back: " + world.getBlackBox().getEnergy());
            returnBitsFromBlackBox(world);

            //KINO
            if (world.getTime() - lastDraw > 100.0) {
                kinoCount = AMOUNT_OF_IMAGES;
                lastDraw = world.getTime();
            }

            if (kinoCount > 0) {
                kinoCount--;
                drawSurfacePicture(AMOUNT_OF_IMAGES - kinoCount);
                if (kinoCount == 0) {
                    makeVideo();
                }
            }
        }
    }

    private void drawSurfacePicture(final int count) {
        log.info("Start Picture Producer count=" + count);
        for (final Observer observer : world.getObservers()) {
            if (observer instanceof SurfaceBody) {
                SurfaceBody surface = (SurfaceBody) observer;

                final Set<PipeVector> vectors = new HashSet<PipeVector>(10000);
                for (Bit bit : world.getBits().findAcceptable(observer)) {
                    PipeVector vector = DirectionPlotDiagram.getPipeVectorForImage(bit, surface.getSurfaceOrientation(), world);
                    vectors.add(vector);
                }
                drawThreads.submit(new Runnable() {
                    @Override
                    public void run() {
                        DirectionPlotDiagram canvas = new DirectionPlotDiagram(count, world.getSide(), 100, vectors);
                        canvas.drawInFile();
                    }
                });
            }

        }
        log.info("Finish Picture Producer count=" + count);
    }

    private void makeVideo() {
        drawThreads.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    log.info("KINO KINO KINO - process started");
                    ProcessBuilder processBuilder = new ProcessBuilder("./videoinbatch.sh");
                    Process process = processBuilder.start();
                    PrintStream errorStream = log.getStream(Level.ERROR);
                    PrintStream infoStream = log.getStream(Level.INFO);

                    byte[] buf = new byte[32 * 256];

                    while (true) {
                        int r = process.getInputStream().read(buf);
                        if (r == -1) {
                            break;
                        }
                        infoStream.write(buf, 0, r);
                        int r2 = process.getErrorStream().read(buf);
                        if (r2 == -1) {
                            break;
                        }
                        errorStream.write(buf, 0, r2);
                    }

                } catch (Throwable e) {
                    log.error("Creation video ", e);
                }
                log.info("KINO KINO KINO - f-i-n-i-s-h");
            }
        });
    }

    private void drawObserversPicture() {
        drawThreads.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    ObserversPlotDiagram canvas = new ObserversPlotDiagram(
                            new Point(0, 0, 1), world, 100, world.getObservers());
                    canvas.drawInFile(AverageAmountParameter.class.getSimpleName());
                } catch (Exception e) {
                    log.error("drawObserversPicture throw Exception", e);
                }
            }
        });
    }

    private Future handleModificator(final float dt, final Modificator next) {
        Future result = threads.submit(new Callable() {
            @Override
            public Void call() throws Exception {
                handleModificatorInternal(dt, next);
                return null;
            }
        });
        return result;
    }

    private void handleModificatorInternal(float dt, Modificator next) {
        Iterable<Bit> potential = world.getBits().findAcceptable(next);
        next.observe(dt, world, potential);
    }

    private Future handleObserver(final Observer next) {
        Future<Void> result = threads.submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                return handleObserverInternal(next);
            }
        });
        return result;
    }

    private Void handleObserverInternal(Observer next) {
        Iterable<Bit> potential = world.getBits().findAcceptable(next);
        next.observe(potential);
        return null;
    }

    private void returnBitsFromBlackBox(World world) {
        Bit bit = world.getBlackBox().pullBit();
        while (bit != null) {
            moveBitToWorldBorder(bit);
            bit = world.getBlackBox().pullBit();
        }
    }

    private void moveBitToWorldBorder(Bit bit) {
        double dice = Math.random();

        if (dice >= 0 && dice < 1.0 / 6.0) {
            bit.getPipe().setOffset(new Point(world.getSide() / 2, ((float) Math.random() - .5f) * world.getSide(), ((float) Math.random() - .5f) * world.getSide()));
        } else if (dice >= 1.0 / 6.0 && dice < 2.0 / 6.0) {
            bit.getPipe().setOffset(new Point(-world.getSide() / 2, ((float) Math.random() - .5f) * world.getSide(), ((float) Math.random() - .5f) * world.getSide()));
        } else if (dice >= 2.0 / 6.0 && dice < 3.0 / 6.0) {
            bit.getPipe().setOffset(new Point(((float) Math.random() - .5f) * world.getSide(), world.getSide() / 2, ((float) Math.random() - .5f) * world.getSide()));
        } else if (dice >= 3.0 / 6.0 && dice < 4.0 / 6.0) {
            bit.getPipe().setOffset(new Point(((float) Math.random() - .5f) * world.getSide(), -world.getSide() / 2, ((float) Math.random() - .5f) * world.getSide()));
        } else if (dice >= 4.0 / 6.0 && dice < 5.0 / 6.0) {
            bit.getPipe().setOffset(new Point(((float) Math.random() - .5f) * world.getSide(), ((float) Math.random() - .5f) * world.getSide(), world.getSide() / 2));
        } else if (dice >= 5.0 / 6.0 && dice < 1) {
            bit.getPipe().setOffset(new Point(((float) Math.random() - .5f) * world.getSide(), ((float) Math.random() - .5f) * world.getSide(), -world.getSide() / 2));
        }
    }

    private Future freeMove(final double dt, final Set<Bit> group) {
        Future<BitsIndex> result = threads.submit(new Callable<BitsIndex>() {
            @Override
            public BitsIndex call() throws Exception {
                return freeMoveInternal(group, dt);
            }
        });
        return result;
    }

    private BitsIndex freeMoveInternal(Set<Bit> group, double dt) {
        return world.freeMove(dt, group);

    }

    private double countEnergy(Iterable<Bit> bits) {
        double energy = 0;
        for (Bit bit : bits) {
            energy = energy + bit.getVelocity();
        }
        return energy;
    }

    private Bit findUnchangedBits(Monitor m, Iterable<Bit> limitedSet) {
        Iterator<Bit> closeBitsIterator = limitedSet.iterator();
        Bit bit2 = closeBitsIterator.next();
        while (m.isBitChanged(bit2)) {
            if (closeBitsIterator.hasNext()) {
                bit2 = closeBitsIterator.next();
            } else {
                bit2 = null;
                break;
            }
        }
        return bit2;
    }

    private Collection<Set<Bit>> divideBits(Iterable<Bit> bits, int threads) {
        log.info("divideBits started");
        List<Set<Bit>> groups = new ArrayList<Set<Bit>>(threads);
        for (int i = 0; i < threads; i++) {
            groups.add(new HashSet<Bit>());
        }
        int i = 0;
        for (Bit bit : bits) {
            Set<Bit> group = groups.get(i % threads);
            group.add(bit);
            i++;
        }
        log.info("divideBits finished");
        return groups;
    }

    private Future<Monitor> findImpactCandidates(final Set<Bit> group, final Monitor m) {
        Future<Monitor> result = threads.submit(new Callable<Monitor>() {
            @Override
            public Monitor call() throws Exception {
                return findCandidatesInternal(group, m);
            }
        });
        return result;
    }

    private Monitor findCandidatesInternal(Set<Bit> group, Monitor m) {
        for (Bit bit1 : group) {
            Iterable<Bit> limitedSet = getRandomBits(bit1);
            if (limitedSet.iterator().hasNext()) {
                if (!m.isBitChanged(bit1)) {
                    Bit bit2 = findUnchangedBits(m, limitedSet);
                    if (bit2 != null) {
                        if (world.getImpactStrategy().isEligableForImpact(bit1, bit2)) {
                            world.getImpactStrategy().doImpact(bit1, bit2);
                            m.setMaxVelocity(bit1, bit2);
                            m.addChangedBit(bit1);
                            m.addChangedBit(bit2);
                        }
                    }
                }
            }
        }
        return m;
    }

    private Iterable<Bit> getRandomBits(Bit bit1) {
        Iterable<Bit> randomList = world.getBits().getCloseBits(bit1, world.getBitSize(), 100);
        if (randomList instanceof ArrayList) {
            final List<Bit> list = (ArrayList<Bit>) randomList;
            final int size = list.size();
            if(list.isEmpty()){
                return list;
            }
            randomList = new Iterable<Bit>() {
                int random = (int) Math.floor(Math.random() * size);

                @Override
                public Iterator<Bit> iterator() {
                    return new Iterator<Bit>() {
                        int cursor = random;
                        int count = 0;

                        @Override
                        public boolean hasNext() {
                            return count < list.size();
                        }

                        @Override
                        public Bit next() {
                            if(!hasNext()) {
                                throw new NoSuchElementException();
                            }
                            Bit bit = list.get(cursor);
                            cursor++;
                            count++;
                            if(cursor >= list.size()){
                                cursor = 0;
                            }
                            return bit;
                        }

                        @Override
                        public void remove() {
                            throw new UnsupportedOperationException();
                        }
                    };
                }
            };
        }
        return randomList;
    }

    class Monitor {
        private float maxVelocity = 0;
        private Bit maxVelocityBit = null;
        private Set<Bit> changeBits = Sets.newSetFromMap(new ConcurrentHashMap<Bit, Boolean>());

        Monitor() {
            this.maxVelocity = 1;
        }

        synchronized void setMaxVelocity(Bit v1, Bit v2) {
            Bit localMaxVelBit = null;
            if (v1.getVelocity() > v2.getVelocity()) {
                localMaxVelBit = v1;
            } else {
                localMaxVelBit = v2;
            }
            if (localMaxVelBit.getVelocity() > maxVelocity) {
                maxVelocityBit = localMaxVelBit;
                maxVelocity = localMaxVelBit.getVelocity();
            }
            //maxVelocity = Math.max(Math.max(v1.getVelocity(), v2.getVelocity()), maxVelocity);
        }

        synchronized void setMaxVelocity(Bit v1) {
            Bit localMaxVelBit = v1;
            if (localMaxVelBit.getVelocity() > maxVelocity) {
                maxVelocityBit = localMaxVelBit;
                maxVelocity = localMaxVelBit.getVelocity();
            }
            //maxVelocity = Math.max(Math.max(v1.getVelocity(), v2.getVelocity()), maxVelocity);
        }

        boolean isBitChanged(Bit bit) {
            return changeBits.contains(bit);
        }

        void addChangedBit(Bit bit) {
            changeBits.add(bit);
        }

        int amountChangedBits() {
            return changeBits.size();
        }

        synchronized public float getMaxVelocity() {
            return maxVelocity;
        }

        synchronized public Bit getMaxVelocityBit() {
            return maxVelocityBit;
        }

        synchronized public void clearChangedBits() {
            changeBits.clear();
        }
    }
}

