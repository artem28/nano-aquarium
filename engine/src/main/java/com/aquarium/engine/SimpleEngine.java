package com.aquarium.engine;

import com.aquarium.engine.configuration.SmallCubeWorldFactory;
import com.aquarium.entities.*;
import com.aquarium.math.PipesCan;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


/**
 * too slow!!!
 */
public class SimpleEngine {

    static final Logger log = LogManager.getLogger("SimpleEngine");

    public static final int THREADS = 8;
    static double WORLD_DEAD_LINE = 1000000d * 1000000d;
    World world;
    ExecutorService threads = Executors.newFixedThreadPool(THREADS);

    public SimpleEngine(World w) {
        world = w;
    }

    static public void main(String[] args) throws Exception {
        new SimpleEngine(new SmallCubeWorldFactory().createWorld()).runWorld();
    }

    private void runWorld() throws Exception {
        Iterable<Bit> bits = world.getBits();
        Collection<Set<Bit>> groups = divideBits(bits, THREADS);

        while (world.getTime() < WORLD_DEAD_LINE) {
            log.info("Time: " + world.getTime() + "runWorld new step");
            Set<Future> futures = new HashSet<Future>(THREADS);
            //find minimal dt
            for (Set<Bit> group : groups) {
                futures.add(findCandidates(group));
//              decreasingBits.remove(bit1);
            }

            Monitor m = new Monitor();
            for (Future<Monitor> future : futures) {
                Monitor monitor = future.get();
                if (m.dt > monitor.dt) {
                    m = monitor;
                }
            }
            log.info("Time: " + world.getTime() + " Delta time: " + m.dt + " impact bits: " + m.candidates[0].getId() + ", " + m.candidates[1].getId());
            log.info("Time: " + world.getTime() + " Energy: " + countEnergy(world.getBits()));
            //all move
            world.addTime(m.dt);
            Iterable<Bit> newBits = world.freeMove(m.dt, world.getBits());
            world.replaceBits(newBits);
            log.info("Time: " + world.getTime() + " Energy after Transfomers: " + world.getBlackBox().getEnergy());

            //flush observers
            world.flushObservers();

            //impact handle
            world.getImpactStrategy().doImpact(m.candidates[0], m.candidates[1]);

            //correct world boundaries
            int outcount = 0;
            for (Bit bit : bits) {
                if (!world.isInside(bit.getPipe().getOffset())) {
                    world.returnBitBack(bit);
                    outcount++;
                }
            }
            log.info("Time: " + world.getTime() + " Out Count: " + outcount + ", Energy return bits back: " + world.getBlackBox().getEnergy());

        }
    }

    private double countEnergy(Iterable<Bit> bits) {
        double energy = 0;
        for (Bit bit : bits) {
            energy = energy + bit.getVelocity();
        }
        return energy;
    }


    private Collection<Set<Bit>> divideBits(Iterable<Bit> bits, int threads) {
        List<Set<Bit>> groups = new ArrayList<Set<Bit>>(threads);
        for (int i = 0; i < threads; i++) {
            groups.add(new HashSet<Bit>());
        }
        int i = 0;
        for (Bit bit : bits) {
            Set<Bit> group = groups.get(i % threads);
            group.add(bit);
            i++;
        }
        return groups;
    }


    private Future<Monitor> findCandidates(final Set<Bit> group) {
        Future<Monitor> result = threads.submit(new Callable<Monitor>() {
            @Override
            public Monitor call() throws Exception {
                return findCandidatesInternal(group);
            }
        });
        return result;
    }

    private Monitor findCandidatesInternal(Set<Bit> group) {
        Monitor m = new Monitor();
        for (Bit bit1 : group) {
            for (Bit bit2 : world.getBits()) {
                if (bit1.getId() != bit2.getId() && world.getImpactStrategy().isEligableForImpact(bit1, bit2)) {
                    float t = PipesCan.getTimeClosestDistance(bit1.getPipe(), bit1.getVelocity(), bit2.getPipe(), bit2.getVelocity());
                    if (t > 0 && t < m.dt) {
                        float d = PipesCan.getDistance(bit1.getPipe(), bit1.getVelocity(), bit2.getPipe(), bit2.getVelocity(), t);
                        if (d < world.getBitSize()) {
                            if (t < m.dt) {
                                m.dt = t;
                                m.candidates[0] = bit1;
                                m.candidates[1] = bit2;
                            }
                        }
                    }
                }
            }
        }
        return m;
    }

    class Monitor {
        float dt = Float.MAX_VALUE;
        Bit[] candidates = new Bit[2];
    }
}

