package com.aquarium.engine;

import com.aquarium.engine.impact.SimpleImpactStrategy;
import com.aquarium.entities.Bit;
import com.aquarium.world.CubeWorld;
import com.aquarium.entities.World;
import com.aquarium.math.BitsCan;
import com.aquarium.math.DoubleCan;

import java.util.Set;
import java.util.logging.Logger;


/**
 * WRONG!!!
 */
public class SimpleEngine2 {

    static final Logger log = Logger.getLogger("SimpleEngine");

    private static final int MAXDEPTH = 5;
    static float SIZE = 10;
    static float BITSIZE = 0.1f;
    static int BITSAMOUNT = 1000;
    static float WORLD_ENERGY = BITSAMOUNT * 1;
    static float WORLD_DEAD_LINE = 1000000f * 1000000f;
    World world;


    SimpleEngine2(World w) {
        world = w;
    }

    static public void main(String[] args) {
        World world = new CubeWorld(SIZE, BITSIZE);
        world.setImpactStrategy(new SimpleImpactStrategy(BITSIZE));
        new SimpleEngine2(world).runWorld();
    }

    private void runWorld() {
        float time = 0;
        world.createBits(BITSAMOUNT, WORLD_ENERGY);
        Iterable<Bit> bits = world.getBits();
        float dt = getMinimalDeltaT(bits, BITSIZE);
        int maxDepth = 1;

        log.info("MinimalDeltaT = " + dt);

        while (time < WORLD_DEAD_LINE) {
            //find minimal dt
            float nextTimePoint = time + dt;
            for (Bit bit1 : bits) {

                int depth = findImpactFor(bits, bit1, nextTimePoint, 0);
                maxDepth = maxDepth > depth ? maxDepth : depth;

            }

            if (maxDepth > MAXDEPTH) {
                dt = dt * 0.7f;
            } else {
                dt = dt * 1.1f;
            }
            log.info("Time:" + time + " time delta corrected:" + dt + " maxDepth was=" + maxDepth);
            time = nextTimePoint;
        }
    }

    private float getMinimalDeltaT(Iterable<Bit> bits, float bitsize) {
        float dt = 0;
        for (Bit bit : bits) {
            float t = bitsize / bit.getVelocity();
            dt = dt > t ? dt : t;
        }
        if (dt == 0) {
            throw new RuntimeException("Can not find minimal time delta.");
        }
        return dt * 0.000001f;
    }

    private int findImpactFor(Iterable<Bit> allBits, Bit bit, float pointOfTime, int depth) {
        log.info("findImpactFor call with depth: " + depth);
        boolean impact = false;
        if (DoubleCan.areEqual(bit.getTime(), pointOfTime) || bit.getTime() > pointOfTime) {
            return depth;
        }
        for (Bit bit2 : allBits) {
            if (bit != bit2) {
                float t = BitsCan.getTimeClosestDistance(bit, bit2);
                if (t < pointOfTime) {
                    double d = BitsCan.getDistance(bit, bit2, t);
                    if (d < BITSIZE) {
                        bit.move(t);
                        if (!world.isInside(bit.getPipe().getOffset())) {
                            world.returnBitBack(bit);
                            break;
                        }
                        bit2.move(t);
                        if (!world.isInside(bit2.getPipe().getOffset())) {
                            world.returnBitBack(bit2);
                            break;
                        }

                        world.getImpactStrategy().doImpact(bit, bit2);
                        int depth1 = findImpactFor(allBits, bit, pointOfTime, depth + 1);
                        int depth2 = findImpactFor(allBits, bit2, pointOfTime, depth + 1);
                        depth = Math.max(depth1, depth2);
                        impact = true;
                        break;

                    }
                }
            }
        }
        if (!impact && pointOfTime > bit.getTime()) {
            bit.move(pointOfTime - bit.getTime());
            if (!world.isInside(bit.getPipe().getOffset())) {
                world.returnBitBack(bit);
            }
        }
        return depth;

    }

}

