package com.aquarium.engine.active;

import com.aquarium.engine.handler.parameters.ImpulseParameter;
import com.aquarium.entities.*;
import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.entities.support.ParameterValue;
import com.aquarium.entities.support.StatisticsAccumulator;
import com.aquarium.math.PipesCan;
import com.aquarium.math.PointCan;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * to add or remove bits in the world
 */
public class BitsTrap implements Modificator {

    private static final double TRAPOVERLOAD = 0.1;
    private final boolean withdraw;
    private final World world;
    private Point position;
    private float radius = 0;
    private int id = 0;
    private ObservableParameter param;
    private StatisticsAccumulator dataSink;
    private double otime = 0;
    static final Logger log = LogManager.getLogger("IncrementalEngine");
    private Set<Bit> previousBits = new HashSet<Bit>();
    private Set<Bit> currentBits = new HashSet<Bit>();
    private volatile int impactCount = 0;
    private boolean moveable = false;
    private Point impuls = new Point();
    private boolean rotation;


    public BitsTrap(int id, Point position, float size, boolean withdraw,
                    World world, StatisticsAccumulator dataSink, boolean moveable, boolean rot) {
        this.position = position;
        this.moveable = moveable;
        this.id = id;
        this.radius = size / 2;
        this.withdraw = withdraw;
        if (withdraw) {
            param = new WithdrawAttractionParameter();
        } else {
            param = new ImpulseParameter();
        }
        this.world = world;
        this.dataSink = dataSink;
        this.rotation = rot;
    }

    @Override
    public void observe(float dt, World world, Iterable<Bit> bits) {
        int count = 0;
        impactCount = 0;
        impuls = new Point();
        for (Bit bit : bits) {
            if (withdraw ) {
                impactCount++;
                PointCan.increasePoint(impuls, getTrapAttraction(bit.getPipe(), bit.getVelocity()));
                param.consider(bit);
                world.getBlackBox().addBit(bit);
            } else {
                if (!previousBits.contains(bit)) {
                    impactCount++;
                    PointCan.increasePoint(impuls, getTrapImpact(bit.getPipe(), bit.getVelocity()));
                    param.consider(bit);
                }
                currentBits.add(bit);
            }
            count++;
            if (count > 1000000) {
                throw new RuntimeException("infinite loop in bits iterator:" + bits);
            }
        }
        if (withdraw) {
            log.info("Negative trap has {} impacts", impactCount);
        }
    }

    /**
     * not multithread safe method
     */
    @Override
    public void flush(float time) {
        synchronized (this) {
            if (otime != time) {
                dataSink.saveParameter(id, time, param);
                otime = time;
            }
        }
    }

    public void postAction() {
        if (!withdraw) {

            int movedCount = moveBitsOut();
            log.info("Positive trap has caught {} bits", movedCount);
            log.info("Positive trap has {} impacts", impactCount);
            previousBits = currentBits;
            currentBits = new HashSet<Bit>();
        }
    }

    public int moveBitsOut() {
        int count = 0;
        int requiredAmount = world.getBlackBox().getAverageTrapBandwidth();
        if (requiredAmount == 0) {
            return count;
        }
        //get bits from black box
        Bit bit = world.getBlackBox().pullBit();
        while (bit != null) {
            randomizeBit(bit);
            currentBits.add(bit);
            count++;
            if (count >= requiredAmount) {
                return count;
            }
            bit = world.getBlackBox().pullBit();
        }

        //get bits from outside

        Iterator<Bit> it = new OutsideWorldIterator();

        while (it.hasNext()) {
            bit = it.next();
            randomizeBit(bit);
            currentBits.add(bit);
            count++;
            if (count >= requiredAmount) {
                return count;
            }
        }

        return count;
    }

    private Iterator<Bit>[] getBitsFrom6Sides() {
        Iterator<Bit>[] bitsFrom6Sides = new Iterator[6];
        float side2 = world.getSide() / 2;

        bitsFrom6Sides[0] = world.getBits().getBitSetInX(-Float.MAX_VALUE, -side2).iterator();
        bitsFrom6Sides[1] = world.getBits().getBitSetInX(side2, Float.MAX_VALUE).iterator();
        bitsFrom6Sides[2] = world.getBits().getBitSetInY(-Float.MAX_VALUE, -side2).iterator();
        bitsFrom6Sides[3] = world.getBits().getBitSetInY(side2, Float.MAX_VALUE).iterator();
        bitsFrom6Sides[4] = world.getBits().getBitSetInZ(-Float.MAX_VALUE, -side2).iterator();
        bitsFrom6Sides[5] = world.getBits().getBitSetInZ(side2, Float.MAX_VALUE).iterator();
        return bitsFrom6Sides;
    }

    @Override
    public Point getImpuls() {
        return impuls;
    }

    @Override
    public boolean isMoveable() {
        return moveable;
    }

    @Override
    public void setPosition(Point point) {
        position = point;
        log.info("Time: " + world.getTime() + " modificator " + id + " moved to position: " + point);
    }

    protected void randomizeBit(Bit bit) {
        bit.getPipe().setOffset(PointCan.randomInSphere(position, radius / 5));
        bit.getPipe().setFactorXYZ(bit.getPipe().getOffset().getX() - position.getX(),
                bit.getPipe().getOffset().getY() - position.getY(),
                bit.getPipe().getOffset().getZ() - position.getZ());
        if (rotation) {
            bit.getPipe().setFactorXYZ(-bit.getPipe().getFactorY(),
                    bit.getPipe().getFactorX(),
                    bit.getPipe().getFactorZ());
        }
    }

    @Override
    public boolean isAcceptable(Bit bit) {
        return PointCan.getDistance(bit.getPipe().getOffset(), position) < radius;
    }

    @Override
    public Point getPosition() {
        return position;
    }

    @Override
    public float getSize() {
        return radius * 2;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof BitsTrap) {
            BitsTrap bitsTrap = (BitsTrap) o;
            if (bitsTrap.withdraw == withdraw) {
                return id == bitsTrap.id ? 0 : 1;
            } else if (withdraw && !bitsTrap.withdraw) {
                return 1;
            }
            return 1;
        }
        return -1;
    }

    private Point getTrapImpact(PipeVector vector, float velocity) {
        Point currentVelocity = PipesCan.getVelocity(vector, velocity);

        Point requiredDirection = new Point(position.getX() - vector.getOffset().getX(),
                position.getY() - vector.getOffset().getY(),
                position.getZ() - vector.getOffset().getZ());

        float projection = (currentVelocity.getX() * requiredDirection.getX()
                + currentVelocity.getY() * requiredDirection.getY()
                + currentVelocity.getZ() * requiredDirection.getZ())
                / (float)Math.sqrt(Math.pow(requiredDirection.getX(), 2)
                + (float)Math.pow(requiredDirection.getY(), 2)
                + (float)Math.pow(requiredDirection.getZ(), 2));
        if (projection < 0) {
            projection = 0;
        }

        return PipesCan.normalize(requiredDirection, projection);
    }

    private Point getTrapAttraction(PipeVector vector, float velocity) {
        Point currentVelocity = PipesCan.getVelocity(vector, velocity);

        Point requiredDirection = new Point(position.getX() - vector.getOffset().getX(),
                position.getY() - vector.getOffset().getY(),
                position.getZ() - vector.getOffset().getZ());

        float k = PipesCan.getVelocity(currentVelocity) / PipesCan.getVelocity(requiredDirection);

        return new Point(
                k * requiredDirection.getX() - currentVelocity.getX(),
                k * requiredDirection.getY() - currentVelocity.getY(),
                k * requiredDirection.getZ() - currentVelocity.getZ()
        );
    }

    public boolean isWithdraw() {
        return withdraw;
    }

    class WithdrawAttractionParameter extends ImpulseParameter {
        @Override
        public synchronized void consider(Bit bit) {
            PointCan.increasePoint(impulses, getTrapAttraction(bit.getPipe(), bit.getVelocity()));
        }

        @Override
        public Point getPrevious() {
            return impulses;
        }

        @Override
        public String toString() {
            return "WithdrawAttractionParameter{" +
                    "impulses=" + impulses +
                    '}';
        }
    }

    public void setMoveable(boolean moveable) {
        this.moveable = moveable;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public String toString() {
        return "BitsTrap{" +
                "withdraw=" + withdraw +
                ", position=" + position +
                ", radius=" + radius +
                ", id=" + id +
                ", param=" + param +
                ", moveable=" + moveable +
                '}';
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public List<String> getParametersNames() {
        List<String> names = new ArrayList<String>(1);
        names.add(param.getName());
        return names;
    }

    @Override
    public List<ParameterValue> getParameterValues(String name) {
        return dataSink.getParameterValues(id, name);
    }

    class OutsideWorldIterator implements Iterator<Bit> {
        private int sequence = 0;
        private Iterator<Bit>[] outside;

        OutsideWorldIterator() {
            outside = getBitsFrom6Sides();
        }

        @Override
        public boolean hasNext() {
            int count = 0;
            while (!outside[sequence].hasNext()) {
                sequence = getNextSequence();
                count++;
                if (count > 6) {
                    return false;
                }
            }
            return outside[sequence].hasNext();
        }

        private int getNextSequence() {
            return sequence == 5 ? 0 : sequence + 1;
        }

        @Override
        public Bit next() {
            Bit bit = outside[sequence].next();
            sequence = getNextSequence();
            return bit;
        }

        @Override
        public void remove() {
            throw new RuntimeException("Not supported");
        }
    }
}

