package com.aquarium.engine.active;

import com.aquarium.entities.Bit;
import com.aquarium.entities.PipeVector;
import com.aquarium.entities.Point;
import com.aquarium.entities.World;
import com.aquarium.entities.support.StatisticsAccumulator;
import com.aquarium.math.PointCan;

/**
 * Created by artem on 10/30/14.
 */
public class EllipseBitsTrap extends BitsTrap {
    PipeVector ellipsOrientation;
    float ellipseLength;


    public EllipseBitsTrap(int id, Point position,
                           float radius, boolean withdraw, World world,
                           StatisticsAccumulator dataSink, boolean moveable,
                           PipeVector ellipsOrientation, float ellipseLength) {
        super(id, position, radius, withdraw, world, dataSink, moveable, false);
        this.ellipsOrientation = ellipsOrientation;
        this.ellipseLength = ellipseLength;
    }

    @Override
    protected void randomizeBit(Bit bit) {
        Point localPosition = getRandomCenter();
        bit.getPipe().setOffset(PointCan.randomInSphere(localPosition, getSize() / 3));
        bit.getPipe().setFactorXYZ(bit.getPipe().getOffset().getX() - localPosition.getX(),
                bit.getPipe().getOffset().getY() - localPosition.getY(),
                bit.getPipe().getOffset().getZ() - localPosition.getZ());
    }

    private Point getRandomCenter() {
        float sign = (float)Math.random() > 0.5f ? 1 : -1;
        return new Point(getPosition().getX() + sign * ellipsOrientation.getFactorX() * ellipseLength / 2.0f,
                getPosition().getY() + sign * ellipsOrientation.getFactorY() * ellipseLength / 2.0f,
                getPosition().getZ() + sign * ellipsOrientation.getFactorZ() * ellipseLength / 2.0f);
    }

    @Override
    public String toString() {
        return "EllipseBitsTrap{" + super.toString()
                + ", ellipsOrientation=" + ellipsOrientation
                + ", ellipseLength=" + ellipseLength
                + "}";
    }
}
