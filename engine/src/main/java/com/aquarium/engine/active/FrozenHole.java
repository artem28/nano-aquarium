package com.aquarium.engine.active;

import com.aquarium.engine.handler.parameters.ImpulseParameter;
import com.aquarium.entities.*;
import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.entities.support.ParameterValue;
import com.aquarium.entities.support.StatisticsAccumulator;
import com.aquarium.math.PipesCan;
import com.aquarium.math.PointCan;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by artem on 5/12/14.
 */
public class FrozenHole implements Modificator {

    public static final float BIT_VELOCITY_THRESHOLD = 0.01f;
    private float frozenFactor = 1;
    private Point position;
    private float radius = 0;
    private int id = 0;
    private StatisticsAccumulator dataSink;
    private float otime = 0;
    private Set<Bit> currentBits = new HashSet();
    private Set<Bit> previouseBits = new HashSet();
    ObservableParameter param = new ImpulseParameter();
    ObservableParameter param2 = new ImpulseParameter() {
        @Override
        public String getName() {
            return "impuls2";
        }
    };

    public FrozenHole(int id, float radius, Point p, float frozenFactor, StatisticsAccumulator dataSink) {
        this.id = id;
        this.frozenFactor = frozenFactor;
        this.radius = radius;
        position = p;
        this.dataSink = dataSink;
    }

    @Override
    public void observe(float dt, World world, Iterable<Bit> bits) {
        float energy = 0;
        for (Bit bit : bits) {
            float minEnergy = BIT_VELOCITY_THRESHOLD * world.getAverageBitEnergy();
            if (bit.getVelocity() > minEnergy) {
                float distance = PointCan.getDistance(this.getPosition(), bit.getPipe().getOffset());
                float lostEnergy = bit.getVelocity() * frozenFactor * dt;
                lostEnergy = (bit.getVelocity() - lostEnergy) > minEnergy ? lostEnergy : (bit.getVelocity() - minEnergy);
                lostEnergy = lostEnergy * (float)Math.pow(1 - (distance ) / (radius), 5);
                bit.setVelocity(bit.getVelocity() - lostEnergy);
                energy = energy - lostEnergy;
            }
            currentBits.add(bit);
        }
        world.getBlackBox().addEnergy(energy);
    }

    @Override
    public boolean isAcceptable(Bit bit) {
        return PointCan.getDistance(bit.getPipe().getOffset(), position) < radius;
    }

    public Point getPosition() {
        return position;
    }

    public float getSize() {
        return radius * 2;
    }

    @Override
    public void flush(float time) {
        synchronized (this) {
            if (otime != time) {
                dataSink.saveParameter((int) id, time, param);
                dataSink.saveParameter((int) id, time, param2);
                otime = time;
            }
        }
    }

    @Override
    public int compareTo(Object o) {
        Body body = (Body) o;
        return body.getId() == id ? 0 : -1;
    }

    @Override
    public void postAction() {
        for (Bit bit : currentBits) {
            if (!previouseBits.contains(bit)) {
                param.consider(bit);
                param2.consider(bit);
            }
        }
        for (Bit bit : previouseBits) {
            if (!currentBits.contains(bit)) {
                param2.consider(negative(bit));
            }
        }
        previouseBits = currentBits;
        currentBits = new HashSet<Bit>(previouseBits.size() + 10);
    }

    private Bit negative(Bit bit) {
        return new NegativeBit(bit);
    }

    @Override
    public Point getImpuls() {
        return null;
    }

    @Override
    public boolean isMoveable() {
        return false;
    }

    @Override
    public void setPosition(Point point) {
        //unsupported
    }

    @Override
    public String toString() {
        return "FrozenHole{" +
                "frozenFactor=" + frozenFactor +
                ", position=" + position +
                ", radius=" + radius +
                ", id=" + id +
                ", param=" + param +
                ", param2=" + param2 +
                '}';
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public List<String> getParametersNames() {
        List<String> names = new ArrayList<String>(1);
        names.add(param.getName());
        names.add(param2.getName());
        return names;
    }

    @Override
    public List<ParameterValue> getParameterValues(String name) {
        return dataSink.getParameterValues(id, name);
    }

    class NegativeBit extends Bit {
        Bit b;

        public NegativeBit(Bit bit) {
            super(bit.getId());
            b = bit;
        }

        @Override
        public PipeVector getPipe() {
            return new PipeVector(b.getPipe().getOffset(),
                    -b.getPipe().getFactorX(),
                    -b.getPipe().getFactorY(),
                    -b.getPipe().getFactorZ());
        }

        @Override
        public float getVelocity() {
            return b.getVelocity();
        }
    }

}
