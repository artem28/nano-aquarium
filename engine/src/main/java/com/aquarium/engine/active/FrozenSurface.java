package com.aquarium.engine.active;

import com.aquarium.entities.*;
import com.aquarium.entities.support.ParameterValue;
import com.aquarium.math.DoubleCan;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;

/**
 * Created by artem on 8/13/14.
 */
public class FrozenSurface implements Modificator, SurfaceBody {

    public static final float ALMOST100 = 0.99f;
    static private final Logger log = LogManager.getLogger("IncrementalEngine");
    private final float frozenFactor;
    private float size;
    private PipeVector position;
    private boolean frozen;
    private int id ;

    public FrozenSurface(int id, PipeVector pos, float frozenFactor, float size, boolean frozen) {
        this.size = size / 2.0f;
        position = pos;
        this.frozenFactor = frozenFactor;
        this.frozen = frozen;
        this.id = id;
    }

    @Override
    public PipeVector getSurfaceOrientation() {
        return position;
    }

    @Override
    public void observe(float dt, World world, Iterable<Bit> bits) {
        float energy = 0;
        int count = 0;
        for (Bit bit : bits) {
            if (frozen) {
                if(bit.getVelocity() > FrozenHole.BIT_VELOCITY_THRESHOLD *world.getAverageBitEnergy()) {
                    float changeEnergy = bit.getVelocity() * frozenFactor * dt;
                    changeEnergy = changeEnergy > bit.getVelocity() ? bit.getVelocity() * ALMOST100 : changeEnergy;
                    bit.setVelocity(bit.getVelocity() - changeEnergy);
                    energy = energy - changeEnergy;
                }
            } else {
                float changeEnergy = bit.getVelocity() * frozenFactor * dt;
                if (bit.getVelocity() > world.getAverageBitEnergy()) {
                    changeEnergy = world.getAverageBitEnergy() * frozenFactor * dt;
                    changeEnergy = changeEnergy > world.getAverageBitEnergy() ? world.getAverageBitEnergy() * ALMOST100 : changeEnergy;
                } else {
                    changeEnergy = changeEnergy > bit.getVelocity() ? bit.getVelocity() * ALMOST100 : changeEnergy;
                }
                bit.setVelocity(bit.getVelocity() + changeEnergy);
                energy = energy + changeEnergy;
            }
            count++;
        }
        world.getBlackBox().addEnergy(energy);
        log.info("{} surface observe call, amount of bits={}, lost energy = {}", new Object[]{frozen ? "Frozen" : "Warm", count, energy});
    }

    @Override
    public void flush(float time) {

    }

    @Override
    public void postAction() {

    }

    @Override
    public Point getImpuls() {
        return null;
    }

    @Override
    public boolean isMoveable() {
        return false;
    }

    @Override
    public void setPosition(Point point) {
        //unsupported
    }

    @Override
    public boolean isAcceptable(Bit bit) {
        Point pointPos = bit.getPipe().getOffset();
        if (DoubleCan.areEqual(position.getFactorX(), 0) && DoubleCan.areEqual(position.getFactorY(), 0)) {
            return pointPos.getZ() < (position.getOffset().getZ() + size) && pointPos.getZ() > (position.getOffset().getZ() - size);
        } else if (DoubleCan.areEqual(position.getFactorY(), 0) && DoubleCan.areEqual(position.getFactorZ(), 0)) {
            return pointPos.getX() < (position.getOffset().getX() + size) && pointPos.getX() > (position.getOffset().getX() - size);
        } else if (DoubleCan.areEqual(position.getFactorX(), 0) && DoubleCan.areEqual(position.getFactorZ(), 0)) {
            return pointPos.getY() < (position.getOffset().getY() + size) && pointPos.getY() > (position.getOffset().getY() - size);
        }
        throw new RuntimeException("Frozen surface has wrong configuration");
    }

    @Override
    public Point getPosition() {
        return position.getOffset();
    }

    @Override
    public float getSize() {
        return size * 2;
    }

    @Override
    public int compareTo(Object o) {
        return frozen ? 1 : -1;
    }

    @Override
    public String toString() {
        return "FrozenSurface{" +
                "id=" + id +
                ", frozenFactor=" + frozenFactor +
                ", size=" + size +
                ", position=" + position +
                ", frozen=" + frozen +
                '}';
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public List<String> getParametersNames() {
        return null;
    }

    @Override
    public List<ParameterValue> getParameterValues(String name) {
        return null;
    }
}
