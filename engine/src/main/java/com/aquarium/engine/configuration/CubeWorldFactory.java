package com.aquarium.engine.configuration;

import com.aquarium.engine.active.FrozenHole;
import com.aquarium.engine.impact.SimpleImpactStrategy;
import com.aquarium.engine.handler.*;
import com.aquarium.engine.handler.parameters.AverageAmountParameter;
import com.aquarium.engine.handler.parameters.AverageDirectionParameter;
import com.aquarium.engine.handler.parameters.AverageVelocityParameter;
import com.aquarium.entities.Point;
import com.aquarium.entities.World;
import com.aquarium.entities.support.StatisticsAccumulator;
import com.aquarium.entities.support.WorldFactory;
import com.aquarium.world.CubeWorld;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by artem on 6/2/14.
 */
public class CubeWorldFactory implements WorldFactory{

    public static final int BITENERGY = 1;
    static float BITSIZE = 0.1f;
    public static float SIZE = BITSIZE * 3 * 200; // 60
    public static int BITSAMOUNT = 8000000;
    public static float WORLD_ENERGY = BITSAMOUNT * BITENERGY;
    static final Logger log = LogManager.getLogger("IncrementalEngine");

    public World createWorld() {
        CubeWorld world = new CubeWorld(SIZE, BITSIZE);
        world.setImpactStrategy(new SimpleImpactStrategy(BITSIZE));
        StatisticsAccumulator dataSink = new SimpleStatisticsLogger();

        for (int i = 1; i <= 60; i++) {
            CubeObserver observer = new CubeObserver(i, new Point((i - .5f) - SIZE / 2f, 0f, 0f), 1, dataSink);
            observer.addParameter(new AverageVelocityParameter("velocity"));
            observer.addParameter(new AverageDirectionParameter("direction"));
            observer.addParameter(new AverageAmountParameter("amount"));
            world.addObserver(observer);

            log.info("observer {} | position {}| size {}", new Object[]{observer.getId(), observer.getPosition(), observer.getSize()});
        }

        FrozenHole earth = new FrozenHole(1, 2, new Point(5, 0, 0), 0.3f, dataSink);
        FrozenHole moon = new FrozenHole(2, 2, new Point(-5, 0, 0), 0.3f, dataSink);
        world.addModificator(earth);
        world.addModificator(moon);

        log.info("modifier  earth | position {}| size {}", new Object[]{earth.getPosition(), earth.getSize()});
        log.info("modifier  moon | position {}| size {}", new Object[]{moon.getPosition(), moon.getSize()});
        world.createBits(BITSAMOUNT, WORLD_ENERGY);

        return world;
    }

}
