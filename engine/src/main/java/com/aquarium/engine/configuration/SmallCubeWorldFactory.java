package com.aquarium.engine.configuration;

import com.aquarium.engine.active.BitsTrap;
import com.aquarium.engine.active.FrozenSurface;
import com.aquarium.engine.handler.SimpleStatisticsAccumulator;
import com.aquarium.engine.handler.SphereObserver;
import com.aquarium.engine.impact.SimpleImpactStrategy;
import com.aquarium.engine.handler.parameters.AverageAmountParameter;
import com.aquarium.engine.handler.parameters.AverageDirectionParameter;
import com.aquarium.engine.handler.parameters.AverageVelocityParameter;
import com.aquarium.engine.handler.parameters.CumulativeDirectionParameter;
import com.aquarium.entities.PipeVector;
import com.aquarium.entities.Point;
import com.aquarium.entities.World;
import com.aquarium.entities.support.StatisticsAccumulator;
import com.aquarium.entities.support.WorldFactory;
import com.aquarium.world.CubeWorld;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by artem on 6/2/14.
 */
public class SmallCubeWorldFactory implements WorldFactory{

    static final Logger log = LogManager.getLogger("IncrementalEngine");
    public static final int BITENERGY = 1;
    public static final float FROZEN_FACTOR = 1f;
    public static float SIZE = 50;
    public static float P5 = 1 / 50f;
    static float BITSIZE = 0.1f;
    public static int BITSAMOUNT = 1000000;
    public static float WORLD_ENERGY = BITSAMOUNT * BITENERGY;

    public World createWorld() {
        CubeWorld world = new CubeWorld(SIZE, BITSIZE);
        world.setImpactStrategy(new SimpleImpactStrategy(BITSIZE));
        StatisticsAccumulator dataSink = new SimpleStatisticsAccumulator();

        for (int i = 1; i <= 10; i++) {
            //addObserver(i, world, dataSink, new Point(((i - 1) - ((10 - 1) / 2.0)) * SIZE * P5, 0, 0));
        }

//        FrozenHole earth = new FrozenHole(11, SIZE * P5, new Point(1.5 * SIZE * P5, 0, 0), FROZEN_FACTOR, dataSink);
//        FrozenHole moon = new FrozenHole(12, SIZE * P5, new Point(-1.5 * SIZE * P5, 0, 0), FROZEN_FACTOR, dataSink);
        BitsTrap minus1 = new BitsTrap(11, new Point(1.5f * SIZE * P5, 0, 0), SIZE * P5, true, world, dataSink, false, false);
        BitsTrap plus = new BitsTrap(12, new Point(-1.5f * SIZE * P5, 0, 0), SIZE * P5, false, world, dataSink, false, false);

        //plus surrenders
        addObserver(101, world, dataSink, new Point(plus.getPosition().getX() + SIZE * P5 * 2, plus.getPosition().getY(), plus.getPosition().getZ()));
        addObserver(102, world, dataSink, new Point(plus.getPosition().getX() - SIZE * P5 * 2, plus.getPosition().getY(), plus.getPosition().getZ()));
        addObserver(103, world, dataSink, new Point(plus.getPosition().getX(), plus.getPosition().getY() + SIZE * P5 * 2, plus.getPosition().getZ()));
        addObserver(104, world, dataSink, new Point(plus.getPosition().getX(), plus.getPosition().getY() - SIZE * P5 * 2, plus.getPosition().getZ()));

        addObserver(105, world, dataSink, new Point(plus.getPosition().getX() + SIZE * P5 * (float)Math.sqrt(2), plus.getPosition().getY() + SIZE * P5 * (float)Math.sqrt(2), plus.getPosition().getZ()));
        addObserver(106, world, dataSink, new Point(plus.getPosition().getX() - SIZE * P5 * (float)Math.sqrt(2), plus.getPosition().getY() + SIZE * P5 * (float)Math.sqrt(2), plus.getPosition().getZ()));
        addObserver(107, world, dataSink, new Point(plus.getPosition().getX() + SIZE * P5 * (float)Math.sqrt(2), plus.getPosition().getY() - SIZE * P5 * (float)Math.sqrt(2), plus.getPosition().getZ()));
        addObserver(108, world, dataSink, new Point(plus.getPosition().getX() - SIZE * P5 * (float)Math.sqrt(2), plus.getPosition().getY() - SIZE * P5 * (float)Math.sqrt(2), plus.getPosition().getZ()));


        PipeVector surfacePosition = new PipeVector(new Point(0, -world.getSide() / 2, 0), 0, 1, 0);
        FrozenSurface ground = new FrozenSurface(1, surfacePosition, FROZEN_FACTOR, world.getBitSize() / 3, true);

        surfacePosition = new PipeVector(new Point(0, world.getSide() / 2, 0), 0, -1, 0);
        FrozenSurface sun = new FrozenSurface(2, surfacePosition, FROZEN_FACTOR, world.getBitSize() / 3, false);

        addObserver(1000, world, dataSink, new Point(world.getSide() / 4, -world.getSide() / 2, world.getSide() / 4));
        addObserver(1001, world, dataSink, new Point(world.getSide() / 4, -world.getSide() / 4, world.getSide() / 4));
        addObserver(1002, world, dataSink, new Point(world.getSide() / 4, world.getSide() / 2, world.getSide() / 4));
//        log.info("modifier  earth | position {}| size {}", new Object[]{earth.getPosition(), earth.getSize()});
//        log.info("modifier  moon | position {}| size {}", new Object[]{moon.getPosition(), moon.getSize()});
        log.info("modifier  minus | position {}| size {}", new Object[]{minus1.getPosition(), minus1.getSize()});
        log.info("modifier  plus | position {}| size {}", new Object[]{plus.getPosition(), plus.getSize()});

        world.addModificator(minus1);
        world.addModificator(plus);
        world.addModificator(ground);
        world.addModificator(sun);
        world.createBits(BITSAMOUNT, WORLD_ENERGY);

        return world;
    }

    public static void addObserver(int id, CubeWorld world, StatisticsAccumulator dataSink, Point point) {
        //CubeObserver plusObserver = new CubeObserver(id, point, SIZE * P5, dataSink);
        SphereObserver plusObserver = new SphereObserver(id, point, SIZE * P5, dataSink);
        plusObserver.addParameter(new AverageVelocityParameter("velocity"));
        plusObserver.addParameter(new AverageDirectionParameter("direction"));
        plusObserver.addParameter(new CumulativeDirectionParameter("cumulative_direction"));
        plusObserver.addParameter(new AverageAmountParameter("amount"));
        world.addObserver(plusObserver);
        log.info("observer {} | position {}| size {}", new Object[]{plusObserver.getId(), plusObserver.getPosition(), plusObserver.getSize()});
    }
}
