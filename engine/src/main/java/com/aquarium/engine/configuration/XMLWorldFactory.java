package com.aquarium.engine.configuration;

import com.aquarium.engine.active.BitsTrap;
import com.aquarium.engine.active.EllipseBitsTrap;
import com.aquarium.engine.active.FrozenHole;
import com.aquarium.engine.active.FrozenSurface;
import com.aquarium.engine.configuration.beans.*;
import com.aquarium.engine.impact.SimpleImpactStrategy;
import com.aquarium.engine.handler.*;
import com.aquarium.engine.handler.parameters.*;
import com.aquarium.entities.*;
import com.aquarium.entities.support.StatisticsAccumulator;
import com.aquarium.entities.support.WorldFactory;
import com.aquarium.math.scale.BitsCubeIndexFactory;
import com.aquarium.math.scale.KDTreeIndexFactory;
import com.aquarium.world.CubeWorld;
import com.aquarium.world.WorldEnergyCounter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

/**
 * Created by artem on 8/31/14.
 */
public class XMLWorldFactory implements WorldFactory {

    static final Logger log = LogManager.getLogger("IncrementalEngine");
    private String fileName;

    public XMLWorldFactory(String fileName) {
        this.fileName = fileName;
    }

    public World createWorld() {
        WorldConfiguration wc = null;
        try {
            JAXBContext jc = JAXBContext.newInstance(WorldConfiguration.class, ImpactStrategyConfiguration.class,
                    ModificatorConfiguration.class, ObserverConfiguration.class, Position.class);

            Unmarshaller u = jc.createUnmarshaller();
            wc = (WorldConfiguration)
                    u.unmarshal(new FileReader(fileName));
            log.info("Config File: " + new Scanner(new File(fileName)).useDelimiter("\\A").next());
        } catch (Exception e) {
            throw new RuntimeException("cannot read configuration: " + e.getMessage(), e);
        }

        return instantiateWorld(wc);
    }

    private World instantiateWorld(WorldConfiguration wc) {

        CubeWorld world = new CubeWorld(wc.getSide(), wc.getBitSize());
        //world.setIndexFactory(new BitsCubeIndexFactory(wc.getBitSize(), wc.getSide()));
        world.setIndexFactory(new KDTreeIndexFactory(wc.getSide()));

        StatisticsAccumulator dataSink = createStatisticsAccumulator(wc);
        world.setImpactStrategy(createImpactStrategy(wc));

        log.info("world | bits amount {}| bits size {} | cube world side size {} | world energy {} ",
                new Object[]{wc.getBitsAmount(), wc.getBitSize(), wc.getSide(), wc.getEnergy()});

        if (wc.getObservers() != null) {
            for (ObserverConfiguration observer : wc.getObservers()) {
                world.addObserver(createObserver(observer, dataSink));
            }
        }
        int amountWithdraw = 0;
        if (wc.getModificators() != null) {
            for (ModificatorConfiguration modificator : wc.getModificators()) {
                world.addModificator(createModificator(modificator, dataSink, world));
                if (BitsTrap.class.getSimpleName().equals(modificator.getType())) {
                    if (modificator.getNegative()) {
                        amountWithdraw++;
                    }
                }
            }
        }
        world.setBlackBox(new WorldEnergyCounter(amountWithdraw));

        if (wc.getNotWindySides() != null && !wc.getNotWindySides().isEmpty()) {
            for (WorldSideConfiguration side : wc.getNotWindySides()) {
                world.addNotWindySide(createWorldSide(side));
            }
        }

        world.createBits(wc.getBitsAmount(), wc.getEnergy());
        return world;
    }

    private Modificator createModificator(ModificatorConfiguration modificator, StatisticsAccumulator dataSink, World world) {
        if (BitsTrap.class.getSimpleName().equals(modificator.getType())) {
            log.info("modifier  {} | position {}| size {}",
                    new Object[]{modificator.getNegative() ? "minus" : "plus", modificator.getPosition(), modificator.getSize()});
            return new BitsTrap(modificator.getId(), createPoint(modificator.getPosition()),
                    modificator.getSize(), modificator.getNegative(), world, dataSink, modificator.isMoveable(), modificator.isRotated());
        } else if (EllipseBitsTrap.class.getSimpleName().equals(modificator.getType())) {
            log.info("modifier  {} | position {}| size {}",
                    new Object[]{modificator.getNegative() ? "minus" : "plus", modificator.getPosition(), modificator.getSize()});
            PipeVector ellipseOrientation = new PipeVector(new Point(),
                    modificator.getOrientationVector().getX(), modificator.getOrientationVector().getY(),
                    modificator.getOrientationVector().getZ());
            return new EllipseBitsTrap(modificator.getId(), createPoint(modificator.getPosition()),
                    modificator.getSize(), modificator.getNegative(), world, dataSink, modificator.isMoveable(),
                    ellipseOrientation, modificator.getEllipseLength());
        } else if (FrozenHole.class.getSimpleName().equals(modificator.getType())) {
            log.info("modifier  Frozen | position {}| size {}",
                    new Object[]{modificator.getPosition(), modificator.getSize()});
            return new FrozenHole(modificator.getId(), modificator.getSize(), createPoint(modificator.getPosition()),
                    modificator.getFactor(), dataSink);
        }
        if (FrozenSurface.class.getSimpleName().equals(modificator.getType())) {
            log.info("modifier  {} surface | position {}| orientation | size {}",
                    new Object[]{modificator.getNegative() ? "minus" : "plus",
                            modificator.getPosition(), modificator.getOrientationVector(), modificator.getSize()});
            PipeVector surfacePosition = new PipeVector(createPoint(modificator.getPosition()),
                    modificator.getOrientationVector().getX(), modificator.getOrientationVector().getY(),
                    modificator.getOrientationVector().getZ());
            return new FrozenSurface(modificator.getId(), surfacePosition, modificator.getFactor(), modificator.getSize(),
                    modificator.getNegative());
        }
        throw new RuntimeException("Can not cast Modificator for provided configuration: " + modificator.getType());
    }

    private Observer createObserver(ObserverConfiguration observer, StatisticsAccumulator dataSink) {
        Observer observerInstance;
        if (SurfaceObserver.class.getSimpleName().equals(observer.getType())) {
            observerInstance = new SurfaceObserver(observer.getId(),
                    createVector(createPoint(observer.getPosition()), observer.getOrientation()),
                    observer.getSize());
        } else if (SphereObserver.class.getSimpleName().equals(observer.getType())) {
            observerInstance = new SphereObserver(observer.getId(), createPoint(observer.getPosition()), observer.getSize(), dataSink);
        } else {
            observerInstance = new CubeObserver(observer.getId(), createPoint(observer.getPosition()), observer.getSize(), dataSink);
        }
        if (observer.getParameters() != null) {
            for (String parameter : observer.getParameters()) {
                if (AverageVelocityParameter.class.getSimpleName().equals(parameter)) {
                    observerInstance.addParameter(new AverageVelocityParameter("velocity"));
                } else if (AverageDirectionParameter.class.getSimpleName().equals(parameter)) {
                    observerInstance.addParameter(new AverageDirectionParameter("direction"));
                } else if (CumulativeDirectionParameter.class.getSimpleName().equals(parameter)) {
                    observerInstance.addParameter(new CumulativeDirectionParameter("cumulative_direction"));
                } else if (AverageAmountParameter.class.getSimpleName().equals(parameter)) {
                    observerInstance.addParameter(new AverageAmountParameter("amount"));
                } else if (ImpulseParameter.class.getSimpleName().equals(parameter)) {
                    observerInstance.addParameter(new ImpulseParameter());
                } else {
                    throw new RuntimeException("Can not cast Observer Parameter for provided configuration: " + parameter);
                }
            }
        }
        log.info("observer type {} | id {} | position {} | size {}", new Object[]{observer.getType(), observerInstance.getId(), observerInstance.getPosition(), observerInstance.getSize()});

        return observerInstance;
    }

    private PipeVector createVector(Point point, Position orientation) {
        return new PipeVector(point, orientation.getX(), orientation.getY(), orientation.getZ());
    }

    private Point createPoint(Position position) {
        return new Point(position.getX(), position.getY(), position.getZ());
    }

    private Point createWorldSide(WorldSideConfiguration position) {
        return new Point(position.getX(), position.getY(), position.getZ());
    }

    private ImpactStrategy createImpactStrategy(WorldConfiguration wc) {
        ImpactStrategyConfiguration strategy = wc.getImpactStrategy();
        if (SimpleImpactStrategy.class.getSimpleName().equals(strategy.getType())) {
            return new SimpleImpactStrategy(wc.getBitSize());
        }
        throw new RuntimeException("Can not cast ImpactStrategy for provided configuration: " + strategy.getType());
    }

    private StatisticsAccumulator createStatisticsAccumulator(WorldConfiguration wc) {
        if (wc.getStatisticsAccumulatorType().equals(SimpleStatisticsAccumulator.class.getSimpleName())) {
            return new SimpleStatisticsAccumulator();
        } else if (wc.getStatisticsAccumulatorType().equals(SimpleStatisticsLogger.class.getSimpleName())) {
            return new SimpleStatisticsLogger();
        }
        throw new RuntimeException("Can not cast StatisticsAccumulator for provided configuration:: " + wc.getStatisticsAccumulatorType());
    }
}