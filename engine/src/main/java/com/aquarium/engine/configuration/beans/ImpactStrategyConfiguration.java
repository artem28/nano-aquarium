package com.aquarium.engine.configuration.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by artem on 8/31/14.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class ImpactStrategyConfiguration {
    @XmlAttribute
    String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
