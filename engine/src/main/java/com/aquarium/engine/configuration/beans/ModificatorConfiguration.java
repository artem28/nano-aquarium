package com.aquarium.engine.configuration.beans;

import javax.xml.bind.annotation.*;

/**
 * Created by artem on 8/31/14.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class ModificatorConfiguration {
    @XmlAttribute
    int id;

    @XmlAttribute
    String type;

    @XmlAttribute
    float size;

    @XmlAttribute
    boolean moveable;

    @XmlAttribute
    float factor;

    @XmlAttribute
    private boolean nagative;

    @XmlElement
    private Position position;

    @XmlElement
    private Position orientation;

    @XmlElement
    private float ellipseLength;

    @XmlAttribute
    boolean rotated;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public float getFactor() {
        return factor;
    }

    public void setFactor(float factor) {
        this.factor = factor;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public boolean getNegative() {
        return nagative;
    }

    public void setNagative(boolean nagative) {
        this.nagative = nagative;
    }

    public Position getOrientationVector() {
        return orientation;
    }

    public void setOrientation(Position orientation) {
        this.orientation = orientation;
    }

    public boolean isMoveable() {
        return moveable;
    }

    public void setMoveable(boolean moveable) {
        this.moveable = moveable;
    }

    public void setEllipseLength(float ellipseLength) {
        this.ellipseLength = ellipseLength;
    }

    public float getEllipseLength() {
        return this.ellipseLength;
    }

    public boolean isRotated() {
        return rotated;
    }

    public void setRotated(boolean rotated) {
        this.rotated = rotated;
    }
}
