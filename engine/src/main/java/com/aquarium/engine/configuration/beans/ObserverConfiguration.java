package com.aquarium.engine.configuration.beans;

import javax.xml.bind.annotation.*;
import java.util.Set;

/**
 * Created by artem on 8/31/14.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class ObserverConfiguration {
    @XmlAttribute
    int id;

    @XmlAttribute
    float size;

    @XmlAttribute
    String type;

    @XmlElement
    Position position;

    @XmlElement(name = "orientation")
    Position orientation;

    @XmlElementWrapper(name = "parameters")
    @XmlElement(name = "parameter")
    Set<String> parameters;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Set<String> getParameters() {
        return parameters;
    }

    public void setParameters(Set<String> parameters) {
        this.parameters = parameters;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Position getOrientation() {
        return orientation;
    }

    public void setOrientation(Position orientation) {
        this.orientation = orientation;
    }
}
