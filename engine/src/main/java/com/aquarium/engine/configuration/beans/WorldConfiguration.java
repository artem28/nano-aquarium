package com.aquarium.engine.configuration.beans;

import com.aquarium.world.WorldSide;

import javax.xml.bind.annotation.*;
import java.util.Set;

/**
 * Created by artem on 8/31/14.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class WorldConfiguration {
    @XmlAttribute
    private int bitsAmount = 0;

    @XmlAttribute
    public float bitSize;

    @XmlAttribute
    private float side = 1;

    @XmlAttribute
    String StatisticsAccumulatorType;

    @XmlElement
    private ImpactStrategyConfiguration impactStrategy;

    @XmlElementWrapper(name = "notwindysides")
    @XmlElement(name = "notwindyside")
    Set<WorldSideConfiguration> notWindySides;

    @XmlElementWrapper(name = "observers")
    @XmlElement(name = "observer")
    Set<ObserverConfiguration> observers;

    @XmlElementWrapper(name = "modificators")
    @XmlElement(name = "modificator")
    Set<ModificatorConfiguration> modificators;

    @XmlAttribute
    private float energy;

    public int getBitsAmount() {
        return bitsAmount;
    }

    public void setBitsAmount(int bitsAmount) {
        this.bitsAmount = bitsAmount;
    }

    public float getBitSize() {
        return bitSize;
    }

    public void setBitSize(float bitSize) {
        this.bitSize = bitSize;
    }

    public float getSide() {
        return side;
    }

    public void setSide(float side) {
        this.side = side;
    }

    public String getStatisticsAccumulatorType() {
        return StatisticsAccumulatorType;
    }

    public void setStatisticsAccumulatorType(String statisticsAccumulatorType) {
        StatisticsAccumulatorType = statisticsAccumulatorType;
    }

    public ImpactStrategyConfiguration getImpactStrategy() {
        return impactStrategy;
    }

    public void setImpactStrategy(ImpactStrategyConfiguration impactStrategy) {
        this.impactStrategy = impactStrategy;
    }

    public Set<ObserverConfiguration> getObservers() {
        return observers;
    }

    public void setObservers(Set<ObserverConfiguration> observers) {
        this.observers = observers;
    }

    public Set<ModificatorConfiguration> getModificators() {
        return modificators;
    }

    public void setModificators(Set<ModificatorConfiguration> modificators) {
        this.modificators = modificators;
    }

    public float getEnergy() {
        return energy;
    }

    public void setEnergy(float energy) {
        this.energy = energy;
    }

    public Set<WorldSideConfiguration> getNotWindySides() {
        return notWindySides;
    }

    public void setNotWindySides(Set<WorldSideConfiguration> notWindySides) {
        this.notWindySides = notWindySides;
    }
}
