package com.aquarium.engine.handler;

import com.aquarium.entities.Bit;
import com.aquarium.entities.Observer;
import com.aquarium.entities.Point;
import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.entities.support.ParameterValue;
import com.aquarium.entities.support.StatisticsAccumulator;
import com.google.common.base.Function;
import com.google.common.collect.Lists;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by artem on 5/8/14.
 */
public abstract class AbstractObserver implements Observer {

    protected Point position;

    private StatisticsAccumulator dataSink;

    private List<ObservableParameter> parameters = new ArrayList();

    private int id;
    private double otime = 0;

    public AbstractObserver(int id) {
        this.id = id;
    }

    public AbstractObserver(int id, Point position, StatisticsAccumulator dataSink) {
        this.position = position;
        this.dataSink = dataSink;
        this.id = id;
    }

    @Override
    public void flush(float time) {
        synchronized (this) {
            if (otime != time) {
                dataSink.saveParameters(id, time, parameters);
                otime = time;
            }
        }
    }

    @Override
    public void observe(Iterable<Bit> bits) {
        for (Bit bit : bits) {
            for (ObservableParameter parameter : parameters) {
                parameter.consider(bit);
            }
        }
    }

    @Override
    public void addParameter(ObservableParameter param) {
        parameters.add(param);
    }

    @Override
    public abstract boolean isAcceptable(Bit bit);

    @Override
    public int getId() {
        return id;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public abstract float getSize();

    public abstract void setSize(float cubeSize);

    public StatisticsAccumulator getDataSink() {
        return dataSink;
    }

    public void setDataSink(StatisticsAccumulator dataSink) {
        this.dataSink = dataSink;
    }

    public List<ObservableParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<ObservableParameter> parameters) {
        this.parameters = parameters;
        dataSink.parametersInfo(parameters);
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public List<String> getParametersNames() {
        return Lists.transform(parameters, new Function<ObservableParameter, String>() {
            @Override
            public String apply(@Nullable ObservableParameter input) {
                return input.getName();
            }
        });
    }

    @Override
    public List<ParameterValue> getParameterValues(String name) {
        return dataSink.getParameterValues(id, name);
    }
}
