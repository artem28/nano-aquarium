package com.aquarium.engine.handler;

import com.aquarium.entities.Bit;
import com.aquarium.entities.Observer;
import com.aquarium.entities.Point;
import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.entities.support.StatisticsAccumulator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by artem on 5/8/14.
 */
public class CubeObserver extends AbstractObserver {

    private float cubeSize;

    public CubeObserver(int id) {
        super(id);
    }

    public CubeObserver(int id, Point position, float cubeSize, StatisticsAccumulator dataSink) {
        super(id, position, dataSink);
        this.cubeSize = cubeSize;
    }

    @Override
    public boolean isAcceptable(Bit bit) {
        Point point = bit.getPipe().getOffset();
        double a1X = position.getX() - cubeSize / 2;
        double a2X = position.getX() + cubeSize / 2;
        double a1Y = position.getY() - cubeSize / 2;
        double a2Y = position.getY() + cubeSize / 2;
        double a1Z = position.getZ() - cubeSize / 2;
        double a2Z = position.getZ() + cubeSize / 2;
        return point.getX() <= a2X && point.getX() >= a1X &&
                point.getY() <= a2Y && point.getY() >= a1Y &&
                point.getZ() <= a2Z && point.getZ() >= a1Z;
    }

    public float getSize() {
        return cubeSize;
    }

    public void setSize(float cubeSize) {
        this.cubeSize = cubeSize;
    }

    @Override
    public String toString() {
        return "CubeObserver{" +
                "cubeSize=" + cubeSize +
                ", position=" + position +
                ", id=" + getId() +
                '}';
    }
}
