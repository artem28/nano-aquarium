package com.aquarium.engine.handler;

import com.aquarium.entities.Point;
import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.entities.support.ParameterValue;
import com.aquarium.entities.support.StatisticsAccumulator;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nullable;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;


/**
 * Created by artem on 5/8/14.
 */
public class SimpleStatisticsAccumulator implements StatisticsAccumulator {

    private static final int LIST_SIZE = 100;
    static private Logger logger = LogManager.getLogger("DATA");
    Map<String, ConcurrentLinkedQueue<ParameterValue>> firstAccumulator = new HashMap();
    Map<String, ConcurrentLinkedQueue<ParameterValue>> secondAccumulator = new HashMap();
    Map<String, ConcurrentLinkedQueue<ParameterValue>> thirdAccumulator = new HashMap();
    Map<String, ConcurrentLinkedQueue<ParameterValue>> fourthAccumulator = new HashMap();
    Map<String, ConcurrentLinkedQueue<ParameterValue>> fifthAccumulator = new HashMap();

    @Override
    public void saveParameter(int id, float time, ObservableParameter parameter) {
        String name = parameter.getName() + "_" + id;
        log(time, id, parameter.getName(), 0, parameter.provideParameterValue());

        ParameterValue value = pushValue(firstAccumulator, name, new ParameterValue(time, parameter.provideParameterValue()));
        if (value != null) {
            log(time, id, parameter.getName(), 1, value.getValue());
            value = pushValue(secondAccumulator, name, value);
            if (value != null) {
                log(time, id, parameter.getName(), 2, value.getValue());
                value = pushValue(thirdAccumulator, name, value);
                if (value != null) {
                    log(time, id, parameter.getName(), 3, value.getValue());
                    value = pushValue(fourthAccumulator, name, value);
                    if (value != null) {
                        log(time, id, parameter.getName(), 4, value.getValue());
                        value = pushValue(fifthAccumulator, name, value);
                        if (value != null) {
                            log(time, id, parameter.getName(), 5, value.getValue());
                        }
                    }
                }
            }
        }
    }

    @Override
    public void saveParameters(int id, float time, List<ObservableParameter> parameters) {
        for (ObservableParameter parameter : parameters) {
            saveParameter(id, time, parameter);
        }
    }

    private void log(double time, int id, String name, int i, Object value) {
        logger.info("Time {} | Observer id {} | Parameter {} | average level {} | {}", new Object[]{time, id, name, i, value});
    }

    private ParameterValue pushValue(Map<String, ConcurrentLinkedQueue<ParameterValue>> map, String name, ParameterValue value) {
        ConcurrentLinkedQueue<ParameterValue> list = map.get(name);
        if (list == null) {
            list = new ConcurrentLinkedQueue<ParameterValue>();
            map.put(name, list);
        }
        list.add(value);
        if (list.size() >= LIST_SIZE) {
            ParameterValue average = getAverage(list);
            list.clear();
            return average;
        }
        return null;
    }

    private ParameterValue getAverage(ConcurrentLinkedQueue<ParameterValue> list) {
        if (list.iterator().hasNext()) {
            ParameterValue paramValue = list.iterator().next();
            Object v = paramValue.getValue();
            float startTime = paramValue.getTime();
            if (v instanceof Number) {
                float average = 0;
                float stopTime = 0;
                for (ParameterValue paramV : list) {
                    average = average + ((Number) paramV.getValue()).floatValue();
                    stopTime = paramV.getTime();
                }
                return new ParameterValue(startTime + (stopTime - startTime) / 2, average / list.size());
            } else if (v instanceof Point) {
                float averageX = 0;
                float averageY = 0;
                float averageZ = 0;
                float stopTime = 0;
                for (ParameterValue paramV : list) {
                    averageX = averageX + ((Point) paramV.getValue()).getX();
                    averageY = averageY + ((Point) paramV.getValue()).getY();
                    averageZ = averageZ + ((Point) paramV.getValue()).getZ();
                    stopTime = paramV.getTime();
                }
                return new ParameterValue(startTime + (stopTime - startTime) / 2,
                        new Point(averageX / list.size(), averageY / list.size(), averageZ / list.size()));
            }
        }
        throw new RuntimeException("unsupported parameter value type!");
    }

    public void parametersInfo(List<ObservableParameter> parameters) {

    }

    public List<ParameterValue> getParameterValues(int id, String nameParam) {
        String name = nameParam + "_" + id;
        List<ParameterValue> result = new ArrayList<ParameterValue>();
        if (fifthAccumulator.get(name) != null && !fifthAccumulator.get(name).isEmpty()) {
            result.addAll(fifthAccumulator.get(name));
        }
        if (fourthAccumulator.get(name) != null && !fourthAccumulator.get(name).isEmpty()) {
            result.addAll(fourthAccumulator.get(name));
        }
        if (thirdAccumulator.get(name) != null && !thirdAccumulator.get(name).isEmpty()) {
            result.addAll(thirdAccumulator.get(name));
        }
        if (secondAccumulator.get(name) != null && !secondAccumulator.get(name).isEmpty()) {
            result.addAll(secondAccumulator.get(name));
        }

        if (firstAccumulator.get(name) != null) {
            result.addAll(firstAccumulator.get(name));
        }
        return result;
    }
}