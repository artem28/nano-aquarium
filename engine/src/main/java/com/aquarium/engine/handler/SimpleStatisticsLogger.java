package com.aquarium.engine.handler;

import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.entities.support.ParameterValue;
import com.aquarium.entities.support.StatisticsAccumulator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;


/**
 * Created by artem on 5/8/14.
 */
public class SimpleStatisticsLogger implements StatisticsAccumulator {
    static private Logger logger = LogManager.getLogger("DATA");

    @Override
    public void saveParameters(int id, float time, List<ObservableParameter> parameters) {
        StringBuilder s = new StringBuilder(" , ");
        for (ObservableParameter parameter : parameters) {
            s.append(parameter.provideParameterValue());
            s.append(" , ");
        }
        logger.info("observer {} | time {} | {} | ", new Object[]{id, time, s});
    }

    @Override
    public void saveParameter(int id, float time, ObservableParameter parameter) {
        StringBuilder s = new StringBuilder(" , ");
        s.append(parameter.provideParameterValue());
        s.append(" , ");
        logger.info("observer {} | time {} | {} | ", new Object[]{id, time, s});
    }

    @Override
    public void parametersInfo(List<ObservableParameter> parameters) {
        StringBuilder s = new StringBuilder();
        for (ObservableParameter parameter : parameters) {
            s.append(parameter.getName());
            s.append(",");
        }
        logger.info("parameter names : {}", s);
    }

    @Override
    public  List<ParameterValue> getParameterValues(int id, String name) {
        return null;
    }
}
