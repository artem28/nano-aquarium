package com.aquarium.engine.handler;

import com.aquarium.entities.Bit;
import com.aquarium.entities.Observer;
import com.aquarium.entities.Point;
import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.entities.support.StatisticsAccumulator;
import com.aquarium.math.PointCan;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artem on 5/8/14.
 */
public class SphereObserver extends AbstractObserver {

    protected float radius;

    public SphereObserver(int id) {
        super(id);
    }

    public SphereObserver(int id, Point position, float size, StatisticsAccumulator dataSink) {
        super(id, position, dataSink);
        this.radius = size / 2;
    }

    @Override
    public boolean isAcceptable(Bit bit) {
        Point point = bit.getPipe().getOffset();

        double dist = PointCan.getDistance(point, position);

        return dist < radius;
    }

    public float getSize() {
        return radius * 2;
    }

    public void setSize(float size) {
        this.radius = size / 2;
    }

    @Override
    public String toString() {
        return "SphereObserver{" +
                "radius=" + radius +
                ", position=" + position +
                ", id=" + getId() +
                '}';
    }
}
