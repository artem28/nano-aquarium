package com.aquarium.engine.handler;

import com.aquarium.entities.*;
import com.aquarium.math.DoubleCan;

/**
 * Created by artem on 10/12/14.
 */
public class SurfaceObserver extends AbstractObserver implements SurfaceBody {

    private PipeVector position;
    private float size;

    public SurfaceObserver(int id, PipeVector pos, float size) {
        super(id);
        this.size = size / 2.0f;
        position = pos;
    }

    @Override
    public boolean isAcceptable(Bit bit) {
        Point pointPos = bit.getPipe().getOffset();
        if (DoubleCan.areEqual(position.getFactorX(), 0) && DoubleCan.areEqual(position.getFactorY(), 0)) {
            return pointPos.getZ() < (position.getOffset().getZ() + size) && pointPos.getZ() > (position.getOffset().getZ() - size);
        } else if (DoubleCan.areEqual(position.getFactorY(), 0) && DoubleCan.areEqual(position.getFactorZ(), 0)) {
            return pointPos.getX() < (position.getOffset().getX() + size) && pointPos.getX() > (position.getOffset().getX() - size);
        } else if (DoubleCan.areEqual(position.getFactorX(), 0) && DoubleCan.areEqual(position.getFactorZ(), 0)) {
            return pointPos.getY() < (position.getOffset().getY() + size) && pointPos.getY() > (position.getOffset().getY() - size);
        }
        throw new RuntimeException("Frozen surface has wrong configuration");
    }



    @Override
    public float getSize() {
        return size * 2;
    }

    @Override
    public void setSize(float cubeSize) {
        size = cubeSize / 2.0f;
    }

    @Override
    public Point getPosition() {
        return position.getOffset();
    }

    @Override
    public PipeVector getSurfaceOrientation() {
        return position;
    }

    @Override
    public void flush(float time) {
    }

    @Override
    public String toString() {
        return "SurfaceObserver{" +
                "position=" + position +
                ", size=" + size +
                ", id=" + getId() +
                '}';
    }
}
