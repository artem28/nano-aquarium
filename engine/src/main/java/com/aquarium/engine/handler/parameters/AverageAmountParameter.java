package com.aquarium.engine.handler.parameters;

import com.aquarium.entities.Bit;
import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.entities.support.ParameterValue;

/**
 * Created by artem on 5/8/14.
 */
public class AverageAmountParameter implements ObservableParameter<Long> {
    private long amountCounter = 0;
    private long previousAmountCounter = 0;
    private String name;
    private boolean gatheringMode = true;

    public AverageAmountParameter(String name) {
        this.name = name;
    }

    @Override
    public synchronized void consider(Bit bit) {
        if (!gatheringMode) {
            previousAmountCounter = amountCounter;
            amountCounter = 0;
            gatheringMode = true;
        }
        amountCounter++;
    }

    @Override
    public synchronized Long provideParameterValue() {
        gatheringMode = false;
        return amountCounter;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Long getPrevious() {
        return previousAmountCounter;
    }
}
