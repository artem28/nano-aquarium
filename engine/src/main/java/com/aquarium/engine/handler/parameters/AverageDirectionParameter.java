package com.aquarium.engine.handler.parameters;

import com.aquarium.entities.Bit;
import com.aquarium.entities.Point;
import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.entities.support.ParameterValue;
import com.aquarium.math.PipesCan;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artem on 5/8/14.
 */
public class AverageDirectionParameter implements ObservableParameter<Point> {

    private List<Point> velocities = new ArrayList<Point>();
    float averageVelocityX = 0;
    float averageVelocityY = 0;
    float averageVelocityZ = 0;
    Point averageVelocity = new Point();
    private String name;

    public AverageDirectionParameter(String name) {
        this.name = name;
    }


    @Override
    public synchronized void consider(Bit bit) {
        if (velocities.isEmpty()) {
            averageVelocity = new Point(averageVelocityX, averageVelocityY, averageVelocityZ);
            averageVelocityX = 0;
            averageVelocityY = 0;
            averageVelocityZ = 0;
        }
        velocities.add(PipesCan.getVelocity(bit.getPipe(), bit.getVelocity()));
    }

    @Override
    public synchronized Point provideParameterValue() {
        if (!velocities.isEmpty()) {
            for (Point velocity : velocities) {
                averageVelocityX = averageVelocityX + velocity.getX();
                averageVelocityY = averageVelocityY + velocity.getY();
                averageVelocityZ = averageVelocityZ + velocity.getZ();
            }
            averageVelocityX = averageVelocityX / velocities.size();
            averageVelocityY = averageVelocityY / velocities.size();
            averageVelocityZ = averageVelocityZ / velocities.size();
        }
        velocities = new ArrayList<Point>();

        return new Point(averageVelocityX, averageVelocityY, averageVelocityZ);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Point getPrevious() {
        return null;
    }

}
