package com.aquarium.engine.handler.parameters;

import com.aquarium.entities.Bit;
import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.entities.support.ParameterValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artem on 5/8/14.
 */
public class AverageVelocityParameter implements ObservableParameter<Double> {

    private List<Float> velocities = new ArrayList<Float>();
    double averageVelocity = 0;
    double prevAverageVelocity = 0;
    private String name;

    public AverageVelocityParameter(String name) {
        this.name = name;
    }


    @Override
    public synchronized void consider(Bit bit) {
        if (velocities.isEmpty()) {
            prevAverageVelocity = averageVelocity;
            averageVelocity = 0;
        }
        velocities.add(bit.getVelocity());
    }

    @Override
    public synchronized Double provideParameterValue() {
        if (!velocities.isEmpty()) {
            for (double velocity : velocities) {
                averageVelocity = averageVelocity + velocity;
            }
            averageVelocity = averageVelocity / velocities.size();
        }
        velocities = new ArrayList<Float>();

        return averageVelocity;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Double getPrevious() {
        return prevAverageVelocity;
    }

}
