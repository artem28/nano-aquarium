package com.aquarium.engine.handler.parameters;

import com.aquarium.entities.Bit;
import com.aquarium.entities.Point;
import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.entities.support.ParameterValue;
import com.aquarium.math.PipesCan;
import com.aquarium.math.PointCan;

import java.util.ArrayList;

/**
 * Created by artem on 5/8/14.
 */
public class CumulativeDirectionParameter implements ObservableParameter<Point> {

    private Point velocity = new Point();
    private String name;

    public CumulativeDirectionParameter(String name) {
        this.name = name;
    }

    @Override
    public synchronized void consider(Bit bit) {
        velocity = PointCan.increasePoint(velocity, PipesCan.getVelocity(bit.getPipe(), bit.getVelocity()));
    }

    @Override
    public synchronized Point provideParameterValue() {
        return velocity;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Point getPrevious() {
        return velocity;
    }

}
