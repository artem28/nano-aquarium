package com.aquarium.engine.handler.parameters;

import com.aquarium.entities.Bit;
import com.aquarium.entities.Point;
import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.entities.support.ParameterValue;
import com.aquarium.math.PipesCan;
import com.aquarium.math.PointCan;

/**
* Created by artem on 7/6/14.
*/
public class ImpulseParameter implements ObservableParameter<Point> {
    protected Point impulses = new Point(0,0,0);

    @Override
    public synchronized void consider(Bit bit) {
        PointCan.increasePoint(impulses, PipesCan.getVelocity(bit.getPipe(), bit.getVelocity()));
    }

    @Override
    public synchronized Point provideParameterValue() {
        return impulses;
    }

    @Override
    public String getName() {
        return "impuls";
    }

    @Override
    public Point getPrevious() {
        return impulses;
    }

    @Override
    public String toString() {
        return "ImpulseParameter{" +
                "impulses=" + impulses +
                '}';
    }
}
