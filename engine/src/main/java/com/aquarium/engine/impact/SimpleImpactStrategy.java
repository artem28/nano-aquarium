package com.aquarium.engine.impact;

import com.aquarium.entities.Bit;
import com.aquarium.entities.ImpactStrategy;
import com.aquarium.entities.PipeVector;
import com.aquarium.entities.Point;
import com.aquarium.math.BitsCan;
import com.aquarium.math.PipesCan;

/**
 * Created by artem on 4/11/14.
 */
public class SimpleImpactStrategy implements ImpactStrategy {
    private double dontMoveTime;
    private double bitSize;

    public SimpleImpactStrategy(double bitSize) {
        this.bitSize = bitSize;
    }

    @Override
    public void doImpact(Bit bit1, Bit bit2) {
        PipeVector pipe1 = bit1.getPipe();
        PipeVector pipe2 = bit2.getPipe();
        PipeVector bisector = PipesCan.getBisector(bit1.getPipe(), bit2.getPipe());

        float v1 = bit1.getVelocity();
        float v2 = bit2.getVelocity();
        //projection  to bisector
        float projection1 = v1 * (bisector.getFactorX() * pipe1.getFactorX() +
                bisector.getFactorY() * pipe1.getFactorY() +
                bisector.getFactorZ() * pipe1.getFactorZ());

        float projection2 = v2 * (bisector.getFactorX() * pipe2.getFactorX() +
                bisector.getFactorY() * pipe2.getFactorY() +
                bisector.getFactorZ() * pipe2.getFactorZ());

        Point unchangedPartPipe1 = new Point(bisector.getFactorX() * projection1,
                bisector.getFactorY() * projection1,
                bisector.getFactorZ() * projection1);

        Point unchangedPartPipe2 = new Point(bisector.getFactorX() * projection2,
                bisector.getFactorY() * projection2,
                bisector.getFactorZ() * projection2);
        //perpendicular to bisector
        Point changedPartPipe1 = new Point(pipe1.getFactorX() * v1 - unchangedPartPipe1.getX(),
                pipe1.getFactorY() * v1 - unchangedPartPipe1.getY(),
                pipe1.getFactorZ() * v1 - unchangedPartPipe1.getZ());

        Point changedPartPipe2 = new Point(pipe2.getFactorX() * v2 - unchangedPartPipe2.getX(),
                pipe2.getFactorY() * v2 - unchangedPartPipe2.getY(),
                pipe2.getFactorZ() * v2 - unchangedPartPipe2.getZ());

        //calculate impact as a sum of projection of own vector and perpendicular of opposite vector
        Point resultPipe1 = new Point(unchangedPartPipe1.getX() + changedPartPipe2.getX(),
                unchangedPartPipe1.getY() + changedPartPipe2.getY(),
                unchangedPartPipe1.getZ() + changedPartPipe2.getZ());

        Point resultPipe2 = new Point(unchangedPartPipe2.getX() + changedPartPipe1.getX(),
                unchangedPartPipe2.getY() + changedPartPipe1.getY(),
                unchangedPartPipe2.getZ() + changedPartPipe1.getZ());

        bit1.getPipe().setFactorXYZ(resultPipe1.getX(), resultPipe1.getY(), resultPipe1.getZ());
        bit1.setVelocity(PipesCan.getVelocity(resultPipe1));
        bit2.getPipe().setFactorXYZ(resultPipe2.getX(), resultPipe2.getY(), resultPipe2.getZ());
        bit2.setVelocity(PipesCan.getVelocity(resultPipe2));

        bit1.setLastImpact(bit2);
        bit2.setLastImpact(bit1);
    }

    public boolean isEligableForImpact(Bit candidate1, Bit candidate2) {
        return candidate1.getLastImpact() == null || candidate1.getLastImpact().getId() != candidate2.getId();
    }

    public double getDontMoveTime() {
        return dontMoveTime;
    }

    public void setDontMoveTime(double dontMoveTime) {
        this.dontMoveTime = dontMoveTime;
    }

    public double getBitSize() {
        return bitSize;
    }

    public void setBitSize(double bitSize) {
        this.bitSize = bitSize;
    }

    public boolean checkImpact(Bit bit1, Bit bit2, double dt) {
        float t = BitsCan.getTimeClosestDistance(bit1, bit2);
        if (t > dontMoveTime && t < dt) {
            double d = BitsCan.getDistance(bit1, bit2, t);
            return d < bitSize;
        }
        return false;
    }
}
