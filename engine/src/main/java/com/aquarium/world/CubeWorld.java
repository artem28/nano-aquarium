package com.aquarium.world;

import com.aquarium.entities.*;
import com.aquarium.entities.support.WorldHolder;
import com.aquarium.math.PointCan;
import com.aquarium.math.scale.BitsCubeIndexFactory;
import com.google.common.collect.Iterables;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by artem on 3/31/14.
 */
public class CubeWorld implements World {

    static final Logger log = LogManager.getLogger("IncrementalEngine");

    private float side = 1;
    private ImpactStrategy impactStrategy;
    private Set<WorldSide> notWindySides = new HashSet<WorldSide>();
    private Set<Observer> observers = new HashSet<Observer>();
    private float time = 0;
    private SortedSet<Modificator> modificators = new TreeSet<Modificator>();
    private BitsIndex scaleHolder = null;
    private WorldEnergyCounter energyCounter;
    private float worldPower = 0;
    private IndexFactory indexFactory;
    private float bitSize;
    private int bitsAmount = 0;
    private boolean needToBeRestarted = false;

    protected Set<Bit> outCommers = new HashSet<Bit>();

    public CubeWorld(float a, float bitSize) {
        side = a;
        this.bitSize = bitSize;
        WorldHolder.world = this;
    }

    public Set<Bit> getOutCommers() {
        return outCommers;
    }

    public void setOutCommers(Set<Bit> outCommers) {
        this.outCommers = outCommers;
    }

    public void setIndexFactory(IndexFactory inf){
        indexFactory = inf;
    }

    @Override
    public BitsIndex generateIndex() {
        return indexFactory.createComplexIndex();
    }

    @Override
    public Point positionOnWorldBoundary(Bit bit) {
        float x = (float)(side * 2 * (Math.random() - 0.5f));
        float y = (float)(side * 2 * (Math.random() - 0.5f));
        float z = (float)(side * 2 * (Math.random() - 0.5f));
        float side = (float)Math.random();
        float side2 = side / 2;
        if (side < (1 / 3)) {
            return new Point(x > 0 ? side2 : -side2, y, z);
        }
        if (side > (2 / 3)) {
            return new Point(x, y > 0 ? side2 : -side2, z);
        }
        return new Point(x, y, z > 0 ? side2 : -side2);
    }

    @Override
    public Iterable<Bit> getBitsFromOutside() {
        BitsIndex bits = scaleHolder;
        float side2 = side / 2;
        Iterable<Bit> outers = Iterables.concat(bits.getBitSetInX(-Float.MAX_VALUE, -side2),
                bits.getBitSetInX(side2, Float.MAX_VALUE),
                bits.getBitSetInY(-Float.MAX_VALUE, -side2),
                bits.getBitSetInY(side2, Float.MAX_VALUE),
                bits.getBitSetInZ(-Float.MAX_VALUE, -side2),
                bits.getBitSetInZ(side2, Float.MAX_VALUE));

        return outers;
    }

    public ImpactStrategy getImpactStrategy() {
        return impactStrategy;
    }

    public void setImpactStrategy(ImpactStrategy impactStrategy) {
        this.impactStrategy = impactStrategy;
    }

    public float getSide() {
        return side;
    }

    public void setSide(float side) {
        this.side = side;
    }

    public double getTime() {
        return time;
    }

    @Override
    public boolean isInside(Point point) {
        double a2 = side / 2;
        return point.getX() <= a2 && point.getX() >= -a2 &&
                point.getY() <= a2 && point.getY() >= -a2 &&
                point.getZ() <= a2 && point.getZ() >= -a2;
    }

    @Override
    public Point randomPoint() {
        return new Point(side * (float)Math.random() - side / 2, side * (float)Math.random() - side / 2, side * (float)Math.random() - side / 2);
    }

    @Override
    public BitsIndex freeMove(double dt, Iterable<Bit> bits) {
        BitsIndex newBits = moveBits(bits);

        return newBits;
    }

    private BitsIndex moveBits(Iterable<Bit> bits) {
        float energy = 0;
        for (Bit bit : bits) {
            energy = energy + bit.getVelocity();
            bit.move(time);
            scaleHolder.modifyBit(bit);
        }
        energyCounter.addEnergy(energy);
        return null;
    }

    public WorldEnergyCounter getBlackBox() {
        return energyCounter;
    }

    @Override
    public void returnBitBack(Bit bit) {
        Point rand = PointCan.randomInSphere(new Point(), 1);
//        bit.getPipe().setFactorXYZ(rand.getX(), rand.getY(), rand.getZ());
        Point point = bit.getPipe().getOffset();
        Point offsetModification = null;
        float a2 = side / 2;
        if (point.getX() > a2) {
            if (isNotWindySide(a2, 0, 0)) {
                offsetModification = new Point(-Math.abs(bit.getPipe().getOffset().getX()),
                        bit.getPipe().getOffset().getY(),
                        bit.getPipe().getOffset().getZ());
            } else {
                bit.getPipe().setFactorXYZ(-Math.abs(rand.getX()), rand.getY(), rand.getZ());
            }
//            point.setX(a2);
        }
        if (point.getX() < -a2) {
            if (isNotWindySide(-a2, 0, 0)) {
                offsetModification = new Point(Math.abs(bit.getPipe().getOffset().getX()),
                        bit.getPipe().getOffset().getY(),
                        bit.getPipe().getOffset().getZ());
            } else {
                bit.getPipe().setFactorXYZ(Math.abs(rand.getX()), rand.getY(), rand.getZ());
            }
//            point.setX(-a2);
        }

        if (point.getY() > a2) {
            if (isNotWindySide(0, a2, 0)) {
                offsetModification = new Point(bit.getPipe().getOffset().getX(),
                        -Math.abs(bit.getPipe().getOffset().getY()),
                        bit.getPipe().getOffset().getZ());
            } else {
                bit.getPipe().setFactorXYZ(rand.getX(), -Math.abs(rand.getY()), rand.getZ());
            }
//            point.setY(a2);
        }
        if (point.getY() < -a2) {
            if (isNotWindySide(0, -a2, 0)) {
                offsetModification = new Point(bit.getPipe().getOffset().getX(),
                        Math.abs(bit.getPipe().getOffset().getY()),
                        bit.getPipe().getOffset().getZ());
            } else {
                bit.getPipe().setFactorXYZ(rand.getX(), Math.abs(rand.getY()), rand.getZ());
            }
//            point.setY(-a2);
        }

        if (point.getZ() > a2) {
            if (isNotWindySide(0, 0, a2)) {
                offsetModification = new Point(bit.getPipe().getOffset().getX(),
                        bit.getPipe().getOffset().getY(),
                        -Math.abs(bit.getPipe().getOffset().getZ()));
            } else {
                bit.getPipe().setFactorXYZ(rand.getX(), rand.getY(), -Math.abs(rand.getZ()));
            }
//            point.setZ(a2);
        }
        if (point.getZ() < -a2) {
            if (isNotWindySide(0, 0, -a2)) {
                offsetModification = new Point(bit.getPipe().getOffset().getX(),
                        bit.getPipe().getOffset().getY(),
                        Math.abs(bit.getPipe().getOffset().getZ()));
            } else {
                bit.getPipe().setFactorXYZ(rand.getX(), rand.getY(), Math.abs(rand.getZ()));
            }
//            point.setZ(-a2);
        }

        if (offsetModification != null) {
            bit.getPipe().setOffset(offsetModification);
            scaleHolder.modifyBit(bit);
        }

        if (!outCommers.contains(bit)) {
            synchronized (energyCounter) {
                float energyDelta = worldPower - energyCounter.getEnergy();
                float sigmum = Math.signum(energyDelta);
                energyDelta = Math.abs(energyDelta);
                if (energyDelta > 0) {
                    if ((sigmum < 0) || (bit.getVelocity() < (2 * worldPower / bitsAmount))) {
                        float additionalEnergy = sigmum * ((bit.getVelocity() / 3) > energyDelta ? energyDelta : bit.getVelocity() / 3);
                        bit.setVelocity(bit.getVelocity() + additionalEnergy);
                        energyCounter.removeEnergy(-additionalEnergy);
                    }
                }
            }
            outCommers.add(bit);
        }
    }

    private boolean isNotWindySide(float x, float y, float z) {
        for (WorldSide notWindySide : notWindySides) {
            if (notWindySide.isSide(new Point(x, y, z))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public float getAverageBitEnergy() {
        return worldPower / bitsAmount;
    }

    @Override
    public void createBits(int size, float energy) {
        worldPower = 0;
        for (int i = 0; i < size; i++) {
            Bit bit = new Bit(i);
            bit.setVelocity(energy / size);
            bit.setPipe(new PipeVector(randomPoint(), (float)Math.random() * 2 - 1, (float)Math.random() * 2 - 1, (float)Math.random() * 2 - 1));
            scaleHolder.addBit(bit, bit.getPipe().getOffset());
            worldPower = worldPower + bit.getVelocity();

            if (i % 100000 == 0) {
                log.info("createBits i=" + i);
            }

        }
        bitsAmount += size;
        log.info("createBits created: " + size);
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
        indexFactory.addIndex(observer.getSize());
        scaleHolder = indexFactory.createComplexIndex();

    }

    public void setBlackBox(WorldEnergyCounter energyCounter) {
        this.energyCounter = energyCounter;
    }

    @Override
    public void addModificator(Modificator observer) {
        modificators.add(observer);
        if (!(observer instanceof SurfaceBody)) {
            indexFactory.addIndex(observer.getSize());
            scaleHolder = indexFactory.createComplexIndex();
        }
    }

    @Override
    public BitsIndex getBits() {
        return scaleHolder;
    }


    public void bitsMovementFinished(){
        scaleHolder.endOfIndexModification();
    }

    public void bitsMovementStarted(){
        scaleHolder.startIndexModification();

    }

    @Override
    public void replaceBits(Iterable<Bit> bits) {
        scaleHolder = (BitsIndex) bits;
    }

    @Override
    public void addTime(float dt) {
        time = time + dt;
    }

    @Override
    public void flushObservers() {
        for (Observer observer : observers) {
            observer.flush(time);
        }
        for (Modificator modificator : modificators) {
            modificator.postAction();
            modificator.flush(time);
        }
    }

    @Override
    public float getBitSize() {
        return bitSize;
    }

    public Set<Observer> getObservers() {
        return observers;
    }

    public SortedSet<Modificator> getModificators() {
        return modificators;
    }

    @Override
    public void addNotWindySide(Point side) {
        notWindySides.add(new WorldSide(side));
    }

    @Override
    public int getBitsAmount() {
        return bitsAmount;
    }

    @Override
    synchronized public boolean isNeedToBeRestarted() {
        return needToBeRestarted;
    }

    @Override
    synchronized public void needToBeRestarted(boolean needToBeRestarted) {
        this.needToBeRestarted = needToBeRestarted;
    }

    @Override
    synchronized public boolean stopTheWorld() {
        return this.needToBeRestarted;
    }
}
