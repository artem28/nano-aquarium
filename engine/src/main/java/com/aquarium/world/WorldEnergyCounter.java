package com.aquarium.world;

import com.aquarium.entities.Bit;
import com.aquarium.entities.WorldBlackBox;

import java.util.*;

/**
 * Created by artem on 5/26/14.
 */
public class WorldEnergyCounter implements WorldBlackBox {
    private static final int MAXNOTWITHDRAWONLY = 20;
    private Queue<Float> energyPortions = new LinkedList<Float>();
    private float energy = 0;
    private Set<Bit> bitsBanks;
    private int amountWithdrawTraps;
    private int lastBitsAmount = 0;


    public WorldEnergyCounter(int withdrawTrapsAmount) {
        amountWithdrawTraps = withdrawTrapsAmount;
        bitsBanks = new HashSet<Bit>();
        lastBitsAmount = MAXNOTWITHDRAWONLY;
    }

    synchronized public void addEnergy(float value) {
        energyPortions.add(value);
    }

    synchronized public float getEnergy() {
        if (energyPortions.isEmpty()) {
            return energy;
        } else {
            energy = 0;
            for (Float value : energyPortions) {
                energy = energy + value;
            }
            energyPortions.clear();
            return energy;
        }
    }

    synchronized public void removeEnergy(float value) {
        energy = getEnergy() - value;
    }

    public void addBit(Bit bit) {
        synchronized (bitsBanks) {
            bitsBanks.add(bit);
        }
    }

    @Override
    public int getBitsSize() {
        synchronized (bitsBanks) {
            return bitsBanks.size();
        }
    }

    @Override
    public Bit pullBit() {
        synchronized (bitsBanks) {

            Iterator<Bit> portion = bitsBanks.iterator();
            if (portion.hasNext()) {
                Bit bit = portion.next();
                portion.remove();
                return bit;
            }
        }
        return null;
    }

    @Override
    public int getAverageTrapBandwidth() {
        if (amountWithdrawTraps != 0) {
            int amount = Math.round(lastBitsAmount / amountWithdrawTraps);
            if (amount == 0 && lastBitsAmount > 0) {
                return (int) Math.round(Math.random());
            }
            return amount;
        }
        return MAXNOTWITHDRAWONLY;
    }

    @Override
    public void calculateStatistics() {
        lastBitsAmount = bitsBanks.size();
    }
}