package com.aquarium.world;

import com.aquarium.entities.Point;
import com.aquarium.math.DoubleCan;

/**
 * Created by artem on 10/1/14.
 */
public class WorldSide {
    Point point;

    public WorldSide(Point p) {
        check(p);
        point = normalize(p);
    }

    boolean isSide(Point p) {
        check(p);
        Point np = normalize(p);
        return DoubleCan.areEqual(point.getX(), np.getX()) &&
                DoubleCan.areEqual(point.getY(), np.getY()) &&
                DoubleCan.areEqual(point.getZ(), np.getZ());
    }

    private Point normalize(Point p) {
        float max = Math.max(Math.max(Math.abs(p.getX()), Math.abs(p.getY())), Math.abs(p.getZ()));
        return new Point(p.getX() / max, p.getY() / max, p.getZ() / max);
    }

    private void check(Point p) {
        byte count = 0;
        if (DoubleCan.areEqual(p.getX(), 0)) {
            count++;
        }
        if (DoubleCan.areEqual(p.getY(), 0)) {
            count++;
        }
        if (DoubleCan.areEqual(p.getZ(), 0)) {
            count++;
        }
        if (count != 2) {
            throw new RuntimeException("Point cannot be assigned to a particular world side");
        }
    }


}
