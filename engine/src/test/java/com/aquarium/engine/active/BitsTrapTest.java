package com.aquarium.engine.active;

import com.aquarium.engine.handler.StubbedDataSink;
import com.aquarium.entities.*;
import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.entities.support.StatisticsAccumulator;
import com.aquarium.math.DoubleCan;
import com.aquarium.math.scale.RoughScaleHolder;
import com.aquarium.world.BitsIndexStub;
import com.aquarium.world.StubbedWorld;
import com.aquarium.world.StubbedWorldBlackBox;
import com.google.common.collect.Lists;
import junit.framework.TestCase;

import java.util.*;

/**
 * Created by artem on 7/19/14.
 */
public class BitsTrapTest extends TestCase {


    public void testMinus() {
        World stubbedWorld = new StubbedWorld();
        final List<ObservableParameter> parameters = new ArrayList<ObservableParameter>();
        StatisticsAccumulator dataSink = new StubbedDataSink() {
            @Override
            public void saveParameter(int id, float time, ObservableParameter params) {
                parameters.add(params);
            }
        };
        BitsTrap trap = new BitsTrap(1, new Point(), 10, true, stubbedWorld, dataSink, false, false);
        //first test
        Iterable<Bit> bits = getBitsInLine();
        trap.observe(0.5f, stubbedWorld, bits);
        trap.flush(1);

        Point result = (Point) parameters.get(0).provideParameterValue();
        assertTrue(DoubleCan.areEqual(result.getX(), 0));
        assertTrue(DoubleCan.areEqual(result.getY(), 0));
        assertTrue(DoubleCan.areEqual(result.getZ(), 0));
        parameters.clear();

        //second test
        bits = getBits();
        trap.observe(0.5f, stubbedWorld, bits);
        trap.flush(2);

        result = (Point) parameters.get(0).provideParameterValue();
        assertTrue(DoubleCan.areEqual(result.getX(), (float)Math.sqrt(0.5) - 1));
        assertTrue(DoubleCan.areEqual(result.getY(), (float)-Math.sqrt(0.5)));
        assertTrue(DoubleCan.areEqual(result.getZ(), 0));
        parameters.clear();


    }

    private Iterable<Bit> getBitsInLine() {
        List<Bit> result = new ArrayList<Bit>();
        Bit bit3 = new Bit(3);
        bit3.setVelocity(1);
        PipeVector pipe3 = new PipeVector(new Point(10, 0, 0), -1, 0, 0);
        bit3.setPipe(pipe3);

        Bit bit2 = new Bit(2);
        bit2.setVelocity(1);
        PipeVector pipe2 = new PipeVector(new Point(-10, 0, 0), 1, 0, 0);
        bit2.setPipe(pipe2);

        Bit bit1 = new Bit(1);
        bit1.setVelocity(1);
        PipeVector pipe1 = new PipeVector(new Point(-10, 0, 0), 1, 0, 0);
        bit1.setPipe(pipe1);

        result.add(bit1);
        result.add(bit2);
        result.add(bit3);

        return result;
    }

    private Iterable<Bit> getBits() {
        List<Bit> result = new ArrayList<Bit>();
        Bit bit3 = new Bit(3);
        bit3.setVelocity(1);
        PipeVector pipe3 = new PipeVector(new Point(10, 10, 0), -1, 0, 0);
        bit3.setPipe(pipe3);

        Bit bit2 = new Bit(2);
        bit2.setVelocity(1);
        PipeVector pipe2 = new PipeVector(new Point(-10, 10, 0), 1, 0, 0);
        bit2.setPipe(pipe2);

        Bit bit1 = new Bit(1);
        bit1.setVelocity(1);
        PipeVector pipe1 = new PipeVector(new Point(-10, -10, 0), 1, 0, 0);
        bit1.setPipe(pipe1);

        result.add(bit1);
        result.add(bit2);
        result.add(bit3);

        return result;
    }


    public void testPositive() {
        final Iterable<Bit> bits = getBitsForPositive();
        World stubbedWorld = new StubbedWorld() {
            @Override
            public Iterable<Bit> getBitsFromOutside() {
                return Collections.EMPTY_LIST;
            }

            @Override
            public void addNotWindySide(Point side) {

            }

            @Override
            public int getBitsAmount() {
                return 0;
            }

            @Override
            public boolean isNeedToBeRestarted() {
                return false;
            }

            @Override
            public void needToBeRestarted(boolean needToBeRestarted) {

            }

            @Override
            public boolean stopTheWorld() {
                return false;
            }

            @Override
            public void setOutCommers(Set<Bit> outCommers) {

            }

            @Override
            public BitsIndex getBits() {
                return new BitsIndexStub() {
                    @Override
                    public Iterable<Bit> getCloseBits(Bit bit1, float bitsize) {
                        return bits;
                    }

                    @Override
                    public Iterable<Bit> getBitSetInX(float from, float to) {
                        return Collections.EMPTY_SET;
                    }

                    @Override
                    public Iterable<Bit> getBitSetInY(float from, float to) {
                        return Collections.EMPTY_SET;
                    }

                    @Override
                    public Iterable<Bit> getBitSetInZ(float from, float to) {
                        return Collections.EMPTY_SET;
                    }
                };
            }

            @Override
            public float getSide() {
                return 50;
            }
        };
        final List<ObservableParameter> parameters = new ArrayList<ObservableParameter>();
        StatisticsAccumulator dataSink = new StubbedDataSink() {
            @Override
            public void saveParameter(int id, float time, ObservableParameter params) {
                parameters.add(params);
            }
        };
        BitsTrap trap = new BitsTrap(1, new Point(), 10, false, stubbedWorld, dataSink, false, false);
        //first test
        trap.observe(0.5f, stubbedWorld, bits);
        trap.postAction();
        trap.flush(1);

        Point result = (Point) parameters.get(0).provideParameterValue();
        assertTrue(DoubleCan.areEqual(result.getX(), -1));
        assertTrue(DoubleCan.areEqual(result.getY(), 0));
        assertTrue(DoubleCan.areEqual(result.getZ(), 0));
        parameters.clear();

        trap.postAction();
        trap.flush(2);
        result = (Point) parameters.get(0).provideParameterValue();
        assertTrue(DoubleCan.areEqual(result.getX(), -1));
        assertTrue(DoubleCan.areEqual(result.getY(), 0));
        assertTrue(DoubleCan.areEqual(result.getZ(), 0));
    }

    public Iterable<Bit> getBitsForPositive() {
        List<Bit> result = new ArrayList<Bit>();
        Bit bit3 = new Bit(3);
        bit3.setVelocity(1);
        PipeVector pipe3 = new PipeVector(new Point(0, 0, 0), -1, 0, 0);
        bit3.setPipe(pipe3);

        Bit bit2 = new Bit(2);
        bit2.setVelocity(1);
        PipeVector pipe2 = new PipeVector(new Point(0, 0, 0), -1, 0, 0);
        bit2.setPipe(pipe2);

        Bit bit1 = new Bit(1);
        bit1.setVelocity(1);
        PipeVector pipe1 = new PipeVector(new Point(0, 0, 0), 1, 0, 0);
        bit1.setPipe(pipe1);

        result.add(bit1);
        result.add(bit2);
        result.add(bit3);

        return result;
    }

    private List<Bit> getBitsForPositivePostAction() {
        List<Bit> result = new ArrayList<Bit>();
        Bit bit8 = new Bit(8);
        bit8.setVelocity(1);
        PipeVector pipe8 = new PipeVector(new Point(-100, -100, -100), -1, 0, 0);
        bit8.setPipe(pipe8);

        Bit bit7 = new Bit(7);
        bit7.setVelocity(1);
        PipeVector pipe7 = new PipeVector(new Point(-100, -100, -100), -1, 0, 0);
        bit7.setPipe(pipe7);

        Bit bit6 = new Bit(6);
        bit6.setVelocity(1);
        PipeVector pipe6 = new PipeVector(new Point(-100, -100, -100), -1, 0, 0);
        bit6.setPipe(pipe6);

        Bit bit5 = new Bit(5);
        bit5.setVelocity(1);
        PipeVector pipe5 = new PipeVector(new Point(-100, -100, -100), -1, 0, 0);
        bit5.setPipe(pipe5);

        Bit bit3 = new Bit(3);
        bit3.setVelocity(1);
        PipeVector pipe3 = new PipeVector(new Point(-100, -100, -100), -1, 0, 0);
        bit3.setPipe(pipe3);

        Bit bit2 = new Bit(2);
        bit2.setVelocity(1);
        PipeVector pipe2 = new PipeVector(new Point(-100, -100, -100), -1, 0, 0);
        bit2.setPipe(pipe2);

        Bit bit1 = new Bit(1);
        bit1.setVelocity(1);
        PipeVector pipe1 = new PipeVector(new Point(-100, -100, -100), 1, 0, 0);
        bit1.setPipe(pipe1);

        Bit bit4 = new Bit(4);
        bit4.setVelocity(1);
        PipeVector pipe4 = new PipeVector(new Point(-100, -100, -100), 1, 0, 0);
        bit4.setPipe(pipe4);

        result.add(bit1);
        result.add(bit2);
        result.add(bit4);
        result.add(bit5);
        result.add(bit6);
        result.add(bit7);
        result.add(bit8);

        return result;
    }

    public void testPositivePostAction() {
        final List<Bit> bitsToTest = getBitsForPositivePostAction();

        World stubbedWorld = new StubbedWorld() {
            List<Bit> blackBits = Lists.newArrayList(bitsToTest);
            SortedSet<Modificator> modificators = new TreeSet<Modificator>();

            @Override
            public WorldBlackBox getBlackBox() {
                return new StubbedWorldBlackBox() {
                    @Override
                    public int getBitsSize() {
                        return blackBits.size();
                    }

                    @Override
                    public Bit pullBit() {
                        Iterator<Bit> result = blackBits.iterator();
                        if (!result.hasNext()) {
                            return null;
                        }
                        Bit bit = result.next();
                        result.remove();
                        return bit;
                    }

                    @Override
                    public int getAverageTrapBandwidth() {
                        return 20;
                    }
                };
            }

            public BitsIndex getBits() {
                return new BitsIndexStub() {
                    @Override
                    public Iterable<Bit> getCloseBits(Bit bit1, float bitsize) {
                        return Collections.EMPTY_LIST;
                    }

                    @Override
                    public Iterable<Bit> getBitSetInX(float from, float to) {
                        return Collections.EMPTY_SET;
                    }

                    @Override
                    public Iterable<Bit> getBitSetInY(float from, float to) {
                        return Collections.EMPTY_SET;
                    }

                    @Override
                    public Iterable<Bit> getBitSetInZ(float from, float to) {
                        return Collections.EMPTY_SET;
                    }
                };
            }

            @Override
            public void addNotWindySide(Point side) {

            }

            @Override
            public int getBitsAmount() {
                return 0;
            }

            @Override
            public boolean isNeedToBeRestarted() {
                return false;
            }

            @Override
            public void needToBeRestarted(boolean needToBeRestarted) {

            }

            @Override
            public boolean stopTheWorld() {
                return false;
            }

            @Override
            public void setOutCommers(Set<Bit> outCommers) {

            }

            @Override
            public void addModificator(Modificator observer) {
                modificators.add(observer);
            }

            @Override
            public SortedSet<Modificator> getModificators() {
                return modificators;
            }

            @Override
            public float getSide() {
                return 50;
            }
        };
        StatisticsAccumulator dataSink = new StubbedDataSink();
        BitsTrap trap = new BitsTrap(1, new Point(20, 20, 20), 10, false, stubbedWorld, dataSink, false, false);
        stubbedWorld.addModificator(trap);
        trap.postAction();

        BitsIndex scale = new RoughScaleHolder(10);
        for (Bit bit : bitsToTest) {
            scale.addBit(bit, bit.getPipe().getOffset());
        }

        for (Bit bit : getBitsForPositive()) {
            scale.addBit(bit, bit.getPipe().getOffset());
        }

        Set<Bit> result = new HashSet<Bit>();
        for (Bit bit : scale.findAcceptable(trap)) {
            result.add(bit);
        }

        assertTrue(result.size() == bitsToTest.size());

        for (Bit bit : bitsToTest) {
            assertTrue(result.contains(bit));
        }

    }
}