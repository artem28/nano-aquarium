package com.aquarium.engine.configuration;

import com.aquarium.engine.configuration.beans.*;
import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.*;

/**
 * Created by artem on 9/1/14.
 */
public class XMLWorldFactoryTest extends TestCase {
    static final Logger log = LogManager.getLogger("IncrementalEngine");

    public void test() throws Exception {
        WorldConfiguration world = createWorld();
        JAXBContext jc = JAXBContext.newInstance(WorldConfiguration.class, ImpactStrategyConfiguration.class,
                ModificatorConfiguration.class, ObserverConfiguration.class, Position.class);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        StringWriter writer = new StringWriter();
        m.marshal(world, writer);

        log.info("serialization result: " + writer.getBuffer().toString());

        Unmarshaller u = jc.createUnmarshaller();
        WorldConfiguration po = (WorldConfiguration)
                u.unmarshal(new StringReader(writer.getBuffer().toString()));


        assertEquals(world.getBitsAmount(), po.getBitsAmount());
        assertEquals(world.getBitSize(), po.getBitSize());
        assertEquals(world.getSide(), po.getSide());
        assertEquals(world.getEnergy(), po.getEnergy());
        assertEquals(world.getStatisticsAccumulatorType(), po.getStatisticsAccumulatorType());
        assertEquals(world.getImpactStrategy().getType(), po.getImpactStrategy().getType());

        assertEquals(world.getObservers().size(), po.getObservers().size());
        assertEquals(world.getModificators().size(), po.getModificators().size());

        TreeSet<ObserverConfiguration> obTree1 = new TreeSet<ObserverConfiguration>(new ObComparator());
        obTree1.addAll(world.getObservers());
        Iterator<ObserverConfiguration> observers1 = obTree1.iterator();
        TreeSet<ObserverConfiguration> obTree2 = new TreeSet<ObserverConfiguration>(new ObComparator());
        obTree2.addAll(po.getObservers());
        Iterator<ObserverConfiguration> observers2 = obTree2.iterator();
        while (observers1.hasNext()) {
            ObserverConfiguration observ1 = observers1.next();
            ObserverConfiguration observ2 = observers2.next();
            compareObservers(observ1, observ2);
        }

        TreeSet<ModificatorConfiguration> modTree1 = new TreeSet<ModificatorConfiguration>(new ModComparator());
        modTree1.addAll(world.getModificators());
        Iterator<ModificatorConfiguration> modifiers1 = modTree1.iterator();
        TreeSet<ModificatorConfiguration> modTree2 = new TreeSet<ModificatorConfiguration>(new ModComparator());
        modTree2.addAll(po.getModificators());
        Iterator<ModificatorConfiguration> modifiers2 = modTree2.iterator();
        while (modifiers1.hasNext()) {
            ModificatorConfiguration mod1 = modifiers1.next();
            ModificatorConfiguration mod2 = modifiers2.next();
            compareModifiers(mod1, mod2);
        }


        TreeSet<WorldSideConfiguration> sideTree1 = new TreeSet<WorldSideConfiguration>(new WSComparator());
        sideTree1.addAll(world.getNotWindySides());
        Iterator<WorldSideConfiguration> sides1 = sideTree1.iterator();
        TreeSet<WorldSideConfiguration> sidesTree2 = new TreeSet<WorldSideConfiguration>(new WSComparator());
        sidesTree2.addAll(po.getNotWindySides());
        Iterator<WorldSideConfiguration> sides2 = sidesTree2.iterator();
        while (sides1.hasNext()) {
            WorldSideConfiguration side1 = sides1.next();
            WorldSideConfiguration side2 = sides2.next();
            compareWorldSide(side1, side2);
        }


    }

    private void compareObservers(ObserverConfiguration observ1, ObserverConfiguration observ2) {
        assertEquals(observ1.getId(), observ2.getId());
        assertEquals(observ1.getSize(), observ2.getSize());
        assertEquals(observ1.getPosition().getX(), observ2.getPosition().getX());
        assertEquals(observ1.getPosition().getY(), observ2.getPosition().getY());
        assertEquals(observ1.getPosition().getZ(), observ2.getPosition().getZ());

        if (observ1.getOrientation() != null || observ2.getOrientation() != null) {
            assertEquals(observ1.getOrientation().getX(), observ2.getOrientation().getX());
            assertEquals(observ1.getOrientation().getY(), observ2.getOrientation().getY());
            assertEquals(observ1.getOrientation().getZ(), observ2.getOrientation().getZ());
        }

        assertEquals(observ1.getParameters(), observ2.getParameters());
        assertEquals(observ1.getType(), observ2.getType());

    }

    private void compareWorldSide(WorldSideConfiguration side1, WorldSideConfiguration side2) {
        assertEquals(side1.getX(), side2.getX());
        assertEquals(side1.getY(), side2.getY());
        assertEquals(side1.getZ(), side2.getZ());
    }


    private void compareModifiers(ModificatorConfiguration mod1, ModificatorConfiguration mod2) {
        assertEquals(mod1.getId(), mod2.getId());
        assertEquals(mod1.getSize(), mod2.getSize());
        assertEquals(mod1.isMoveable(), mod2.isMoveable());
        assertEquals(mod1.getPosition().getX(), mod2.getPosition().getX());
        assertEquals(mod1.getPosition().getY(), mod2.getPosition().getY());
        assertEquals(mod1.getPosition().getZ(), mod2.getPosition().getZ());
        assertEquals(mod1.getOrientationVector().getX(), mod2.getOrientationVector().getX());
        assertEquals(mod1.getOrientationVector().getY(), mod2.getOrientationVector().getY());
        assertEquals(mod1.getOrientationVector().getZ(), mod2.getOrientationVector().getZ());

        assertEquals(mod1.getType(), mod2.getType());
        assertEquals(mod1.getNegative(), mod2.getNegative());
        assertEquals(mod1.getFactor(), mod2.getFactor());

        assertEquals(mod1.getEllipseLength(), mod2.getEllipseLength());
        assertEquals(mod1.isRotated(), mod2.isRotated());
    }

    public WorldConfiguration createWorld() {
        WorldConfiguration world = new WorldConfiguration();
        world.setBitsAmount(123);
        world.setBitSize(0.555f);
        world.setSide(34.23f);
        world.setEnergy(23232323.3f);

        ImpactStrategyConfiguration strategy = new ImpactStrategyConfiguration();
        strategy.setType("Big boom");
        world.setImpactStrategy(strategy);

        world.setStatisticsAccumulatorType("statistics type");

        Set<ObserverConfiguration> observers = new HashSet<ObserverConfiguration>();

        ObserverConfiguration observer = createObserver(1, "Sphere", 1.1f, 1.2f, 3.4f, 11.12f, "tut", "tam", "zdes");
        observers.add(observer);
        observer = createObserver(2, "Cube", 23.1f, 3.33f, 56.23434f, 22.32f, "tut1", "tam1", "zdes1");
        observers.add(observer);
        observer = createObserver(3, "Point", 0.1f, 123.33f, 16.114f, 0.132f, "tut2", "tam2", "zdes2");
        observers.add(observer);
        observer = createSurfaceObserver(4, "Surface", 0.1f, 123.33f, 16.114f, 0.132f, .12f, .23f, 3.4f);
        observers.add(observer);

        world.setObservers(observers);

        Set<ModificatorConfiguration> modifiers = new HashSet<ModificatorConfiguration>();
        ModificatorConfiguration modifier = createModifier(11, true, 3.3f, 1.2f, 3.4f, 5.6f, "z1", 0.01f, true, 0, 1, 0, 11, true);
        modifiers.add(modifier);
        modifier = createModifier(22, false, 2.3f, 0.2f, 0.4f, 34.6f, "a1", 0.561f, false, 0, 0, 1, 0.4f, false);
        modifiers.add(modifier);
        modifier = createModifier(33, true, 1.3f, 9.2f, 45.4f, 3.26f, "f1", 0.21f, true, -1, 0, 0, 0.5f, true);
        modifiers.add(modifier);

        world.setModificators(modifiers);

        world.setNotWindySides(createWorldSides());

        return world;
    }

    private Set<WorldSideConfiguration> createWorldSides() {
        Set<WorldSideConfiguration> sides = new HashSet<WorldSideConfiguration>();
        WorldSideConfiguration side = new WorldSideConfiguration();
        side.setX(3.32323f);
        side.setY(45.3f);
        side.setZ(23);
        sides.add(side);
        return sides;
    }

    public ModificatorConfiguration createModifier(int id, boolean moveable, float size, float posX,
                                                   float posY, float posZ, String type, float factor,
                                                   boolean nagative, float oriX, float oriY, float oriZ,
                                                   float ellipseLength, boolean rotated) {
        ModificatorConfiguration modifier = new ModificatorConfiguration();
        modifier.setId(id);
        modifier.setSize(size);
        Position mposition = new Position();
        mposition.setX(posX);
        mposition.setY(posY);
        mposition.setZ(posZ);
        modifier.setPosition(mposition);
        modifier.setMoveable(moveable);
        modifier.setFactor(factor);
        modifier.setType(type);
        modifier.setNagative(nagative);
        Position orientation = new Position();
        orientation.setX(oriX);
        orientation.setY(oriY);
        orientation.setZ(oriZ);
        modifier.setOrientation(orientation);
        modifier.setEllipseLength(ellipseLength);
        modifier.setRotated(rotated);
        return modifier;
    }

    public ObserverConfiguration createObserver(int id, String type, float size, float posX, float posY, float posZ, String param1, String param2, String param3) {
        ObserverConfiguration observer = new ObserverConfiguration();
        observer.setId(id);
        observer.setSize(size);
        Position position = new Position();
        position.setX(posX);
        position.setY(posY);
        position.setZ(posZ);
        observer.setPosition(position);
        observer.setType(type);
        Set<String> parameters = new HashSet<String>();
        parameters.add(param1);
        parameters.add(param2);
        parameters.add(param3);
        observer.setParameters(parameters);
        return observer;
    }

    public ObserverConfiguration createSurfaceObserver(int id, String type, float size, float posX, float posY, float posZ, float vectorX, float vectorY, float vectorZ) {
        ObserverConfiguration observer = new ObserverConfiguration();
        observer.setId(id);
        observer.setSize(size);
        Position orientation = new Position();
        orientation.setX(vectorX);
        orientation.setY(vectorY);
        orientation.setZ(vectorZ);
        observer.setOrientation(orientation);
        Position position = new Position();
        position.setX(posX);
        position.setY(posY);
        position.setZ(posZ);
        observer.setPosition(position);
        observer.setType(type);
        return observer;
    }

    private class ObComparator implements Comparator<ObserverConfiguration> {
        @Override
        public int compare(ObserverConfiguration o1, ObserverConfiguration o2) {
            return Integer.valueOf(o1.getId()).compareTo(o2.getId());
        }
    }

    private class ModComparator implements Comparator<ModificatorConfiguration> {
        @Override
        public int compare(ModificatorConfiguration o1, ModificatorConfiguration o2) {
            return Integer.valueOf(o1.getId()).compareTo(o2.getId());
        }
    }

    private class WSComparator implements Comparator<WorldSideConfiguration> {
        public int compare(WorldSideConfiguration o1, WorldSideConfiguration o2) {
            return Float.valueOf(1000000 * o1.getX() + 1000 * o1.getY() + o1.getZ()).compareTo(1000000 * o2.getX() + 1000 * o2.getY() + o2.getZ());
        }
    }
}
