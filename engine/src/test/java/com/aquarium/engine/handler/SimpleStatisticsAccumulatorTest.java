package com.aquarium.engine.handler;

import com.aquarium.engine.handler.parameters.AverageVelocityParameter;
import com.aquarium.entities.Bit;
import com.aquarium.entities.support.ObservableParameter;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.List;

/**
 * Created by artem on 04.01.15.
 */
public class SimpleStatisticsAccumulatorTest extends TestCase {

    @Test
    public void test() {
        SimpleStatisticsAccumulator sink = new SimpleStatisticsAccumulator();
        ObservableParameter parameter = new AverageVelocityParameter("test");
        Bit bit = new Bit(1);
        int count = 0;
        for (double i = 0; i < 10; i = i + 0.01f) {
            bit.setVelocity((float)i/10);
            parameter.consider(bit);
            sink.saveParameter(1, (float)i, parameter);
            count ++;
        }
        List testResult = sink.getParameterValues(1, "test");
        assertTrue(testResult.size() == 11);
    }


}
