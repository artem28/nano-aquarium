package com.aquarium.engine.handler;

import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.entities.support.ParameterValue;
import com.aquarium.entities.support.StatisticsAccumulator;

import java.util.List;

/**
 * Created by artem on 7/19/14.
 */
public class StubbedDataSink implements StatisticsAccumulator {
    @Override
    public void saveParameters(int id, float time, List<ObservableParameter> parameters) {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
    }

    @Override
    public void saveParameter(int id, float time, ObservableParameter parameter) {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
    }

    @Override
    public void parametersInfo(List<ObservableParameter> parameters) {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
    }

    @Override
    public List<ParameterValue> getParameterValues(int id, String name) {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
        return null;
    }
}
