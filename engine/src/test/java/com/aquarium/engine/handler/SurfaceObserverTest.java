package com.aquarium.engine.handler;

import com.aquarium.entities.Bit;
import com.aquarium.entities.BitsIndex;
import com.aquarium.entities.PipeVector;
import com.aquarium.entities.Point;
import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.math.scale.BitsCubeIndexFactory;
import junit.framework.TestCase;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by artem on 10/12/14.
 */
public class SurfaceObserverTest extends TestCase {

    public void test() {
        final Set<Bit> bitsInSurface = new HashSet<Bit>();
        SurfaceObserver observer = new SurfaceObserver(1, new PipeVector(new Point(1, 0, 0), -1, 0, 0), 1);
        observer.addParameter(new ObservableParameter() {
            @Override
            public void consider(Bit bit) {
                bitsInSurface.add(bit);
            }

            @Override
            public Object provideParameterValue() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public Object getPrevious() {
                return null;
            }
        });

        BitsCubeIndexFactory factory = new BitsCubeIndexFactory(0.1f, 100);
        factory.addIndex(1);
        BitsIndex bitsIndex = factory.createComplexIndex();
        fillIndex(bitsIndex);

        observer.observe(bitsIndex.findAcceptable(observer));

        assertEquals(3, bitsInSurface.size());
        assertTrue(bitsInSurface.contains(new Bit(1)));
        assertTrue(bitsInSurface.contains(new Bit(2)));
        assertTrue(bitsInSurface.contains(new Bit(3)));
    }

    private void fillIndex(BitsIndex bitsIndex) {
        Bit bit5 = new Bit(5);
        bit5.setTime(0);
        bit5.setVelocity((float)Math.sqrt(2));
        PipeVector pipe5 = new PipeVector();
        pipe5.setFactorXYZ(0, 1, -1);
        Point point5 = new Point();
        point5.setX(1.51f);
        point5.setY(5f / 7f);   //0.714285714
        point5.setZ(11f / 3f);  //3.666666667
        pipe5.setOffset(point5);
        bit5.setPipe(pipe5);

        Bit bit4 = new Bit(4);
        bit4.setTime(0);
        bit4.setVelocity((float)Math.sqrt(2));
        PipeVector pipe4 = new PipeVector();
        pipe4.setFactorXYZ(0, 1, -1);
        Point point4 = new Point();
        point4.setX(0.49f);
        point4.setY(5f / 7f);   //0.714285714
        point4.setZ(11f / 3f);  //3.666666667
        pipe4.setOffset(point4);
        bit4.setPipe(pipe4);

        Bit bit3 = new Bit(3);
        bit3.setTime(0);
        bit3.setVelocity((float)Math.sqrt(2));
        PipeVector pipe3 = new PipeVector();
        pipe3.setFactorXYZ(0, 1, -1);
        Point point3 = new Point();
        point3.setX(11f / 9f);  //1.222222222
        point3.setY(5f / 7f);   //0.714285714
        point3.setZ(11f / 3f);  //3.666666667
        pipe3.setOffset(point3);
        bit3.setPipe(pipe3);

        Bit bit2 = new Bit(2);
        bit2.setTime(0);
        bit2.setVelocity((float)Math.sqrt(2));
        PipeVector pipe2 = new PipeVector();
        pipe2.setFactorXYZ(0, 1, 1);
        Point point2 = new Point();
        point2.setX(5f / 7f);  //0.714285714
        point2.setY(3f / 8f);  //0.375
        point2.setZ(7f / 11f); //0.636363636
        pipe2.setOffset(point2);
        bit2.setPipe(pipe2);

        Bit bit1 = new Bit(1);
        bit1.setTime(0);
        bit1.setVelocity((float)Math.sqrt(2));
        PipeVector pipe1 = new PipeVector();
        pipe1.setFactorXYZ(0, 1, -1);
        Point point1 = new Point();
        point1.setX(5f / 9f);   //0.555555556
        point1.setY(11f / 7f);  //1.571428571
        point1.setZ(7f / 3f);   //2.333333333
        pipe1.setOffset(point1);
        bit1.setPipe(pipe1);

        bitsIndex.addBit(bit1, bit1.getPipe().getOffset());
        bitsIndex.addBit(bit2, bit2.getPipe().getOffset());
        bitsIndex.addBit(bit3, bit3.getPipe().getOffset());
        bitsIndex.addBit(bit4, bit4.getPipe().getOffset());
        bitsIndex.addBit(bit5, bit5.getPipe().getOffset());
    }
}
