package com.aquarium.engine.impact;

import com.aquarium.entities.Bit;
import com.aquarium.math.BitsCan;
import com.aquarium.math.DoubleCan;
import junit.framework.TestCase;

/**
 * Created by artem on 4/27/14.
 */
public class SimpleImpactStrategyTest extends TestCase {

    public void testImpact() {
        SimpleImpactStrategy impact = new SimpleImpactStrategy(1);
        Bit bit1 = new Bit(1);
        bit1.setVelocity(1);
        bit1.getPipe().setFactorXYZ(1, 1, 1);
        Bit bit2 = new Bit(2);
        bit2.setVelocity(1);
        bit2.getPipe().setFactorXYZ(-1, -1, -1);
        impact.doImpact(bit1, bit2);

        Bit bit11 = new Bit(1);
        bit11.setVelocity(1);
        bit11.getPipe().setFactorXYZ(1, 1, 1);

        Bit bit22 = new Bit(2);
        bit22.setVelocity(1);
        bit22.getPipe().setFactorXYZ(-1, -1, -1);

        assertTrue(BitsCan.areEqual(bit1, bit11));
        assertTrue(BitsCan.areEqual(bit2, bit22));
    }

    public void testImpact2() {
        SimpleImpactStrategy impact = new SimpleImpactStrategy(1);
        Bit bit1 = new Bit(1);
        bit1.setVelocity(1);
        bit1.getPipe().setFactorXYZ(1, 1, 1);
        Bit bit2 = new Bit(2);
        bit2.setVelocity(0);
        bit2.getPipe().setFactorXYZ(0, 0, 0);
        impact.doImpact(bit1, bit2);

        Bit bit11 = new Bit(1);
        bit11.setVelocity(1);
        bit11.getPipe().setFactorXYZ(1, 1, 1);

        Bit bit22 = new Bit(2);
        bit22.setVelocity(0);
        bit22.getPipe().setFactorXYZ(0, 0, 0);

        assertTrue(BitsCan.areEqual(bit1, bit11));
        assertTrue(BitsCan.areEqual(bit2, bit22));

    }

    public void testImpact3() {
        SimpleImpactStrategy impact = new SimpleImpactStrategy(1);
        Bit bit1 = new Bit(1);
        bit1.setVelocity(1);
        bit1.getPipe().setFactorXYZ(1, 1, 0);
        Bit bit2 = new Bit(2);
        bit2.setVelocity(1);
        bit2.getPipe().setFactorXYZ(-1, 1, 0);
        impact.doImpact(bit1, bit2);

        Bit bit11 = new Bit(1);
        bit11.setVelocity(1);
        bit11.getPipe().setFactorXYZ(1, 1, 0);

        Bit bit22 = new Bit(2);
        bit22.setVelocity(1);
        bit22.getPipe().setFactorXYZ(-1, 1, 0);

        assertTrue(BitsCan.areEqual(bit1, bit22));
        assertTrue(BitsCan.areEqual(bit2, bit11));

    }

    public void testImpact4() {
        SimpleImpactStrategy impact = new SimpleImpactStrategy(1);
        Bit bit1 = new Bit(1);
        bit1.setVelocity(1);
        bit1.getPipe().setFactorXYZ(1, 0, 0);
        Bit bit2 = new Bit(2);
        bit2.setVelocity(0.5f);
        bit2.getPipe().setFactorXYZ(0, 1, 0);
        impact.doImpact(bit1, bit2);

        assertTrue(DoubleCan.areEqual(bit1.getVelocity(), bit2.getVelocity()));
        assertTrue(DoubleCan.areEqual(bit1.getVelocity(),
                (float)Math.sqrt(0.5f + 0.125f)));
    }

    public void testImpact5() {
        SimpleImpactStrategy impact = new SimpleImpactStrategy(1);
        Bit bit1 = new Bit(1);
        bit1.setVelocity(1);
        bit1.getPipe().setFactorXYZ(1, 0, 1);
        Bit bit2 = new Bit(2);
        bit2.setVelocity(1);
        bit2.getPipe().setFactorXYZ(0, 1, 0);
        impact.doImpact(bit1, bit2);
        impact.doImpact(bit1, bit2);

        Bit bit11 = new Bit(1);
        bit11.setVelocity(1);
        bit11.getPipe().setFactorXYZ(1, 0, 1);

        Bit bit22 = new Bit(2);
        bit22.setVelocity(1);
        bit22.getPipe().setFactorXYZ(0, 1, 0);

        assertTrue(BitsCan.areEqual(bit1, bit11));
        assertTrue(BitsCan.areEqual(bit2, bit22));

    }

}
