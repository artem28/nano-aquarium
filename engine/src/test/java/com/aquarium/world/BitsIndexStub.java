package com.aquarium.world;

import com.aquarium.entities.Bit;
import com.aquarium.entities.BitsIndex;
import com.aquarium.entities.Body;
import com.aquarium.entities.Point;

import java.util.Iterator;

/**
 * Created by artem on 7/21/14.
 */
public class BitsIndexStub implements BitsIndex {
    @Override
    public void addBit(Bit bit, Point point) {

    }

    @Override
    public void addBits(BitsIndex bits) {

    }

    @Override
    public Iterable<Bit> getBitSetInX(float from, float to) {
        return null;
    }

    @Override
    public Iterable<Bit> getBitSetInY(float from, float to) {
        return null;
    }

    @Override
    public Iterable<Bit> getBitSetInZ(float from, float to) {
        return null;
    }

    @Override
    public Iterable<Bit> findAcceptable(Body body) {
        return null;
    }

    @Override
    public Iterable<Bit> getCloseBits(Bit bit1, float bitsize) {
        return null;
    }

    @Override
    public Iterable<Bit> getCloseBits(Bit bit1, float bitsize, int completeFast) {
        return null;
    }

    @Override
    public Iterator<Bit> iterator() {
        return null;
    }

    @Override
    public void startIndexModification() {
    }

    @Override
    public void endOfIndexModification() {
    }

    @Override
    public void modifyBit(Bit bit) {

    }
}
