package com.aquarium.world;

import com.aquarium.entities.Bit;
import com.aquarium.entities.PipeVector;
import com.aquarium.entities.Point;
import junit.framework.TestCase;

/**
 * Created by artem on 4/27/14.
 */
public class CubeWorldTest extends TestCase {

    public void testInSide() {
        CubeWorld world = new CubeWorld(100, 1);

        assertTrue(world.isInside(new Point(1, 1, 1)));
        assertTrue(!world.isInside(new Point(51, 1, 1)));
        assertTrue(!world.isInside(new Point(1, 51, 1)));
        assertTrue(!world.isInside(new Point(1, 1, 51)));

        assertTrue(!world.isInside(new Point(1, 1, -51)));
        assertTrue(!world.isInside(new Point(1, -51, 1)));
        assertTrue(!world.isInside(new Point(-51, 1, 1)));

        assertTrue(world.isInside(new Point(-49, 1, 1)));
        assertTrue(world.isInside(new Point(1, -49, 1)));
        assertTrue(world.isInside(new Point(1, 1, -49)));

        assertTrue(world.isInside(new Point(1, 1, 49)));
        assertTrue(world.isInside(new Point(1, 49, 1)));
        assertTrue(world.isInside(new Point(49, 1, 1)));
    }

    public void testReturnBitBack() {
        CubeWorld world = new CubeWorld(100, 1);
        world.setBlackBox(new WorldEnergyCounter(1));
        Bit bit1 = new Bit(1);
        bit1.setVelocity(1);
        PipeVector pipe = new PipeVector(new Point(51, 2, 2), 1, 1, 1);
        bit1.setPipe(pipe);
        float time = 0;
        while (true) {
            if (!world.isInside(bit1.getPipe().getOffset())) {
                world.returnBitBack(bit1);
            }
            time = time + 10;
            bit1.move(time);
            if (world.isInside(bit1.getPipe().getOffset())) {
                break;
            }
        }
        assertTrue(world.isInside(bit1.getPipe().getOffset()));


        bit1 = new Bit(1);
        bit1.setVelocity(1);
        pipe = new PipeVector(new Point(-51, 2, 2), -1, 1, 1);
        bit1.setPipe(pipe);
        time = 0;

        while (true) {
            if (!world.isInside(bit1.getPipe().getOffset())) {
                world.returnBitBack(bit1);
            }
            time += 10;
            bit1.move(time);
            if (world.isInside(bit1.getPipe().getOffset())) {
                break;
            }
        }
        assertTrue(world.isInside(bit1.getPipe().getOffset()));

        bit1 = new Bit(1);
        bit1.setVelocity(1);
        pipe = new PipeVector(new Point(-51, 51, 2), -1, 1, 1);
        bit1.setPipe(pipe);
        while (true) {
            if (!world.isInside(bit1.getPipe().getOffset())) {
                world.returnBitBack(bit1);
            }
            time += 10;
            bit1.move(time);
            if (world.isInside(bit1.getPipe().getOffset())) {
                break;
            }
        }
        assertTrue(world.isInside(bit1.getPipe().getOffset()));
    }

    public void testReturnBitBackWithNotWindyWorldSide() {
        CubeWorld world = new CubeWorld(100, 1);
        world.setBlackBox(new WorldEnergyCounter(1));
        world.addNotWindySide(new Point(0, 0, -1));
        Bit bit1 = new Bit(1);
        bit1.setVelocity(1);
        PipeVector pipe = new PipeVector(new Point(2, 2, -51), 1, 1, 1);
        bit1.setPipe(pipe);
        world.replaceBits(new BitsIndexStub());
        float time = 0;
        while (true) {
            if (!world.isInside(bit1.getPipe().getOffset())) {
                world.returnBitBack(bit1);
            }
            time += 10;
            bit1.move(time);
            if (world.isInside(bit1.getPipe().getOffset())) {
                break;
            }
        }
        assertTrue(world.isInside(bit1.getPipe().getOffset()));
        assertTrue(bit1.getPipe().getOffset().getZ() > 30);
    }
}
