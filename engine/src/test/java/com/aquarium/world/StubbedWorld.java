package com.aquarium.world;

import com.aquarium.entities.*;

import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

/**
 * Created by artem on 7/19/14.
 */
public class StubbedWorld implements World {
    @Override
    public boolean isInside(Point point) {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
        return false;
    }

    @Override
    public float getSide() {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
        return 0;
    }

    @Override
    public float getAverageBitEnergy() {
        return 0;
    }

    @Override
    public Point randomPoint() {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
        return null;
    }

    @Override
    public BitsIndex freeMove(double time, Iterable<Bit> bits) {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
        return null;
    }

    @Override
    public WorldBlackBox getBlackBox() {
        return new StubbedWorldBlackBox();
    }

    @Override
    public void returnBitBack(Bit bit) {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
    }

    @Override
    public void setImpactStrategy(ImpactStrategy strategy) {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
    }

    @Override
    public ImpactStrategy getImpactStrategy() {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
        return null;
    }

    @Override
    public void createBits(int size, float energy) {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
    }

    @Override
    public void addObserver(Observer observer) {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
    }

    @Override
    public void addModificator(Modificator observer) {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
    }

    @Override
    public double getTime() {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
        return 0;
    }

    public void bitsMovementFinished() {
    }

    public void bitsMovementStarted() {
    }


    @Override
    public BitsIndex getBits() {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
        return null;
    }

    @Override
    public void replaceBits(Iterable<Bit> bits) {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
    }

    @Override
    public void addTime(float dt) {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
    }

    @Override
    public void flushObservers() {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
    }

    @Override
    public float getBitSize() {
        return 0.1f;
    }

    @Override
    public BitsIndex generateIndex() {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
        return null;
    }

    @Override
    public Point positionOnWorldBoundary(Bit bit) {
        return null;
    }

    @Override
    public Iterable<Bit> getBitsFromOutside() {
        if (true) {
            throw new RuntimeException("NotSupported method");
        }
        return null;
    }

    @Override
    public Set<Observer> getObservers() {
        return null;
    }

    @Override
    public SortedSet<Modificator> getModificators() {
        return null;
    }

    @Override
    public void addNotWindySide(Point side) {

    }

    @Override
    public int getBitsAmount() {
        return 0;
    }

    @Override
    public boolean isNeedToBeRestarted() {
        return false;
    }

    @Override
    public void needToBeRestarted(boolean needToBeRestarted) {

    }

    @Override
    public boolean stopTheWorld() {
        return false;
    }

    @Override
    public void setOutCommers(Set<Bit> outCommers) {

    }

    @Override
    public Set<Bit> getOutCommers() {
        return null;
    }
}
