package com.aquarium.world;

import com.aquarium.entities.Bit;
import com.aquarium.entities.WorldBlackBox;

import java.util.Iterator;

/**
 * Created by artem on 7/26/14.
 */
public class StubbedWorldBlackBox implements WorldBlackBox {

    @Override
    public void addEnergy(float value) {

    }

    @Override
    public float getEnergy() {
        return 0;
    }

    @Override
    public void removeEnergy(float value) {

    }

    @Override
    public void addBit(Bit bit) {

    }

    @Override
    public int getBitsSize() {
        return 0;
    }

    @Override
    public Bit pullBit() {
        return null;
    }

    @Override
    public int getAverageTrapBandwidth() {
        return 0;
    }

    @Override
    public void calculateStatistics() {

    }
}
