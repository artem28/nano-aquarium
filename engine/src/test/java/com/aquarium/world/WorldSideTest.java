package com.aquarium.world;

import com.aquarium.entities.Point;
import junit.framework.TestCase;

/**
 * Created by artem on 10/1/14.
 */
public class WorldSideTest extends TestCase {

    public void test1() {
        WorldSide ws = new WorldSide(new Point(1.3454323423452345f, 0, 0));
        assertTrue(ws.isSide(new Point(45454.45434634563456f, 0, 0)));
        for (int i = 0; i < 100; i++) {
            assertTrue(ws.isSide(new Point((float)Math.random()+0.00001f, 0, 0)));
        }
        assertFalse(ws.isSide(new Point(0, 34434.456456f, 0)));
        try {
            assertFalse(ws.isSide(new Point(0, 34434.456456f, 0.00034534f)));
            fail();
        } catch (Exception e) {

        }
        try {
            assertFalse(ws.isSide(new Point(0, 0, 0)));
            fail();
        } catch (Exception e) {

        }
    }

    public void test2() {
        WorldSide ws = new WorldSide(new Point(0,-54545454.454545454f,  0));
        assertFalse(ws.isSide(new Point(45454.45434634563456f, 0, 0)));
        assertTrue(ws.isSide(new Point(0,-4.56545645745f, 0)));
    }

}
