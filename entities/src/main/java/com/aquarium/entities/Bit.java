package com.aquarium.entities;

/**
 * Created by artem on 3/31/14.
 */
public class Bit {
    PipeVector pipe = new PipeVector();
    Integer id;
    float time = 0;
    float velocity = 0;
    Bit lastImpact = null;

    public Bit(int identy) {
        id = identy;
    }

    public int getId() {
        return id.intValue();
    }

    public Integer getIdLong() {
        return id;
    }

    public PipeVector getPipe() {
        return pipe;
    }

    public void setPipe(PipeVector pipe) {
        this.pipe = pipe;
    }

    public float getTime() {
        return time;
    }

    public void setTime(float time) {
        this.time = time;
    }

    public float getVelocity() {
        return velocity;
    }

    public void setVelocity(float velocity) {
        this.velocity = Math.abs(velocity);
    }

    public Bit getLastImpact() {
        return lastImpact;
    }

    public void setLastImpact(Bit lastImpact) {
        this.lastImpact = lastImpact;
    }

    public void move(float time) {
        Bit bit = this;
        bit.getPipe().setOffset(new Point(
                bit.getPipe().getOffset().getX() + bit.getPipe().getFactorX() * bit.getVelocity() * (time - getTime()),
                bit.getPipe().getOffset().getY() + bit.getPipe().getFactorY() * bit.getVelocity() * (time - getTime()),
                bit.getPipe().getOffset().getZ() + bit.getPipe().getFactorZ() * bit.getVelocity() * (time - getTime())
        ));
        bit.setTime(time);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && ((Bit) obj).id.longValue() == id.longValue();
    }

    @Override
    public String toString() {
        return "Bit id=" + id + ", velocity=" + velocity + ", time=" + time + ", pipe=" + pipe +
                ", last impact bit id=" + (lastImpact != null ? lastImpact.getId() : "null");
    }
}
