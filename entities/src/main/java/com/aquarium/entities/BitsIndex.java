package com.aquarium.entities;

import java.util.Iterator;

/**
 * Created by artem on 6/9/14.
 */
public interface BitsIndex extends Iterable<Bit> {
    void addBit(Bit bit, Point point);

    void addBits(BitsIndex bits);

    Iterable<Bit> getBitSetInX(float from, float to);

    Iterable<Bit> getBitSetInY(float from, float to);

    Iterable<Bit> getBitSetInZ(float from, float to);

    Iterable<Bit> findAcceptable(Body body);

    Iterable<Bit> getCloseBits(Bit bit1, float bitsize);

    Iterable<Bit> getCloseBits(Bit bit1, float bitsize, int completeFast);

    @Override
    Iterator<Bit> iterator();

    void modifyBit(Bit bit);

    void startIndexModification();

    void endOfIndexModification();
}
