package com.aquarium.entities;

/**
 * Created by artem on 5/15/14.
 */
public interface Body {

    /**
     * is the Bit inside the Body?
     * @param bit
     * @return
     */
    boolean isAcceptable(Bit bit);

    /**
     * returns the body center
     * @return
     */
    public Point getPosition();

    /**
     * returns size of the body (cube side length or sphere diameter)
     * @return
     */
    public float getSize();

    int getId();
}
