package com.aquarium.entities;

import java.util.Collection;

/**
 * Created by artem on 4/10/14.
 */
public class CubeWorldObserver implements WorldObserver {

    private double sumVelocity = 0;
    private long bitCount = 0;

    private Point position;
    private double side;

    public CubeWorldObserver(double side, Point position) {
        this.side = side;
        this.position = position;
    }

    @Override
    public void observe(Collection<Bit> bits) {
        double a2 = side / 2;
        sumVelocity = 0;
        bitCount = 0;

        for (Bit bit : bits) {
            Point bitPoint = bit.getPipe().getOffset();
            if (bitPoint.getX() > position.getX() - a2 &&
                    bitPoint.getX() < position.getX() + a2 &&
                    bitPoint.getY() > position.getY() - a2 &&
                    bitPoint.getY() < position.getY() + a2 &&
                    bitPoint.getZ() > position.getZ() - a2 &&
                    bitPoint.getZ() < position.getZ() + a2
                    ) {
                sumVelocity += bit.getVelocity();
                bitCount++;
            }
        }
    }

    public double getAverageVelocity() {
        return sumVelocity / bitCount;
    }

    public long getBitCount() {
        return bitCount;
    }

    public double getDensity() {
        return bitCount / (Math.pow(side, 3));
    }
}
