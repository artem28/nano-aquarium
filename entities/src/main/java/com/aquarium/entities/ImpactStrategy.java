package com.aquarium.entities;

/**
 * Created by artem on 4/11/14.
 */
public interface ImpactStrategy {
    void doImpact(Bit bit1, Bit bit2);

    boolean isEligableForImpact(Bit candidate1, Bit candidate2);

    boolean checkImpact(Bit bit1, Bit bit2, double dt);
}
