package com.aquarium.entities;

/**
 * Created by artem on 30.03.15.
 */
public interface IndexFactory {

    BitsIndex createComplexIndex();

    IndexFactory addIndex(float cellSize);
}
