package com.aquarium.entities;

import com.aquarium.entities.support.ParametersHolder;
import com.sun.org.apache.regexp.internal.recompile;

import java.util.Collection;
import java.util.Map;

/**
 * Created by artem on 5/11/14.
 */
public interface Modificator extends Body , Comparable, ParametersHolder{

    void observe(float dt, World world, Iterable<Bit> bits);

    void flush(float time);

    void postAction();

    Point getImpuls();

    boolean isMoveable();

    void setPosition(Point point);
}
