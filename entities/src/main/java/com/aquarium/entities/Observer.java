package com.aquarium.entities;

import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.entities.support.ParametersHolder;

import java.util.Collection;
import java.util.List;

/**
 * Created by artem on 5/8/14.
 */
public interface Observer extends Body, ParametersHolder {

    void observe( Iterable<Bit> bits);

    void flush(float time);

    void addParameter(ObservableParameter param);

    public List<ObservableParameter> getParameters();
}
