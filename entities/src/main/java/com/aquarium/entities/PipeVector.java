package com.aquarium.entities;


/**
 * Created by artem on 3/31/14.
 */
public class PipeVector {

    Point offset = new Point();

    float factorX = 1;
    float factorY = 1;
    float factorZ = 1;

    public PipeVector(Point offset, float factorX, float factorY, float factorZ) {
        this.offset = offset;
        this.factorX = factorX;
        this.factorY = factorY;
        this.factorZ = factorZ;
        normalize();
    }

    public PipeVector() {
    }

    public Point getOffset() {
        return offset;
    }

    public void setOffset(Point offset) {
        this.offset = offset;
    }

    public float getFactorX() {
        return factorX;
    }

    public float getFactorY() {
        return factorY;
    }

    public float getFactorZ() {
        return factorZ;
    }

    public void setFactorXYZ(float factorX, float factorY, float factorZ) {
        this.factorX = factorX;
        this.factorY = factorY;
        this.factorZ = factorZ;
        normalize();
    }

    private void normalize() {
        float norm = (float)Math.sqrt(Math.pow(factorX, 2) + Math.pow(factorY, 2) + Math.pow(factorZ, 2));
        if (Math.abs(norm) < 0.000001) {
            this.factorX = 0;
            this.factorY = 0;
            this.factorZ = 0;
            return;
        }
        this.factorX = this.factorX / norm;
        this.factorY = this.factorY / norm;
        this.factorZ = this.factorZ / norm;
    }

    @Override
    public String toString() {
        return "PipeVector{" +
                "offset=" + offset +
                ", factorX=" + factorX +
                ", factorY=" + factorY +
                ", factorZ=" + factorZ +
                '}';
    }
}
