package com.aquarium.entities;

/**
 * Created by artem on 3/31/14.
 */
public class Point {

    Float x = new Float(0);
    Float y = new Float(0);
    Float z = new Float(0);

    public Point() {
    }

    public Point(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public Float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public Float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    @Override
    public String toString() {
        return "[ " + x +
                " , " + y +
                " , " + z + " ]";
    }
}
