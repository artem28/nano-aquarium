package com.aquarium.entities;

/**
 * Created by artem on 8/14/14.
 */
public interface SurfaceBody extends Body {

    PipeVector getSurfaceOrientation();

}
