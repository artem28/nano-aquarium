package com.aquarium.entities;

import java.util.Set;
import java.util.SortedSet;

/**
 * Created by artem on 3/31/14.
 */
public interface World {

    boolean isInside(Point point);

    float getSide();

    float getAverageBitEnergy();

    Point randomPoint();

    BitsIndex freeMove(double time, Iterable<Bit> bits);

    WorldBlackBox getBlackBox();

    void returnBitBack(Bit bit);

    void setImpactStrategy(ImpactStrategy strategy);

    ImpactStrategy getImpactStrategy();

    void createBits(int size, float energy);

    void addObserver(Observer observer);

    void addModificator(Modificator observer);

    double getTime();

    BitsIndex getBits();

    void bitsMovementFinished();

    void bitsMovementStarted();

    void replaceBits(Iterable<Bit> bits);

    void addTime(float dt);

    void flushObservers();

    float getBitSize();

    BitsIndex generateIndex();

    Point positionOnWorldBoundary(Bit bit);

    Iterable<Bit> getBitsFromOutside();

    public Set<Observer> getObservers();

    public SortedSet<Modificator> getModificators();

    public void addNotWindySide(Point side);

    int getBitsAmount();

    boolean isNeedToBeRestarted();

    void needToBeRestarted(boolean needToBeRestarted);

    boolean stopTheWorld();

    public Set<Bit> getOutCommers();

    public void setOutCommers(Set<Bit> outCommers);

}