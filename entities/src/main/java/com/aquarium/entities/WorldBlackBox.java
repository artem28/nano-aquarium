package com.aquarium.entities;

import java.util.Iterator;

/**
 * Created by artem on 7/5/14.
 */
public interface WorldBlackBox {
    void addEnergy(float value);

    float getEnergy();

    void removeEnergy(float value);

    void addBit(Bit bit);

    int getBitsSize();

    Bit pullBit();

    int getAverageTrapBandwidth();

    void calculateStatistics();
}
