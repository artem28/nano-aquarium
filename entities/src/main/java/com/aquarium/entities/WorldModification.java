package com.aquarium.entities;

/**
 * Created by artem on 5/12/14.
 */
public class WorldModification {

    double energy = 0;

    public void addEnergy(double value) {
        energy = energy + value;
    }

    public void setEnergy(double value) {
        energy = value;
    }

    public double getEnergy() {
        return energy;
    }
}
