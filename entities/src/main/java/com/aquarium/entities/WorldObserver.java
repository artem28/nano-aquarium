package com.aquarium.entities;

import java.util.Collection;

/**
 * Created by artem on 4/10/14.
 */
public interface WorldObserver {

    void observe(Collection<Bit> bit);

    public double getAverageVelocity();

    public long getBitCount();

    public double getDensity();

}
