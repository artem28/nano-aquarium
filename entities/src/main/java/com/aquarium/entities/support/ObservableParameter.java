package com.aquarium.entities.support;

import com.aquarium.entities.Bit;

/**
 * Created by artem on 5/8/14.
 */
public interface ObservableParameter<T> {
    /**
     * need to be synchronized
     *
     * @param bit
     */
    void consider(Bit bit);

    /**
     * need to be synchronized
     *
     * @return
     */
    T provideParameterValue();

    /**
     * return name
     * @return
     */
    String getName();

    T getPrevious();
}
