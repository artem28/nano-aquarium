package com.aquarium.entities.support;

/**
 * Created by artem on 01.01.15.
 */
public class ParameterValue <T>{

    private T value;
    private float time;

    public ParameterValue(float time, T value) {
        this.time = time;
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public float getTime() {
        return time;
    }

}
