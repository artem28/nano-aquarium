package com.aquarium.entities.support;

import java.util.List;

/**
 * Created by artem on 01.01.15.
 */
public interface ParametersHolder {

    List<String> getParametersNames();

    List<ParameterValue> getParameterValues(String name);
}
