package com.aquarium.entities.support;

import java.util.List;

/**
 * Created by artem on 5/8/14.
 */
public interface StatisticsAccumulator {
    void saveParameters(int id, float time, List<ObservableParameter> parameters);

    void saveParameter(int id, float time, ObservableParameter parameter);

    void parametersInfo(List<ObservableParameter> parameters);

    List<ParameterValue> getParameterValues (int id, String name) ;
}
