package com.aquarium.entities.support;

import com.aquarium.entities.World;

/**
 * Created by artem on 8/31/14.
 */
public interface WorldFactory {

    public World createWorld();

}
