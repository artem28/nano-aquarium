package com.aquarium.math;

import com.aquarium.entities.Bit;
import com.aquarium.entities.Distance;
import com.aquarium.entities.PipeVector;

/**
 * Created by artem on 3/31/14.
 */
public class BitsCan {

    static public Distance calculateIntersectionDistance(PipeVector pipe1, PipeVector pipe2) {
        return null;
    }

    static public float getTimeClosestDistance(Bit bit1, Bit bit2) {
        if (DoubleCan.areEqual(bit1.getTime(), bit2.getTime())) {
            return PipesCan.getTimeClosestDistance(bit1.getPipe(), bit1.getVelocity(), bit2.getPipe(), bit2.getVelocity()) + bit1.getTime();
        } else {
            float dt = bit1.getTime() - bit2.getTime();
            PipeVector shiftedBit1Pipe = PipesCan.makeTimeShift(bit1.getPipe(), -dt, bit1.getVelocity());
            return PipesCan.getTimeClosestDistance(shiftedBit1Pipe, bit1.getVelocity(), bit2.getPipe(), bit2.getVelocity()) + bit1.getTime();
        }
    }

    static public float getDistance(Bit bit1, Bit bit2, float t) {
        if (DoubleCan.areEqual(bit1.getTime(), bit2.getTime())) {
            return PipesCan.getDistance(bit1.getPipe(), bit1.getVelocity(), bit2.getPipe(), bit2.getVelocity(), t - bit1.getTime());
        } else {
            float dt = bit1.getTime() - bit2.getTime();
            PipeVector shiftedBit1Pipe = PipesCan.makeTimeShift(bit1.getPipe(), -dt, bit1.getVelocity());
            return PipesCan.getDistance(shiftedBit1Pipe, bit1.getVelocity(), bit2.getPipe(), bit2.getVelocity(), t - bit1.getTime());
        }
    }


    static public boolean areEqual(Bit b1, Bit b2) {
        return PipesCan.areEqual(b1.getPipe(), b2.getPipe()) &&
                DoubleCan.areEqual(b1.getTime(), b2.getTime()) &&
                DoubleCan.areEqual(b1.getVelocity(), b2.getVelocity());
    }
}
