package com.aquarium.math;

/**
 * Created by artem on 4/27/14.
 */
public class DoubleCan {

    static private double ERROR = 0.000001;

    static public boolean areEqual(float d1, float d2) {
        return Math.abs(d1 - d2) < ERROR;
    }

    static public boolean firstIsLessThenSecond(float d1, float d2) {
        return d2 - d1 > ERROR;
    }

    static public boolean firstIsMoreThenSecond(float d1, float d2) {
        return d1 - d2 > ERROR;
    }
}
