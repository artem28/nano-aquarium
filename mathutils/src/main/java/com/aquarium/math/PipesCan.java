package com.aquarium.math;

import com.aquarium.entities.PipeVector;
import com.aquarium.entities.Point;

public class PipesCan {

    static public float getTimeClosestDistance(PipeVector pipe1, double v1, PipeVector pipe2, double v2) {
        double nn = pipe2.getFactorZ();
        double ll = pipe2.getFactorX();
        double mm = pipe2.getFactorY();

        double n = pipe1.getFactorZ();
        double l = pipe1.getFactorX();
        double m = pipe1.getFactorY();

        double z1 = pipe1.getOffset().getZ();
        double x1 = pipe1.getOffset().getX();
        double y1 = pipe1.getOffset().getY();

        double z2 = pipe2.getOffset().getZ();
        double x2 = pipe2.getOffset().getX();
        double y2 = pipe2.getOffset().getY();

        double nn2 = Math.pow(pipe2.getFactorZ(), 2);
        double ll2 = Math.pow(pipe2.getFactorX(), 2);
        double mm2 = Math.pow(pipe2.getFactorY(), 2);

        double n2 = Math.pow(pipe1.getFactorZ(), 2);
        double l2 = Math.pow(pipe1.getFactorX(), 2);
        double m2 = Math.pow(pipe1.getFactorY(), 2);

        double v12 = Math.pow(v1, 2);
        double v22 = Math.pow(v2, 2);

        float t = (float)(-((nn * v2 - n * v1) * z2 + (n * v1 - nn * v2) * z1 + (mm * v2 - m * v1) * y2 + (m * v1 - mm * v2) * y1 + (ll * v2 - l * v1) * x2 + (l * v1 - ll * v2) * x1) / ((nn2 + mm2 + ll2) * v22 + (-2 * n * nn - 2 * m * mm - 2 * l * ll) * v1 * v2 + (n2 + m2 + l2) * v12));
        return Float.isNaN(t) ? 0 : t;
    }

    static public PipeVector getBisector(PipeVector pipe1, PipeVector pipe2) {
        PipeVector pipe = new PipeVector(new Point(),
                pipe1.getFactorX() + pipe2.getFactorX(),
                pipe1.getFactorY() + pipe2.getFactorY(),
                pipe1.getFactorZ() + pipe2.getFactorZ());
        if (pipe.getFactorX() == 0 && pipe.getFactorY() == 0 && pipe.getFactorZ() == 0) {
            pipe.setFactorXYZ(pipe1.getFactorX(), pipe1.getFactorY(), pipe1.getFactorZ());
        }
        return pipe;
    }

    static public float getDistance(PipeVector pipe1, double v1, PipeVector pipe2, double v2, double t) {
        double nn = pipe2.getFactorZ();
        double ll = pipe2.getFactorX();
        double mm = pipe2.getFactorY();

        double n = pipe1.getFactorZ();
        double l = pipe1.getFactorX();
        double m = pipe1.getFactorY();

        double z1 = pipe1.getOffset().getZ();
        double x1 = pipe1.getOffset().getX();
        double y1 = pipe1.getOffset().getY();

        double z2 = pipe2.getOffset().getZ();
        double x2 = pipe2.getOffset().getX();
        double y2 = pipe2.getOffset().getY();

        return (float) Math.sqrt(Math.pow(-z2 + z1 - nn * t * v2 + n * t * v1, 2) + Math.pow(-y2 + y1 - mm * t * v2 + m * t * v1, 2) + Math.pow(-x2 + x1 - ll * t * v2 + l * t * v1, 2));
    }

    static public float getVelocity(Point vector) {
        return (float)Math.sqrt(vector.getX() * vector.getX() + vector.getY() * vector.getY() + vector.getZ() * vector.getZ());
    }

    static public Point getVelocity(PipeVector vector, float velocity) {
        return new Point(vector.getFactorX() * velocity,  vector.getFactorY() * velocity, vector.getFactorZ() * velocity);
    }


    public static Point normalize(Point point, float factor) {
        float norm = (float)Math.sqrt(Math.pow(point.getX(), 2)) + (float)Math.pow(point.getY(), 2) + (float)Math.pow(point.getZ(), 2);
        if (Math.abs(norm) < 0.000001) {
            point.setX(0);
            point.setY(0);
            point.setZ(0);
            return point;
        }
        point.setX(factor * point.getX() / norm);
        point.setY(factor * point.getY() / norm);
        point.setZ(factor * point.getZ() / norm);
        return point;
    }


    static public PipeVector makeTimeShift(PipeVector pipe, float dt, float v) {
        Point shiftedPoint1 = new Point(pipe.getOffset().getX() + dt * v * pipe.getFactorX(),
                pipe.getOffset().getY() + dt * v * pipe.getFactorY(),
                pipe.getOffset().getZ() + dt * v * pipe.getFactorZ()
        );
        return new PipeVector(shiftedPoint1,
                pipe.getFactorX(),
                pipe.getFactorY(),
                pipe.getFactorZ());
    }

    static boolean areEqual(PipeVector p1, PipeVector p2) {
        boolean equal = DoubleCan.areEqual(p1.getFactorX(), p2.getFactorX());
        equal = equal && DoubleCan.areEqual(p1.getFactorY(), p2.getFactorY());
        equal = equal && DoubleCan.areEqual(p1.getFactorZ(), p2.getFactorZ());
        equal = equal && PointCan.areEqual(p1.getOffset(), p2.getOffset());
        return equal;
    }

    public static PipeVector getDifference(PipeVector vector1, PipeVector vector2) {
        PipeVector vector = new PipeVector();
        vector.setFactorXYZ(vector1.getFactorX() - vector2.getFactorX(),
                vector1.getFactorY() - vector2.getFactorY(),
                vector1.getFactorZ() - vector2.getFactorZ());
        return vector;
    }
}
