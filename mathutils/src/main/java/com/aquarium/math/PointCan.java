package com.aquarium.math;

import com.aquarium.entities.PipeVector;
import com.aquarium.entities.Point;

/**
 * Created by artem on 4/27/14.
 */
public class PointCan {

    static public boolean areEqual(Point p1, Point p2) {
        return DoubleCan.areEqual(p1.getX(), p2.getX()) &&
                DoubleCan.areEqual(p1.getY(), p2.getY()) &&
                DoubleCan.areEqual(p1.getZ(), p2.getZ());
    }

    static public float getDistance(Point p1, Point p2) {
        return (float)Math.sqrt(Math.pow((p1.getX() - p2.getX()), 2f) + Math.pow((p1.getY() - p2.getY()), 2) + Math.pow((p1.getZ() - p2.getZ()), 2f));
    }

    static public Point increasePoint(Point p1, Point p2) {
        p1.setX(p1.getX() + p2.getX());
        p1.setY(p1.getY() + p2.getY());
        p1.setZ(p1.getZ() + p2.getZ());
        return p1;
    }

    static public Point decreasePoint(Point p1, Point p2) {
        p1.setX(p1.getX() - p2.getX());
        p1.setY(p1.getY() - p2.getY());
        p1.setZ(p1.getZ() - p2.getZ());
        return p1;
    }

    /**
     * too slow method!!!
     *
     * @param position
     * @param radius
     * @return
     */
    public static Point randomInSphere(Point position, double radius) {
        double dist = radius * Math.random();
        Point point = PipesCan.normalize(new Point((float)(Math.random() * 2 - 1), (float)(Math.random() * 2 - 1), (float)(Math.random() * 2 - 1)), (float)dist);
        point.setX(point.getX() + position.getX());
        point.setY(point.getY() + position.getY());
        point.setZ(point.getZ() + position.getZ());
        return point;
    }

    public static Point randomInEllipse(Point position, double radius) {
        double dist = radius * Math.random();
        Point point = PipesCan.normalize(new Point((float)(Math.random() * 2 - 1), (float)(Math.random() * 2 - 1), (float)(Math.random() * 2 - 1)), (float)dist);
        point.setX(point.getX() + position.getX());
        point.setY(point.getY() + position.getY());
        point.setZ(point.getZ() + position.getZ());
        return point;
    }
}
