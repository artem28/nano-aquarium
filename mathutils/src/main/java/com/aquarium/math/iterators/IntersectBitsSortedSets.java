package com.aquarium.math.iterators;

import com.aquarium.entities.Bit;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * Created by artem on 6/26/14.
 */
public class IntersectBitsSortedSets {

    static public Iterator<Bit> intersect(final Collection<TreeSet<Bit>>[] sublists) {
        if (sublists.length == 1) {
            return MergeBitsSortedSets.merge(sublists[0]);
        }

        return new Iterator<Bit>() {

            Bit[] cursorPositions = new Bit[sublists.length];
            Iterator<Bit>[] cursors = initiateCursors(sublists);
            int i = 1;
            Bit current = findMatch(first2());

            @Override
            public String toString() {
                return "IntersectBitsSortedSets " +
                        "cursorPositions=[" + Arrays.toString(cursorPositions) +
                        "], cursors=[" + Arrays.toString(cursors) +
                        "], i=" + i +
                        ", current=[" + current + "]";
            }

            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public Bit next() {
                Bit previous = current;
                current = findMatch(anyNext(cursors[i], i));
                previous.getIdLong();
                return previous;
            }

            private Bit findMatch(Bit next) {
                int count = 1;
                if (next == null) {
                    return null;
                }
                Bit bit = null;
                while (true) {
                    bit = ceiling2(cursors[i], next, i);
                    if (bit == null) {
                        return null;
                    } else if (bit.equals(next)) {
                        count++;
                        if (count == sublists.length) {
                            i = ((i + 1) >= sublists.length) ? 0 : i + 1;
                            return bit;
                        }
                    } else {
                        count = 1;
                        next = bit;
                    }
                    i = ((i + 1) >= sublists.length) ? 0 : i + 1;
                }
            }

            private Iterator<Bit>[] initiateCursors(Collection<TreeSet<Bit>>[] sublists) {
                Iterator<Bit>[] newCursors = new Iterator[sublists.length];
                for (int i = 0; i < sublists.length; i++) {
                    newCursors[i] = MergeBitsSortedSets.merge(sublists[i]);
                }
                return newCursors;
            }

            private Bit ceiling2(Iterator<Bit> mergedSet, Bit previousBit, int pos) {
                if (cursorPositions[pos] != null && cursorPositions[pos].getIdLong().equals(previousBit.getIdLong())) {
                    return cursorPositions[pos];
                }
                while (mergedSet.hasNext()) {
                    Bit next = mergedSet.next();
                    if (next != null && (next.getId() >= previousBit.getId())) {
                        cursorPositions[pos] = next;
                        return next;
                    }
                }
                cursorPositions[pos] = null;
                return null;
            }

            private Bit first2() {
                cursorPositions[0] = cursors[0].next();
                return cursorPositions[0];
            }

            private Bit anyNext(Iterator<Bit> mergedSet, int pos) {
                Bit next = null;
                if (mergedSet.hasNext()) {
                    next = mergedSet.next();
                }
                cursorPositions[pos] = next;
                return next;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("intersect2 iterator doesnt support remove method.");
            }
        };
    }
}
