package com.aquarium.math.iterators;

import com.aquarium.entities.Bit;

import java.util.Collection;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * Created by artem on 6/26/14.
 */
public class IntersectBitsSortedSets2 {

    static public Iterator<Bit> intersect(final Collection<TreeSet<Bit>>[] sublists) {
        if (sublists.length == 1) {
            return MergeBitsSortedSets.merge(sublists[0]);
        }

        return new Iterator<Bit>() {

            int i = 1;
            Bit current = findMatch(first());

            @Override
            public String toString() {
                return "IntersectBitsSortedSets2 " +
                        "i=" + i +
                        ", current= [" + current +"]";
            }

            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public Bit next() {
                Bit previous = current;
                current = findMatch(higher(current, i));
                previous.getIdLong();
                return previous;
            }

            private Bit findMatch(Bit next) {
                int count = 1;
                if (next == null) {
                    return null;
                }
                Bit bit = null;
                while (true) {
                    bit = ceiling(next, i);
                    if (bit == null) {
                        return null;
                    } else if (bit.equals(next)) {
                        count++;
                        if (count == sublists.length) {
                            i = ((i + 1) >= sublists.length) ? 0 : i + 1;
                            return bit;
                        }
                    } else {
                        count = 1;
                        next = bit;
                    }
                    i = ((i + 1) >= sublists.length) ? 0 : i + 1;
                }
            }

            private Iterator<Bit>[] initiateCursors(Collection<TreeSet<Bit>>[] sublists) {
                Iterator<Bit>[] newCursors = new Iterator[sublists.length];
                for (int i = 0; i < sublists.length; i++) {
                    newCursors[i] = MergeBitsSortedSets.merge(sublists[i]);
                }
                return newCursors;
            }

            private Bit ceiling(Bit previousBit, int pos) {
                Bit result = null;
                for (TreeSet<Bit> sortedSet : sublists[pos]) {
                    Bit next = sortedSet.ceiling(previousBit);
                    if (next == null) {
                        continue;
                    }
                    if (result == null || next.getId() < result.getId()) {
                        result = next;
                    }
                }
                return result;
            }


            private Bit first() {
                Bit result = null;
                for (TreeSet<Bit> sortedSet : sublists[0]) {
                    Bit first = sortedSet.first();
                    if (result == null || first.getId() < result.getId()) {
                        result = first;
                    }
                }
                return result;
            }

            private Bit higher(Bit previousBit, int pos) {
                Bit result = null;
                for (TreeSet<Bit> sortedSet : sublists[pos]) {
                    Bit next = sortedSet.higher(previousBit);
                    if (next == null) {
                        continue;
                    }
                    if (result == null || next.getId() < result.getId()) {
                        result = next;
                    }
                }
                return result;
            }


            @Override
            public void remove() {
                throw new UnsupportedOperationException("intersect2 iterator doesnt support remove method.");
            }
        };
    }
}
