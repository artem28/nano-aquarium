package com.aquarium.math.iterators;

import com.aquarium.entities.Bit;

import java.util.*;

/**
 * utility class
 */
public class MergeBitsSortedSets {

    static public Iterator<Bit> merge(final Collection<TreeSet<Bit>> sublists) {
        if (sublists.size() == 1) {
            return sublists.iterator().next().iterator();
        }
        return new Iterator<Bit>() {

            List<Iterator<Bit>> iterators = getIterators(sublists);
            List<Bit> cursors = getFirstElements(iterators);
            int nullsCount = -1;

            @Override
            public String toString() {
                return "MergeBitsSortedSets " +
                        "iterators=[" + iterators +
                        "], cursors=[" + cursors +
                        "], nullsCount=" + nullsCount ;
            }

            private List<Bit> getFirstElements(Collection<Iterator<Bit>> sublists) {
                List<Bit> result = new ArrayList(sublists.size());
                nullsCount = 0;
                for (Iterator<Bit> sublist : sublists) {
                    Bit bit = sublist.next();
                    result.add(bit);
                    if (bit == null) {
                        nullsCount++;
                    }

                }
                return result;
            }

            private List<Iterator<Bit>> getIterators(Collection<TreeSet<Bit>> sublists) {
                List<Iterator<Bit>> result = new ArrayList(sublists.size());
                for (TreeSet<Bit> sublist : sublists) {
                    result.add(sublist.iterator());
                }
                return result;
            }

            @Override
            public boolean hasNext() {
                return nullsCount < cursors.size();
            }

            @Override
            public Bit next() {
                int posMax = 0;
                Bit current = null;
                nullsCount = 0;
                for (int i = 0; i < cursors.size(); i++) {
                    Bit bit = cursors.get(i);
                    if (current == null || (bit != null && bit.getId() < current.getId())) {
                        posMax = i;
                        current = bit;
                    }
                    if (bit == null) {
                        nullsCount++;
                    }
                }
                if (current == null) {
                    return null;
                }
                if (iterators.get(posMax).hasNext()) {
                    Bit bit = iterators.get(posMax).next();
                    cursors.set(posMax, bit);
                    if (bit == null) {
                        nullsCount++;
                    }
                } else {
                    cursors.set(posMax, null);
                    nullsCount++;
                }
                return current;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("merge iterator doesnt support remove method.");
            }
        };
    }
}

