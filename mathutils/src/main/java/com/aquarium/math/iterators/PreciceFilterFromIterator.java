package com.aquarium.math.iterators;

import com.aquarium.entities.Bit;
import com.aquarium.entities.Body;

import java.util.Iterator;

/**
 * Created by artem on 7/4/14.
 */
public class PreciceFilterFromIterator {

    static public Iterable<Bit> preciceFilter(final Iterable<Bit> set, final Body body) {
        return new Iterable<Bit>() {
            @Override
            public Iterator<Bit> iterator() {
                return new Iterator<Bit>() {
                    final Iterator<Bit> iteratorSet = set.iterator();
                    Bit next = findNext(iteratorSet, body);

                    @Override
                    public String toString() {
                        return "PreciceFilterFromIterator 1 " +
                                "iteratorSet=" + iteratorSet +
                                ", next=" + next;
                    }

                    @Override
                    public boolean hasNext() {
                        return next != null;
                    }

                    @Override
                    public Bit next() {
                        Bit previous = next;
                        next = findNext(iteratorSet, body);
                        return previous;
                    }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException("preciceFilter iterator doesnt support remove method.");
                    }
                };
            }
        };
    }

    static public Iterable<Bit> preciceFilter(final Iterable<Bit> set, final double fromX, final double fromY, final double fromZ, final double toX, final double toY, final double toZ) {
        return new Iterable<Bit>() {
            @Override
            public Iterator<Bit> iterator() {
                return new Iterator<Bit>() {
                    Iterator<Bit> iteratorSet = set.iterator();
                    Bit next = findNext(iteratorSet, fromX, fromY, fromZ, toX, toY, toZ);

                    @Override
                    public String toString() {
                        return "PreciceFilterFromIterator 2 " +
                                "iteratorSet=[" + iteratorSet +
                                "], next=[" + next + "]";
                    }

                    @Override
                    public boolean hasNext() {
                        return next != null;
                    }

                    @Override
                    public Bit next() {
                        Bit previous = next;
                        next = findNext(iteratorSet, fromX, fromY, fromZ, toX, toY, toZ);
                        previous.getIdLong();
                        return previous;
                    }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException("preciceFilter iterator doesnt support remove method.");
                    }
                };
            }
        };
    }

    static private Bit findNext(Iterator<Bit> iteratorSet, Body body) {
        while (iteratorSet.hasNext()) {
            Bit candidate = iteratorSet.next();
            if (body.isAcceptable(candidate)) {
                return candidate;
            }
        }
        return null;
    }


    static private Bit findNext(Iterator<Bit> iteratorSet, double fromX, double fromY, double fromZ, double toX, double toY, double toZ) {
        while (iteratorSet.hasNext()) {
            Bit candidate = iteratorSet.next();
            if (candidate.getPipe().getOffset().getX() >= fromX &&
                    candidate.getPipe().getOffset().getX() <= toX &&
                    candidate.getPipe().getOffset().getY() >= fromY &&
                    candidate.getPipe().getOffset().getY() <= toY &&
                    candidate.getPipe().getOffset().getZ() >= fromZ &&
                    candidate.getPipe().getOffset().getZ() <= toZ
                    ) {
                return candidate;
            }
        }
        return null;
    }

}
