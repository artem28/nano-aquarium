package com.aquarium.math.iterators;

import com.aquarium.entities.Bit;

import java.util.Iterator;

/**
 * Created by artem on 7/4/14.
 */
public class RemoveBitFormIterator {

    public static Iterable<Bit> removeBit(final Iterable<Bit> bits, final Bit bit1) {
        return new Iterable<Bit>() {
            @Override
            public Iterator<Bit> iterator() {
                return new Iterator<Bit>() {
                    Iterator<Bit> iterator = bits.iterator();
                    Bit next = nextNotBit(iterator, bit1);

                    @Override
                    public String toString() {
                        return "RemoveBitFormIterator " +
                                "iterator=[" + iterator +
                                "], next=[" + next + "]";
                    }

                    @Override
                    public boolean hasNext() {
                        return next != null;
                    }

                    @Override
                    public Bit next() {
                        Bit previous = next;
                        next = nextNotBit(iterator, bit1);
                        previous.getIdLong();
                        return previous;
                    }

                    private Bit nextNotBit(Iterator<Bit> iterator, Bit bit1) {
                        Bit bit = null;
                        while (iterator.hasNext()) {
                            bit = iterator.next();
                            if (!bit.getIdLong().equals(bit1.getIdLong())) {
                                return bit;
                            }
                        }
                        return null;
                    }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException("remove Bit iterator doesnt support remove method.");
                    }
                };
            }
        };
    }

}
