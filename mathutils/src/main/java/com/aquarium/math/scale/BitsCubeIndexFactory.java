package com.aquarium.math.scale;

import com.aquarium.entities.BitsIndex;
import com.aquarium.entities.IndexFactory;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created by artem on 6/9/14.
 */
public class BitsCubeIndexFactory implements IndexFactory {
    Set<Float> indexes = new TreeSet<Float>();
    float bitSize = 0;
    float worldSize;

    public BitsCubeIndexFactory(float bitSize, float worldSize) {
        this.bitSize = bitSize;
        this.worldSize = worldSize;
    }

    public BitsCubeIndexFactory addIndex(float cellSize) {
        indexes.add(cellSize);
        return this;
    }

    public BitsIndex createComplexIndex() {
        BitsIndex[] indexs = new BitsIndex[indexes.size()+1];
        float[] cellSizes = new float[indexes.size()+1];
        cellSizes[0] = bitSize;
        indexs[0]  = new CubeScaleHolder(bitSize, worldSize);
        int i = 1;
        for (Float size : indexes) {
            indexs[i] = new RoughScaleHolder(size);
            cellSizes[i] = size;
            i++;
        }
        BitsIndexFacade complexIndex = new BitsIndexFacade(indexs, cellSizes);
        return complexIndex;
    }
}