package com.aquarium.math.scale;

import com.aquarium.entities.*;
import com.aquarium.math.DoubleCan;

import java.util.*;

/**
 * Created by artem on 6/9/14.
 */
public class BitsIndexFacade implements BitsIndex {
    private BitsIndex[] indexs = new BitsIndex[0];
    private float[] cellSizes = new float[0];

    public BitsIndexFacade(BitsIndex[] indexs, float[] cellSizes) {
        this.indexs = indexs;
        this.cellSizes = cellSizes;
    }

    @Override
    public void addBit(Bit bit, Point point) {
        for (BitsIndex index : indexs) {
            index.addBit(bit, point);
        }
    }

    @Override
    public void addBits(BitsIndex bits) {
        for (BitsIndex index : indexs) {
            index.addBits(bits);
        }
    }

    @Override
    public Iterable<Bit> getBitSetInX(float from, float to) {
        return getAppropriateIndex(to - from).getBitSetInX(from, to);
    }

    @Override
    public Iterable<Bit> getBitSetInY(float from, float to) {
        return getAppropriateIndex(to - from).getBitSetInY(from, to);
    }

    @Override
    public Iterable<Bit> getBitSetInZ(float from, float to) {
        return getAppropriateIndex(to - from).getBitSetInZ(from, to);
    }

    @Override
    public Iterable<Bit> findAcceptable(Body body) {
        if (body instanceof SurfaceBody && indexs.length > 1) {
            return getBitsForSurface(body);
        } else {
            return getAppropriateIndex(body.getSize()).findAcceptable(body);
        }
    }

    public Iterable<Bit> getBitsForSurface(Body body) {
        if (indexs[1] instanceof RoughScaleHolder) {
            PipeVector surfacePosition = ((SurfaceBody) body).getSurfaceOrientation();
            float size = body.getSize() / 2.0f;
            if (DoubleCan.areEqual(surfacePosition.getFactorY(), 0) && DoubleCan.areEqual(surfacePosition.getFactorZ(), 0)) {
                return indexs[1].getBitSetInX(surfacePosition.getOffset().getX() - size, surfacePosition.getOffset().getX() + size);
            } else if (DoubleCan.areEqual(surfacePosition.getFactorX(), 0) && DoubleCan.areEqual(surfacePosition.getFactorZ(), 0)) {
                return indexs[1].getBitSetInY(surfacePosition.getOffset().getY() - size, surfacePosition.getOffset().getY() + size);
            } else if (DoubleCan.areEqual(surfacePosition.getFactorY(), 0) && DoubleCan.areEqual(surfacePosition.getFactorX(), 0)) {
                return indexs[1].getBitSetInZ(surfacePosition.getOffset().getZ() - size, surfacePosition.getOffset().getZ() + size);
            }
            throw new RuntimeException("Surface body has wrong configuration");
        } else {
            throw new RuntimeException("Bit index is not supported - " + indexs[1].getClass());
        }
    }

    @Override
    public Iterable<Bit> getCloseBits(Bit bit1, float bitsize) {
        return getAppropriateIndex(bitsize).getCloseBits(bit1, bitsize);
    }

    @Override
    public Iterable<Bit> getCloseBits(Bit bit1, float bitsize, int completeFast) {
        return getCloseBits(bit1, bitsize);
    }

    @Override
    public Iterator<Bit> iterator() {
        return indexs[0].iterator();
    }

    @Override
    public void modifyBit(Bit bit) {
        for (BitsIndex index : indexs) {
            index.modifyBit(bit);
        }
    }

    private BitsIndex getAppropriateIndex(float cellSize) {
        for (int i = 0; i < cellSizes.length; i++) {
            if (cellSizes[i] >= cellSize) {
                return indexs[i];
            }
        }
        return indexs[cellSizes.length - 1];
    }

    @Override
    public void startIndexModification() {
        for (BitsIndex index : indexs) {
            index.startIndexModification();
        }
    }

    @Override
    public void endOfIndexModification() {
        for (BitsIndex index : indexs) {
            index.endOfIndexModification();
        }
    }


}
