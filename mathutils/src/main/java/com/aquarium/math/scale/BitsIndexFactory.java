package com.aquarium.math.scale;

import com.aquarium.entities.BitsIndex;

import java.util.*;

/**
 * Created by artem on 6/9/14.
 */
public class BitsIndexFactory {
    Set<Float> indexes = new TreeSet<Float>();

    public BitsIndexFactory addIndex(float cellSize) {
        indexes.add(cellSize);
        return this;
    }

    public BitsIndex createComplexIndex() {
        BitsIndex[] indexs = new BitsIndex[indexes.size()];
        float[] cellSizes = new float[indexes.size()];
        int i = 0;
        for (Float size : indexes) {
            indexs[i] = new RoughScaleHolder(size);
            cellSizes[i] = size;
            i++;
        }
        BitsIndexFacade complexIndex = new BitsIndexFacade(indexs, cellSizes);
        return complexIndex;
    }
}