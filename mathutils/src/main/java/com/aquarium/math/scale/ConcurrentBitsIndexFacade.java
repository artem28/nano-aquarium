package com.aquarium.math.scale;

import com.aquarium.entities.Bit;
import com.aquarium.entities.BitsIndex;
import com.aquarium.entities.Point;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by artem on 9/14/14.
 */
public class ConcurrentBitsIndexFacade extends BitsIndexFacade {

    Queue<Bit> bitsQueue = new ConcurrentLinkedQueue<Bit>();
    public static final Logger log = LogManager.getLogger("IncrementalEngine");

    public ConcurrentBitsIndexFacade(BitsIndex[] indexs, float[] cellSizes) {
        super(indexs, cellSizes);
    }

    @Override
    public void addBit(Bit bit, Point point) {
        bitsQueue.add(bit);
    }

    @Override
    public void addBits(BitsIndex bits) {
        addBits(bits);
    }

    public void flushQueue(int amountOfBits) {
        int count = 0;
        while (count < amountOfBits) {
            Bit bit = bitsQueue.poll();
            if (bit == null) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    throw new RuntimeException("Bits index creation was interrupted", e);
                }
            } else {
                super.addBit(bit, bit.getPipe().getOffset());
                count++;
                if (count % 100000 == 0) {
                    log.info("ConcurrentBitsIndexFacade.flushQueue i=" + count);
                }

            }
        }
    }
}
