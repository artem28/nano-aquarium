package com.aquarium.math.scale;

import com.aquarium.entities.*;
import com.aquarium.math.iterators.PreciceFilterFromIterator;
import com.aquarium.math.iterators.RemoveBitFormIterator;
import com.google.common.collect.Iterables;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * Created by artem on 5/12/14.
 */
public class CubeScaleHolder implements BitsIndex {

    static final Logger log = LogManager.getLogger("IncrementalEngine");


    private Map<Integer, Set<Bit>> index = new HashMap<Integer, Set<Bit>>();
    private Map<Integer, Integer> invertedIndex = new HashMap<Integer, Integer>();
    private final int worldSize;
    private final float step;

    public CubeScaleHolder(float step, float worldSize) {
        this.step = step;
        this.worldSize = ((int) Math.floor(2 * worldSize / step));
    }

    private int getRoughKey(float x, float y, float z) {
        int intX = (int)(Math.round(x / step));
        int intY = (int)(Math.round(y / step));
        int intZ = (int)(Math.round(z / step));

        return intX + intY * worldSize + intZ * worldSize * worldSize;
    }

    private int[] getReturnRoughKey(float x, float y, float z) {
        int[] result = new int[27];
        int intX = (int)(Math.round(x / step));
        int intY = (int)(Math.round(y / step));
        int intZ = (int)(Math.round(z / step));
        int pos = 0;
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                for (int k = -1; k < 2; k++) {
                    result[pos] = intX + i + (intY + j) * worldSize + (intZ + k) * worldSize * worldSize;
                    pos++;
                }
            }
        }
        return result;
    }

    @Override
    public void addBit(Bit bit, Point point) {
        Integer value = getRoughKey(point.getX(), point.getY(), point.getZ());
        Set<Bit> bits = index.get(value);
        if (bits == null) {
            bits = new HashSet<Bit>();
            index.put(value, bits);
        }
        invertedIndex.put(bit.getId(), value);
        bits.add(bit);
    }

    @Override
    public void addBits(BitsIndex bits) {
        for (Bit bit : bits) {
            addBit(bit, bit.getPipe().getOffset());
        }
    }

    @Override
    public Iterable<Bit> getBitSetInX(float from, float to) {
        throw new RuntimeException("getBitSetInX method is not support in CubeScaleHolder");
    }

    @Override
    public Iterable<Bit> getBitSetInY(float from, float to) {
        throw new RuntimeException("getBitSetInY method is not support in CubeScaleHolder");
    }

    @Override
    public Iterable<Bit> getBitSetInZ(float from, float to) {
        throw new RuntimeException("getBitSetInZ method is not support in CubeScaleHolder");
    }


    @Override
    public Iterable<Bit> findAcceptable(Body body) {
        if (body instanceof SurfaceBody) {
            throw new RuntimeException("CubeScaleHolder cannot work with Surface body");
        }
        Iterable<Bit> cube = Collections.EMPTY_SET;

        for (Integer i : getReturnRoughKey(body.getPosition().getX(),
                body.getPosition().getY(),
                body.getPosition().getZ())) {
            Iterable<Bit> next = index.get(i);
            if (next != null) {
                cube = Iterables.concat(next, cube);
            }
        }

        return PreciceFilterFromIterator.preciceFilter(cube, body);
    }

    @Override
    public Iterable<Bit> getCloseBits(Bit bit1, float bitsize) {
        Iterable<Bit> cube = null;

        for (Integer i : getReturnRoughKey(bit1.getPipe().getOffset().getX(),
                bit1.getPipe().getOffset().getY(),
                bit1.getPipe().getOffset().getZ())) {
            Set<Bit> next = index.get(i);
            if (cube != null && next != null && !next.isEmpty()) {
                cube = Iterables.concat(next, cube);
            } else if (cube == null && next != null && !next.isEmpty()) {
                cube = next;
            }
        }
        if (cube == null) {
            return Collections.EMPTY_SET;
        }

        return RemoveBitFormIterator.removeBit(PreciceFilterFromIterator.preciceFilter(cube,
                bit1.getPipe().getOffset().getX() - bitsize,
                bit1.getPipe().getOffset().getY() - bitsize,
                bit1.getPipe().getOffset().getZ() - bitsize,
                bit1.getPipe().getOffset().getX() + bitsize,
                bit1.getPipe().getOffset().getY() + bitsize,
                bit1.getPipe().getOffset().getZ() + bitsize), bit1);
    }

    @Override
    public Iterable<Bit> getCloseBits(Bit bit1, float bitsize, int completeFast) {
        return getCloseBits(bit1, bitsize);
    }

    @Override
    public Iterator<Bit> iterator() {
        final Iterator<Set<Bit>> bits = index.values().iterator();
        return new Iterator<Bit>() {
            Iterator<Bit> lastGroup;

            @Override
            public boolean hasNext() {
                if (lastGroup == null || !lastGroup.hasNext()) {
                    while (bits.hasNext()) {
                        Object n = bits.next();
                        try {
                            lastGroup = ((Iterable) n).iterator();
                        } catch (ClassCastException cce) {
                            log.error("ClassCastException because of (Iterable) n where n.class=" + n.getClass().getName(), cce);
                            throw cce;
                        }
                        if (lastGroup.hasNext()) {
                            break;
                        }
                    }
                }
                return lastGroup.hasNext();
            }

            @Override
            public Bit next() {
                if (!hasNext()) {
                    return null;
                }
                return lastGroup.next();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("ScaleHolder iterator doesnt support remove method.");
            }
        };
    }

    @Override
    public void startIndexModification() {

    }

    @Override
    public void endOfIndexModification() {

    }

    @Override
    public synchronized void modifyBit(Bit bit) {
        Integer newValue = getRoughKey(bit.getPipe().getOffset().getX(), bit.getPipe().getOffset().getY(), bit.getPipe().getOffset().getZ());
        synchronized (invertedIndex) {
            synchronized (index) {

                Integer previousValue = invertedIndex.get(bit.getId());
                if (previousValue.equals(newValue)) {
                    return;
                }
                Set<Bit> newBits = index.get(newValue);
                if (newBits == null) {
                    newBits = new HashSet<Bit>();
                    index.put(newValue, newBits);
                }

                invertedIndex.put(bit.getId(), newValue);
                Set<Bit> prevBits = index.get(previousValue);
                prevBits.remove(bit);

                if (prevBits.isEmpty()) {
                    index.remove(previousValue);
                }

                newBits.add(bit);
            }
        }
        //assertion
/*
        if (log.isDebugEnabled()) {
            Integer value = invertedIndex.get(bit.getId());
            if (!index.containsKey(value)) {
                throw new RuntimeException("Assertion failed!!!");
            }
        }
*/
    }
}
