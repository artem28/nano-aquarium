package com.aquarium.math.scale;

import com.aquarium.entities.BitsIndex;
import com.aquarium.entities.IndexFactory;

/**
 * Created by artem on 30.03.15.
 */
public class KDTreeIndexFactory implements IndexFactory {

    private float worldSize;

    public KDTreeIndexFactory(float worldSize){
        this.worldSize = worldSize;
    }

    @Override
    public BitsIndex createComplexIndex() {
        return new KDTreeScaleHolder(worldSize);
    }

    @Override
    public IndexFactory addIndex(float cellSize) {
        return this;
    }
}
