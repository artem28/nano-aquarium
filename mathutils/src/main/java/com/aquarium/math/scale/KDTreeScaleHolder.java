package com.aquarium.math.scale;

import com.aquarium.entities.*;
import com.aquarium.math.DoubleCan;
import com.aquarium.math.scale.kdtree.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by artem on 29.03.15.
 */
public class KDTreeScaleHolder implements BitsIndex {

    private KDTree tree;
    private KDTree newTree;
    private float worldSize;
    private IMultiPoint[] supportrValues;
    private static final float WORLD_SIZE_FACTOR = 3;
    CompleteFastException cfe = new CompleteFastException();

    public KDTreeScaleHolder(float worldSize) {
        this.worldSize = worldSize;
        supportrValues = generateSupportValue(worldSize);
        tree = new KDTree(3, supportrValues, worldSize * WORLD_SIZE_FACTOR);
    }

    private IMultiPoint[] generateSupportValue(float worldSize) {
        float worldSizeQuarter = worldSize / 4;
        IMultiPoint[] values = new IMultiPoint[15];
        float xRoot = 0;
        values[0] = new Hyperpoint(new Bit(Integer.MAX_VALUE - 1), new float[]{xRoot, 1, 2});
        float lessXRoot = xRoot - worldSizeQuarter;
        float moreXRoot = xRoot + worldSizeQuarter;
        float yRoot = 0;
        values[1] = new Hyperpoint(new Bit(Integer.MAX_VALUE - 2), new float[]{lessXRoot, yRoot, 2});
        values[2] = new Hyperpoint(new Bit(Integer.MAX_VALUE - 3), new float[]{moreXRoot, yRoot, 2});
        float lessYRoot = yRoot - worldSizeQuarter;
        float moreYRoot = yRoot + worldSizeQuarter;
        float zRoot = 0;
        values[3] = new Hyperpoint(new Bit(Integer.MAX_VALUE - 4), new float[]{lessXRoot, lessYRoot, zRoot});
        values[4] = new Hyperpoint(new Bit(Integer.MAX_VALUE - 5), new float[]{lessXRoot, moreYRoot, zRoot});
        values[5] = new Hyperpoint(new Bit(Integer.MAX_VALUE - 6), new float[]{moreXRoot, lessYRoot, zRoot});
        values[6] = new Hyperpoint(new Bit(Integer.MAX_VALUE - 7), new float[]{moreXRoot, moreYRoot, zRoot});

        float lessZRoot = zRoot - worldSizeQuarter;
        float moreZRoot = zRoot + worldSizeQuarter;

        values[7] = new Hyperpoint(new Bit(Integer.MAX_VALUE - 8), new float[]{-worldSizeQuarter, lessYRoot, lessZRoot});
        values[8] = new Hyperpoint(new Bit(Integer.MAX_VALUE - 9), new float[]{-worldSizeQuarter, lessYRoot, moreZRoot});
        values[9] = new Hyperpoint(new Bit(Integer.MAX_VALUE - 10), new float[]{-worldSizeQuarter, moreYRoot, lessZRoot});
        values[10] = new Hyperpoint(new Bit(Integer.MAX_VALUE - 11), new float[]{-worldSizeQuarter, moreYRoot, moreZRoot});
        values[11] = new Hyperpoint(new Bit(Integer.MAX_VALUE - 12), new float[]{worldSizeQuarter, lessYRoot, lessZRoot});
        values[12] = new Hyperpoint(new Bit(Integer.MAX_VALUE - 13), new float[]{worldSizeQuarter, lessYRoot, moreZRoot});
        values[13] = new Hyperpoint(new Bit(Integer.MAX_VALUE - 14), new float[]{worldSizeQuarter, moreYRoot, lessZRoot});
        values[14] = new Hyperpoint(new Bit(Integer.MAX_VALUE - 15), new float[]{worldSizeQuarter, moreYRoot, moreZRoot});

        return values;
    }

    @Override
    public void addBit(Bit bit, Point point) {
        tree.insert(new Hyperpoint(bit, new float[]{point.getX(), point.getY(), point.getZ()}));
    }

    @Override
    public void addBits(BitsIndex bits) {
        for (Bit b : bits) {
            addBit(b, b.getPipe().getOffset());
        }
    }

    @Override
    public Iterable<Bit> getBitSetInX(float from, float to) {
        float low = Math.min(from, to);
        float high = Math.max(from, to);
        Hypercube criteria = new Hypercube(
                new float[]{low, -2 * worldSize, -2 * worldSize},
                new float[]{high, 2 * worldSize, 2 * worldSize}, worldSize * WORLD_SIZE_FACTOR);
        return searchNonSupportValues(criteria);
    }

    @Override
    public Iterable<Bit> getBitSetInY(float from, float to) {
        float low = Math.min(from, to);
        float high = Math.max(from, to);
        Hypercube criteria = new Hypercube(
                new float[]{-2 * worldSize, low, -2 * worldSize},
                new float[]{2 * worldSize, high, 2 * worldSize}, worldSize * WORLD_SIZE_FACTOR);
        return searchNonSupportValues(criteria);
    }

    @Override
    public Iterable<Bit> getBitSetInZ(float from, float to) {
        float low = Math.min(from, to);
        float high = Math.max(from, to);
        Hypercube criteria = new Hypercube(
                new float[]{-2 * worldSize, -2 * worldSize, low},
                new float[]{2 * worldSize, 2 * worldSize, high}, worldSize * WORLD_SIZE_FACTOR);
        return searchNonSupportValues(criteria);
    }

    private Iterable<Bit> searchNonSupportValues(Hypercube criteria) {
        final List<IMultiPoint> result = new ArrayList<IMultiPoint>();
        tree.search(criteria, new IVisitKDNode() {
            @Override
            public void visit(DimensionalNode node) {
                if (!node.isSupport()) {
                    result.add(node.point);
                }
            }

            @Override
            public void drain(DimensionalNode node) {
                visit(node);
            }
        });
        return getBitsIterable(result);
    }

    @Override
    public Iterable<Bit> findAcceptable(final Body body) {
        if (body instanceof SurfaceBody) {
            return getBitsForSurface(body);
        }
        float r = body.getSize() / 2;
        Point p = body.getPosition();
        Hypercube criteria = new Hypercube(
                new float[]{p.getX() - r, p.getY() - r, p.getZ() - r},
                new float[]{p.getX() + r, p.getY() + r, p.getZ() + r}, worldSize * WORLD_SIZE_FACTOR);
        final List<IMultiPoint> points = new ArrayList<IMultiPoint>(100);
        tree.search(criteria, new IVisitKDNode() {
            @Override
            public void visit(DimensionalNode node) {
                if (!node.isSupport() && body.isAcceptable(node.point.getBit())) {
                    points.add(node.point);
                }
            }

            @Override
            public void drain(DimensionalNode node) {
                visit(node);
            }
        });

        return getBitsIterable(points);
    }

    public Iterable<Bit> getBitsForSurface(Body body) {
        PipeVector surfacePosition = ((SurfaceBody) body).getSurfaceOrientation();
        float size = body.getSize() / 2.0f;
        if (DoubleCan.areEqual(surfacePosition.getFactorY(), 0) && DoubleCan.areEqual(surfacePosition.getFactorZ(), 0)) {
            return getBitSetInX(surfacePosition.getOffset().getX() - size, surfacePosition.getOffset().getX() + size);
        } else if (DoubleCan.areEqual(surfacePosition.getFactorX(), 0) && DoubleCan.areEqual(surfacePosition.getFactorZ(), 0)) {
            return getBitSetInY(surfacePosition.getOffset().getY() - size, surfacePosition.getOffset().getY() + size);
        } else if (DoubleCan.areEqual(surfacePosition.getFactorY(), 0) && DoubleCan.areEqual(surfacePosition.getFactorX(), 0)) {
            return getBitSetInZ(surfacePosition.getOffset().getZ() - size, surfacePosition.getOffset().getZ() + size);
        }
        throw new RuntimeException("Surface body has wrong configuration");
    }

    @Override
    public Iterable<Bit> getCloseBits(final Bit bit1, float bitsize) {
        Point p = bit1.getPipe().getOffset();
        final float b = bitsize;
        final List<IMultiPoint> result = new ArrayList<IMultiPoint>(20);
        tree.search(new Hypercube(
                        new float[]{p.getX() - b, p.getY() - b, p.getZ() - b},
                        new float[]{p.getX() + b, p.getY() + b, p.getZ() + b}, worldSize * WORLD_SIZE_FACTOR),
                new IVisitKDNode() {
                    @Override
                    public void visit(DimensionalNode node) {
                        if (!node.isSupport() && node.point.getBit().getId() != bit1.getId()) {
                            result.add(node.point);
                        }
                    }

                    @Override
                    public void drain(DimensionalNode node) {
                        visit(node);
                    }
                }
        );

        return getBitsIterable(result);
    }

    @Override
    public Iterable<Bit> getCloseBits(final Bit bit1, float bitsize, final int completeFast) {
        Point p = bit1.getPipe().getOffset();
        final float b = bitsize;
        final List<Bit> result = new ArrayList<Bit>(completeFast);
        try {
            tree.search(new Hypercube(
                            new float[]{p.getX() - b, p.getY() - b, p.getZ() - b},
                            new float[]{p.getX() + b, p.getY() + b, p.getZ() + b}, worldSize * WORLD_SIZE_FACTOR),
                    new IVisitKDNode() {
                        @Override
                        public void visit(DimensionalNode node) {
                            if (!node.isSupport() && node.point.getBit().getId() != bit1.getId()) {
                                result.add(node.point.getBit());
                            }
                            if (result.size() >= completeFast) {
                                throw cfe;
                            }
                        }

                        @Override
                        public void drain(DimensionalNode node) {
                            visit(node);
                        }
                    }
            );
        } catch (CompleteFastException e) {
        }


        return result;
    }

    private Iterable<Bit> getBitsIterable(final List<IMultiPoint> result) {
        return new Iterable<Bit>() {
            @Override
            public Iterator<Bit> iterator() {
                return new Iterator<Bit>() {
                    Iterator<IMultiPoint> it = result.iterator();

                    @Override
                    public boolean hasNext() {
                        return it.hasNext();
                    }

                    @Override
                    public Bit next() {
                        return it.next().getBit();
                    }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException();
                    }
                };
            }
        };
    }

    @Override
    public synchronized void startIndexModification() {
        newTree = new KDTree(3, supportrValues, worldSize * WORLD_SIZE_FACTOR);
    }

    @Override
    public synchronized void endOfIndexModification() {
        tree = newTree;
        newTree = null;
    }

    @Override
    public Iterator<Bit> iterator() {
        final ArrayList<IMultiPoint> result = new ArrayList<IMultiPoint>(tree.count());
        tree.getRoot().drain(new IVisitKDNode() {
            @Override
            public void visit(DimensionalNode node) {
                if (!node.isSupport()) {
                    result.add(node.point);
                }
            }

            @Override
            public void drain(DimensionalNode node) {
                visit(node);
            }
        });
        return getBitsIterable(result).iterator();
    }

    @Override
    public void modifyBit(Bit bit) {
        Point point = bit.getPipe().getOffset();
        newTree.insertAsync(new Hyperpoint(bit, new float[]{point.getX(), point.getY(), point.getZ()}));
    }

    private class CompleteFastException extends RuntimeException {
        public Throwable fillInStackTrace() {
            return this;
        }
    }
}
