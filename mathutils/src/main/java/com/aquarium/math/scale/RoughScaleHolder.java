package com.aquarium.math.scale;

import com.aquarium.entities.*;
import com.aquarium.math.iterators.IntersectBitsSortedSets;
import com.aquarium.math.iterators.MergeBitsSortedSets;
import com.aquarium.math.iterators.PreciceFilterFromIterator;
import com.aquarium.math.iterators.RemoveBitFormIterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * Created by artem on 5/12/14.
 */
public class RoughScaleHolder implements BitsIndex {

    static final Logger log = LogManager.getLogger("IncrementalEngine");


    TreeMap<Integer, TreeSet<Bit>> xScale = new TreeMap<Integer, TreeSet<Bit>>();
    TreeMap<Integer, TreeSet<Bit>> yScale = new TreeMap<Integer, TreeSet<Bit>>();
    TreeMap<Integer, TreeSet<Bit>> zScale = new TreeMap<Integer, TreeSet<Bit>>();
    private Map<Integer, Integer> invertedIndexX = new HashMap<Integer, Integer>();
    private Map<Integer, Integer> invertedIndexY = new HashMap<Integer, Integer>();
    private Map<Integer, Integer> invertedIndexZ = new HashMap<Integer, Integer>();

    float step;

    final BitComparator COMPARATOR = new BitComparator();


    public RoughScaleHolder(float step) {
        this.step = step;
    }

    private int getRoughKey(float value) {
        return (int) Math.floor(value / step);
    }

    @Override
    public void addBit(Bit bit, Point point) {
        addBit(bit, getRoughKey(point.getX()), xScale, invertedIndexX);
        addBit(bit, getRoughKey(point.getY()), yScale, invertedIndexY);
        addBit(bit, getRoughKey(point.getZ()), zScale, invertedIndexZ);
    }

    @Override
    public void addBits(BitsIndex bits) {
        for (Bit bit : bits) {
            addBit(bit, bit.getPipe().getOffset());
        }
    }

    private void addBit(Bit bit, Integer value, TreeMap<Integer, TreeSet<Bit>> scale, Map<Integer, Integer> invertedIndex) {
        TreeSet<Bit> bits = scale.get(value);
        if (bits == null) {
            bits = new TreeSet<Bit>(COMPARATOR);
            scale.put(value, bits);
        }
        invertedIndex.put(bit.getId(), value);
        bits.add(bit);
    }

    private void modifyBit(Bit bit, Integer newValue, TreeMap<Integer, TreeSet<Bit>> scale, Map<Integer, Integer> invertedIndex) {
        Integer previousValue;
        synchronized (invertedIndex) {
            previousValue = invertedIndex.get(bit.getId());
            if (previousValue.equals(newValue)) {
                return;
            }
            invertedIndex.put(bit.getId(), newValue);
        }
        synchronized (scale) {
            TreeSet<Bit> newBits = scale.get(newValue);
            if (newBits == null) {
                newBits = new TreeSet<Bit>(COMPARATOR);
                scale.put(newValue, newBits);
            }
            newBits.add(bit);
            TreeSet<Bit> prevBits = scale.get(previousValue);

            prevBits.remove(bit);
            if (prevBits.isEmpty()) {
                scale.remove(previousValue);
            }
        }
    }

    @Override
    public void modifyBit(Bit bit) {
        modifyBit(bit, getRoughKey(bit.getPipe().getOffset().getX()), xScale, invertedIndexX);
        modifyBit(bit, getRoughKey(bit.getPipe().getOffset().getY()), yScale, invertedIndexY);
        modifyBit(bit, getRoughKey(bit.getPipe().getOffset().getZ()), zScale, invertedIndexZ);
    }

    private Iterable<Bit> getBitSet(TreeMap<Integer, TreeSet<Bit>> scale, float from, float to) {
        int correctedTo = getRoughKey(to) + (to > 100000000 ? 0 : 1);
        Collection<TreeSet<Bit>> sublists = scale.subMap(getRoughKey(from), correctedTo).values();
        if (!sublists.isEmpty()) {
            return merge(sublists);
        }
        return Collections.EMPTY_LIST;
    }

    private Iterable<Bit> merge(final Collection<TreeSet<Bit>> sublists) {
        return new Iterable<Bit>() {
            @Override
            public Iterator<Bit> iterator() {
                return MergeBitsSortedSets.merge(sublists);
            }
        };
    }

    private Iterable<Bit> intersect2(final Collection<TreeSet<Bit>>[] sublists) {

        return new Iterable<Bit>() {
            @Override
            public Iterator<Bit> iterator() {
                return IntersectBitsSortedSets.intersect(sublists);
            }
        };

    }

    private Bit getMaxBit(Bit val1, Bit val2) {
        return val1.getId() > val2.getId() ? val1 : val2;
    }

    @Override
    public Iterable<Bit> getBitSetInX(float from, float to) {
        return PreciceFilterFromIterator.preciceFilter(getBitSet(xScale, from, to),
                from,
                -Float.MAX_VALUE,
                -Float.MAX_VALUE,
                to,
                Float.MAX_VALUE,
                Float.MAX_VALUE
        );
    }

    @Override
    public Iterable<Bit> getBitSetInY(float from, float to) {
        return PreciceFilterFromIterator.preciceFilter(getBitSet(yScale, from, to),
                -Float.MAX_VALUE,
                from,
                -Float.MAX_VALUE,
                Float.MAX_VALUE,
                to,
                Float.MAX_VALUE
        );
    }

    @Override
    public Iterable<Bit> getBitSetInZ(float from, float to) {
        return PreciceFilterFromIterator.preciceFilter(getBitSet(zScale, from, to),
                -Float.MAX_VALUE,
                -Float.MAX_VALUE,
                from,
                Float.MAX_VALUE,
                Float.MAX_VALUE,
                to
        );
    }

    @Override
    public void startIndexModification() {

    }

    @Override
    public void endOfIndexModification() {

    }

    @Override
    public Iterable<Bit> findAcceptable(Body body) {
        if (body instanceof SurfaceBody) {
            throw new RuntimeException("RoughScaleHolder cannot work with Surface body");
        }

        Collection<TreeSet<Bit>> xSet = xScale.subMap(getRoughKey(body.getPosition().getX() - body.getSize()), true, getRoughKey(body.getPosition().getX() + body.getSize()), true).values();
        Collection<TreeSet<Bit>> ySet = yScale.subMap(getRoughKey(body.getPosition().getY() - body.getSize()), true, getRoughKey(body.getPosition().getY() + body.getSize()), true).values();
        Collection<TreeSet<Bit>> zSet = zScale.subMap(getRoughKey(body.getPosition().getZ() - body.getSize()), true, getRoughKey(body.getPosition().getZ() + body.getSize()), true).values();
        Collection<TreeSet<Bit>>[] arr = new Collection[]{xSet, ySet, zSet};

        return PreciceFilterFromIterator.preciceFilter(intersect2(arr), body);
    }

    @Override
    public Iterable<Bit> getCloseBits(Bit bit1, float bitsize) {


        Collection<TreeSet<Bit>> xSet = xScale.subMap(getRoughKey(bit1.getPipe().getOffset().getX() - bitsize) - 1, true, getRoughKey(bit1.getPipe().getOffset().getX() + bitsize) + 1, true).values();
        Collection<TreeSet<Bit>> ySet = yScale.subMap(getRoughKey(bit1.getPipe().getOffset().getY() - bitsize) - 1, true, getRoughKey(bit1.getPipe().getOffset().getY() + bitsize) + 1, true).values();
        Collection<TreeSet<Bit>> zSet = zScale.subMap(getRoughKey(bit1.getPipe().getOffset().getZ() - bitsize) - 1, true, getRoughKey(bit1.getPipe().getOffset().getZ() + bitsize) + 1, true).values();
        Collection<TreeSet<Bit>>[] arr = new Collection[]{xSet, ySet, zSet};

        return RemoveBitFormIterator.removeBit(PreciceFilterFromIterator.preciceFilter(intersect2(arr),
                bit1.getPipe().getOffset().getX() - bitsize,
                bit1.getPipe().getOffset().getY() - bitsize,
                bit1.getPipe().getOffset().getZ() - bitsize,
                bit1.getPipe().getOffset().getX() + bitsize,
                bit1.getPipe().getOffset().getY() + bitsize,
                bit1.getPipe().getOffset().getZ() + bitsize
        ), bit1);
    }

    @Override
    public Iterable<Bit> getCloseBits(Bit bit1, float bitsize, int completeFast) {
        return getCloseBits(bit1,bitsize);
    }


    @Override
    public Iterator<Bit> iterator() {
        final Iterator<TreeSet<Bit>> bits = xScale.values().iterator();
        return new Iterator<Bit>() {
            Iterator<Bit> lastGroup;

            @Override
            public boolean hasNext() {
                if (lastGroup == null || !lastGroup.hasNext()) {
                    while (bits.hasNext()) {
                        Object n = bits.next();
                        try {
                            lastGroup = ((Iterable) n).iterator();
                        } catch (ClassCastException cce) {
                            log.error("ClassCastException because of (Iterable) n where n.class=" + n.getClass().getName(), cce);
                            throw cce;
                        }
                        if (lastGroup.hasNext()) {
                            break;
                        }
                    }
                }
                return lastGroup.hasNext();
            }

            @Override
            public Bit next() {
                if (!hasNext()) {
                    return null;
                }
                return lastGroup.next();
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("ScaleHolder iterator doesnt support remove method.");
            }
        };
    }

    static class BitComparator implements Comparator<Bit> {
        @Override
        public int compare(Bit o1, Bit o2) {
            return (o1.getId() < o2.getId() ? -1 : (o1.getId() == o2.getId() ? 0 : 1));
        }

        @Override
        public boolean equals(Object obj) {
            return false;
        }
    }
}
