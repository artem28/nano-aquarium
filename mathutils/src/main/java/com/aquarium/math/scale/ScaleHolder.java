package com.aquarium.math.scale;

import com.aquarium.entities.Bit;
import com.aquarium.entities.Body;
import com.aquarium.entities.Point;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;

import java.util.*;

/**
 * Created by artem on 5/12/14.
 */
public class ScaleHolder implements Iterable<Bit> {

    TreeMap<Float, Bit[]> xScale = new TreeMap<Float, Bit[]>();
    TreeMap<Float, Bit[]> yScale = new TreeMap<Float, Bit[]>();
    TreeMap<Float, Bit[]> zScale = new TreeMap<Float, Bit[]>();

    public void modifyBit(Bit bit, Point point1, Point point2) {
        removeBit(point1);
        addBit(bit, point2);
    }

    public void removeBit(Point point) {
        xScale.remove(point.getX());
        yScale.remove(point.getY());
        zScale.remove(point.getZ());
    }

    public void addBit(Bit bit, Point point) {
        addBit(bit, point.getX(), xScale);
        addBit(bit, point.getY(), yScale);
        addBit(bit, point.getZ(), zScale);
    }

    public void addBits(ScaleHolder bits) {
        for (Bit bit : bits) {
            addBit(bit, bit.getPipe().getOffset());
        }
    }

    private void addBit(Bit bit, Float value, TreeMap<Float, Bit[]> scale) {
        Bit[] bits = scale.get(value);
        if (bits == null) {
            bits = new Bit[]{bit};
        } else {
            bits = Arrays.copyOf(bits, bits.length + 1);
            bits[bits.length - 1] = bit;
        }
        scale.put(value, bits);
    }

    private Set<Bit> getBitSet(TreeMap<Float, Bit[]> scale, float from, float to) {
        Map<Float, Bit[]> submap = scale.subMap(from, to);
        Set<Bit> result = new HashSet<Bit>(submap.size() * 2);
        for (Map.Entry<Float, Bit[]> entry : submap.entrySet()) {
            for (Bit bit : entry.getValue()) {
                result.add(bit);
            }
        }
        return result;
    }

    public Set<Bit> getBitSetInX(float from, float to) {
        return getBitSet(xScale, from, to);
    }

    public Set<Bit> getBitSetInY(float from, float to) {
        return getBitSet(yScale, from, to);
    }

    public Set<Bit> getBitSetInZ(float from, float to) {
        return getBitSet(zScale, from, to);
    }

    public Set<Bit> findAcceptable(Body body) {
        Set<Bit> potentialX = getBitSetInX(body.getPosition().getX() - body.getSize(), body.getPosition().getX() + body.getSize());
        Set<Bit> potentialY = getBitSetInY(body.getPosition().getY() - body.getSize(), body.getPosition().getY() + body.getSize());
        Set<Bit> potentialZ = getBitSetInZ(body.getPosition().getZ() - body.getSize(), body.getPosition().getZ() + body.getSize());

        return Sets.intersection(potentialX, Sets.intersection(potentialY, potentialZ));
    }


    private Set<Bit> getCloseBits(Bit bit1, float bitsize) {
        Set<Bit> bitsX = getBitSetInX(bit1.getPipe().getOffset().getX() - bitsize, bit1.getPipe().getOffset().getX() + bitsize);
        Set<Bit> bitsY = getBitSetInY(bit1.getPipe().getOffset().getY() - bitsize, bit1.getPipe().getOffset().getY() + bitsize);
        Set<Bit> bitsZ = getBitSetInZ(bit1.getPipe().getOffset().getZ() - bitsize, bit1.getPipe().getOffset().getZ() + bitsize);
        return Sets.intersection(bitsX, Sets.intersection(bitsY, bitsZ));
    }


    @Override
    public Iterator<Bit> iterator() {
        final Iterator<Bit[]> bits = xScale.values().iterator();
        return new Iterator<Bit>() {
            int position = 0;
            Bit[] lastGroup;

            @Override
            public boolean hasNext() {
                if (lastGroup == null || lastGroup.length <= position) {
                    return bits.hasNext();
                }
                return true;
            }

            @Override
            public Bit next() {
                if (lastGroup == null || lastGroup.length <= position) {
                    if (!bits.hasNext()) {
                        return null;
                    }
                    lastGroup = bits.next();
                    position = 0;
                }
                return lastGroup[position++];
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("ScaleHolder iterator doesnt support remove method.");
            }
        };
    }
}
