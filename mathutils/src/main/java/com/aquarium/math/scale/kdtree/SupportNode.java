package com.aquarium.math.scale.kdtree;

import java.util.ArrayList;

/**
 * Created by artem on 14.04.15.
 */
public class SupportNode extends DimensionalNode {

    public SupportNode(int dimension, IMultiPoint pt, float worldSize) {
        super(dimension, pt, worldSize);
        support = true;
    }


    @Override
    public void search(IHypercube space, ArrayList<IMultiPoint> results) {
        if (space.getLeft(dimension) < coord) {
            if (below != null) {
                below.search(space, results);
            }
        }
        if (coord < space.getRight(dimension)) {
            if (above != null) {
                above.search(space, results);
            }
        }
    }

    @Override
    public void search(IHypercube space, IVisitKDNode visitor) {
        if (below != null) {
            if (space.getLeft(dimension) < coord) {
                below.search(space, visitor);
            }
        }
        if (above != null) {
            if (coord < space.getRight(dimension)) {
                above.search(space, visitor);
            }
        }
    }
}
