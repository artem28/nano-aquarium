package com.aquarium.math;

import com.aquarium.entities.Bit;
import com.aquarium.entities.PipeVector;
import com.aquarium.entities.Point;
import junit.framework.TestCase;

/**
 * Created by artem on 3/31/14.
 */
public class PipesCanTest extends TestCase {

    public void testGetTimeAndDistance() {
        Bit bit2 = new Bit(2);
        bit2.setTime(0);
        bit2.setVelocity((float)Math.sqrt(2));
        PipeVector pipe2 = new PipeVector();
        pipe2.setFactorXYZ(0, 1, 1);
        Point point2 = new Point();
        point2.setX(0);
        point2.setY(0);
        point2.setZ(0);
        pipe2.setOffset(point2);
        bit2.setPipe(pipe2);

        Bit bit1 = new Bit(1);
        bit1.setTime(0);
        bit1.setVelocity((float)Math.sqrt(2));
        PipeVector pipe1 = new PipeVector();
        pipe1.setFactorXYZ(0, 1, -1);
        Point point1 = new Point();
        point1.setX(1);
        point1.setY(0);
        point1.setZ(1);
        pipe1.setOffset(point1);
        bit1.setPipe(pipe1);

        double t = PipesCan.getTimeClosestDistance(bit1.getPipe(), bit1.getVelocity(), bit2.getPipe(), bit2.getVelocity());
        double d = PipesCan.getDistance(bit1.getPipe(), bit1.getVelocity(), bit2.getPipe(), bit2.getVelocity(), t);
        System.out.println("d=" + d);
        System.out.println("t=" + t);
        assertTrue(d < 1.001 && d > 0.999);
        assertTrue(t < 0.501 && t > 0.499);

    }

    public void testGetDistance() {
        Bit bit2 = new Bit(2);
        bit2.setTime(0);
        bit2.setVelocity((float)Math.sqrt(2));
        PipeVector pipe2 = new PipeVector();
        pipe2.setFactorXYZ(0, 1, 1);
        Point point2 = new Point();
        point2.setX(0);
        point2.setY(0);
        point2.setZ(0);
        pipe2.setOffset(point2);
        bit2.setPipe(pipe2);

        Bit bit1 = new Bit(1);
        bit1.setTime(0);
        bit1.setVelocity((float)Math.sqrt(2));
        PipeVector pipe1 = new PipeVector();
        pipe1.setFactorXYZ(0, -1, -1);
        Point point1 = new Point();
        point1.setX(1);
        point1.setY(0);
        point1.setZ(0);
        pipe1.setOffset(point1);
        bit1.setPipe(pipe1);

        double t = PipesCan.getTimeClosestDistance(bit1.getPipe(), bit1.getVelocity(), bit2.getPipe(), bit2.getVelocity());
        double d = PipesCan.getDistance(bit1.getPipe(), bit1.getVelocity(), bit2.getPipe(), bit2.getVelocity(), t);
        System.out.println("d=" + d);
        System.out.println("t=" + t);
        assertTrue(d < 1.001 && d > 0.999);
        assertTrue(t < 0.001 && t > -0.001);
    }


    public void testGetDistanceForParallel() {
        Bit bit2 = new Bit(2);
        bit2.setTime(0);
        bit2.setVelocity((float)Math.sqrt(2));
        PipeVector pipe2 = new PipeVector();
        pipe2.setFactorXYZ(0, 1, 1);
        Point point2 = new Point();
        point2.setX(0);
        point2.setY(0);
        point2.setZ(0);
        pipe2.setOffset(point2);
        bit2.setPipe(pipe2);

        Bit bit1 = new Bit(1);
        bit1.setTime(0);
        bit1.setVelocity((float)Math.sqrt(2));
        PipeVector pipe1 = new PipeVector();
        pipe1.setFactorXYZ(0, 1, 1);
        Point point1 = new Point();
        point1.setX(1);
        point1.setY(0);
        point1.setZ(0);
        pipe1.setOffset(point1);
        bit1.setPipe(pipe1);

        double t = PipesCan.getTimeClosestDistance(bit1.getPipe(), bit1.getVelocity(), bit2.getPipe(), bit2.getVelocity());
        System.out.println("t=" + t);
        assertTrue(t == 0);
    }
}
