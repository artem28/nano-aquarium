package com.aquarium.math.scale;

import com.aquarium.entities.*;
import com.aquarium.entities.support.ParameterValue;
import com.aquarium.math.PointCan;
import com.google.common.collect.Sets;
import junit.framework.TestCase;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by artem on 5/14/14.
 */
public class KDTreeScaleHolderTest extends TestCase {

    public void test() {
        Bit bit7 = new Bit(7);
        bit7.setTime(0);
        bit7.setVelocity((float) Math.sqrt(2));
        PipeVector pipe7 = new PipeVector();
        pipe7.setFactorXYZ(0, 1, -1);
        Point point7 = new Point();
        point7.setX(0.9f);
        point7.setY(12f / 5f);
        point7.setZ(8f / 6f);
        pipe7.setOffset(point7);
        bit7.setPipe(pipe7);

        Bit bit6 = new Bit(6);
        bit6.setTime(0);
        bit6.setVelocity((float) Math.sqrt(2));
        PipeVector pipe6 = new PipeVector();
        pipe6.setFactorXYZ(0, 1, -1);
        Point point6 = new Point();
        point6.setX(1.9f);
        point6.setY(8f / 6f);
        point6.setZ(12f / 5f);
        pipe6.setOffset(point6);
        bit6.setPipe(pipe6);

        Bit bit5 = new Bit(5);
        bit5.setTime(0);
        bit5.setVelocity((float) Math.sqrt(2));
        PipeVector pipe5 = new PipeVector();
        pipe5.setFactorXYZ(0, 1, -1);
        Point point5 = new Point();
        point5.setX(1.1f);
        point5.setY(5f / 7f);   //0.714285714
        point5.setZ(11f / 9f);  //1.222222222
        pipe5.setOffset(point5);
        bit5.setPipe(pipe5);

        Bit bit4 = new Bit(4);
        bit4.setTime(0);
        bit4.setVelocity((float) Math.sqrt(2));
        PipeVector pipe4 = new PipeVector();
        pipe4.setFactorXYZ(0, 1, -1);
        Point point4 = new Point();
        point4.setX(0.99f);   //0.714285714
        point4.setY(11f / 9f);  //1.222222222
        point4.setZ(11f / 3f);  //3.666666667
        pipe4.setOffset(point4);
        bit4.setPipe(pipe4);

        Bit bit3 = new Bit(3);
        bit3.setTime(0);
        bit3.setVelocity((float) Math.sqrt(2));
        PipeVector pipe3 = new PipeVector();
        pipe3.setFactorXYZ(0, 1, -1);
        Point point3 = new Point();
        point3.setX(11f / 9f);  //1.222222222
        point3.setY(5f / 7f);   //0.714285714
        point3.setZ(11f / 3f);  //3.666666667
        pipe3.setOffset(point3);
        bit3.setPipe(pipe3);

        Bit bit2 = new Bit(2);
        bit2.setTime(0);
        bit2.setVelocity((float) Math.sqrt(2));
        PipeVector pipe2 = new PipeVector();
        pipe2.setFactorXYZ(0, 1, 1);
        Point point2 = new Point();
        point2.setX(5f / 7f);  //0.714285714
        point2.setY(3f / 8f);  //0.375
        point2.setZ(7f / 11f); //0.636363636
        pipe2.setOffset(point2);
        bit2.setPipe(pipe2);

        Bit bit1 = new Bit(1);
        bit1.setTime(0);
        bit1.setVelocity((float) Math.sqrt(2));
        PipeVector pipe1 = new PipeVector();
        pipe1.setFactorXYZ(0, 1, -1);
        Point point1 = new Point();
        point1.setX(5f / 9f);   //0.555555556
        point1.setY(11f / 7f);  //1.571428571
        point1.setZ(7f / 3f);   //2.333333333
        pipe1.setOffset(point1);
        bit1.setPipe(pipe1);

        BitsIndex scale = new KDTreeScaleHolder(100);
        scale.addBit(bit1, bit1.getPipe().getOffset());
        scale.addBit(bit2, bit2.getPipe().getOffset());
        scale.addBit(bit3, bit3.getPipe().getOffset());
        scale.addBit(bit4, bit4.getPipe().getOffset());
        scale.addBit(bit5, bit5.getPipe().getOffset());
        scale.addBit(bit6, bit6.getPipe().getOffset());
        scale.addBit(bit7, bit7.getPipe().getOffset());


        Set<Bit> bits = Sets.newHashSet(scale.getBitSetInX(0.7f, 2f));
        assertTrue(bits.contains(new Bit(3)));
        assertTrue(bits.contains(new Bit(2)));
        assertTrue(bits.contains(new Bit(4)));
        assertTrue(bits.contains(new Bit(5)));
        assertTrue(bits.contains(new Bit(6)));
        assertTrue(bits.contains(new Bit(7)));
        assertTrue(!bits.contains(new Bit(1)));

        bits = Sets.newHashSet(scale.getBitSetInZ(2, 3));
        assertTrue(!bits.contains(new Bit(3)));
        assertTrue(bits.contains(new Bit(1)));
        assertTrue(bits.contains(new Bit(6)));
        assertTrue(!bits.contains(new Bit(2)));
        assertTrue(!bits.contains(new Bit(4)));
        assertTrue(!bits.contains(new Bit(5)));
        assertTrue(!bits.contains(new Bit(7)));


/*
        scale.modifyBit(bit2, bit2.getPipe().getOffset(), new Point(1.22222, 3.22222, 1.234345));

        bits = scale.getBitSetInY(3, 4);
        assertTrue(!bits.contains(new Bit(3)));
        assertTrue(bits.contains(new Bit(2)));
        assertTrue(!bits.contains(new Bit(1)));
*/

    }

    public void testIterator() {
        Bit bit5 = new Bit(5);
        bit5.setTime(0);
        bit5.setVelocity((float) Math.sqrt(2));
        PipeVector pipe5 = new PipeVector();
        pipe5.setFactorXYZ(0, 1, -1);
        Point point5 = new Point();
        point5.setX(1);
        point5.setY(5f / 7f);   //0.714285714
        point5.setZ(11f / 3f);  //3.666666667
        pipe5.setOffset(point5);
        bit5.setPipe(pipe5);

        Bit bit4 = new Bit(4);
        bit4.setTime(0);
        bit4.setVelocity((float) Math.sqrt(2));
        PipeVector pipe4 = new PipeVector();
        pipe4.setFactorXYZ(0, 1, -1);
        Point point4 = new Point();
        point4.setX(1);
        point4.setY(5f / 7f);   //0.714285714
        point4.setZ(11f / 3f);  //3.666666667
        pipe4.setOffset(point4);
        bit4.setPipe(pipe4);

        Bit bit3 = new Bit(3);
        bit3.setTime(0);
        bit3.setVelocity((float) Math.sqrt(2));
        PipeVector pipe3 = new PipeVector();
        pipe3.setFactorXYZ(0, 1, -1);
        Point point3 = new Point();
        point3.setX(11f / 9f);  //1.222222222
        point3.setY(5f / 7f);   //0.714285714
        point3.setZ(11f / 3f);  //3.666666667
        pipe3.setOffset(point3);
        bit3.setPipe(pipe3);

        Bit bit2 = new Bit(2);
        bit2.setTime(0);
        bit2.setVelocity((float) Math.sqrt(2));
        PipeVector pipe2 = new PipeVector();
        pipe2.setFactorXYZ(0, 1, 1);
        Point point2 = new Point();
        point2.setX(5f / 7f);  //0.714285714
        point2.setY(3f / 8f);  //0.375
        point2.setZ(7f / 11f); //0.636363636
        pipe2.setOffset(point2);
        bit2.setPipe(pipe2);

        Bit bit1 = new Bit(1);
        bit1.setTime(0);
        bit1.setVelocity((float) Math.sqrt(2));
        PipeVector pipe1 = new PipeVector();
        pipe1.setFactorXYZ(0, 1, -1);
        Point point1 = new Point();
        point1.setX(5f / 9f);   //0.555555556
        point1.setY(11f / 7f);  //1.571428571
        point1.setZ(7f / 3f);   //2.333333333
        pipe1.setOffset(point1);
        bit1.setPipe(pipe1);

        BitsIndex scale = new KDTreeScaleHolder(100);
        scale.addBit(bit1, bit1.getPipe().getOffset());
        scale.addBit(bit2, bit2.getPipe().getOffset());
        scale.addBit(bit3, bit3.getPipe().getOffset());
        scale.addBit(bit4, bit4.getPipe().getOffset());
        scale.addBit(bit5, bit5.getPipe().getOffset());

        Set<Bit> result = new HashSet();
        for (Bit bit : scale) {
            result.add(bit);
        }

        assertTrue(result.size() == 5);
        assertTrue(result.contains(bit1));
        assertTrue(result.contains(bit2));
        assertTrue(result.contains(bit3));
        assertTrue(result.contains(bit4));
        assertTrue(result.contains(bit5));

    }

    public void testIterator2() {
        Bit bit5 = new Bit(5);
        bit5.setTime(0);
        bit5.setVelocity((float) Math.sqrt(2));
        PipeVector pipe5 = new PipeVector();
        pipe5.setFactorXYZ(0, 1, -1);
        Point point5 = new Point();
        point5.setX(100);
        point5.setY(5f / 7f);   //0.714285714
        point5.setZ(11f / 3f);  //3.666666667
        pipe5.setOffset(point5);
        bit5.setPipe(pipe5);

        Bit bit4 = new Bit(4);
        bit4.setTime(0);
        bit4.setVelocity((float) Math.sqrt(2));
        PipeVector pipe4 = new PipeVector();
        pipe4.setFactorXYZ(0, 1, -1);
        Point point4 = new Point();
        point4.setX(100);
        point4.setY(5f / 7f);   //0.714285714
        point4.setZ(11f / 3f);  //3.666666667
        pipe4.setOffset(point4);
        bit4.setPipe(pipe4);

        Bit bit3 = new Bit(3);
        bit3.setTime(0);
        bit3.setVelocity((float) Math.sqrt(2));
        PipeVector pipe3 = new PipeVector();
        pipe3.setFactorXYZ(0, 1, -1);
        Point point3 = new Point();
        point3.setX(11f / 9f);  //1.222222222
        point3.setY(5f / 7f);   //0.714285714
        point3.setZ(11f / 3f);  //3.666666667
        pipe3.setOffset(point3);
        bit3.setPipe(pipe3);

        Bit bit2 = new Bit(2);
        bit2.setTime(0);
        bit2.setVelocity((float) Math.sqrt(2));
        PipeVector pipe2 = new PipeVector();
        pipe2.setFactorXYZ(0, 1, 1);
        Point point2 = new Point();
        point2.setX(5f / 7f);  //0.714285714
        point2.setY(3f / 8f);  //0.375
        point2.setZ(7f / 11f); //0.636363636
        pipe2.setOffset(point2);
        bit2.setPipe(pipe2);

        Bit bit1 = new Bit(1);
        bit1.setTime(0);
        bit1.setVelocity((float) Math.sqrt(2));
        PipeVector pipe1 = new PipeVector();
        pipe1.setFactorXYZ(0, 1, -1);
        Point point1 = new Point();
        point1.setX(5f / 9f);   //0.555555556
        point1.setY(11f / 7f);  //1.571428571
        point1.setZ(7f / 3f);   //2.333333333
        pipe1.setOffset(point1);
        bit1.setPipe(pipe1);

        BitsIndex scale = new KDTreeScaleHolder(100);
        scale.addBit(bit1, bit1.getPipe().getOffset());
        scale.addBit(bit2, bit2.getPipe().getOffset());
        scale.addBit(bit3, bit3.getPipe().getOffset());
        scale.addBit(bit4, bit4.getPipe().getOffset());
        scale.addBit(bit5, bit5.getPipe().getOffset());

        Set<Bit> result = new HashSet();
        for (Bit bit : scale) {
            result.add(bit);
        }

        assertTrue(result.size() == 5);
        assertTrue(result.contains(bit1));
        assertTrue(result.contains(bit2));
        assertTrue(result.contains(bit3));
        assertTrue(result.contains(bit4));
        assertTrue(result.contains(bit5));

        Set<Bit> bits = Sets.newHashSet(scale.getCloseBits(bit5, 0.3f));
        assertTrue(bits.size() == 1);
        assertTrue(bits.contains(new Bit(4)));

        bits = Sets.newHashSet(scale.getCloseBits(bit2, 3.5f));
        assertTrue(bits.size() == 2);
        assertTrue(bits.contains(new Bit(1)));
        assertTrue(bits.contains(new Bit(3)));

    }

    public void testIterator3() {
        Bit bit5 = new Bit(5);
        bit5.setTime(0);
        bit5.setVelocity((float) Math.sqrt(2));
        PipeVector pipe5 = new PipeVector();
        pipe5.setFactorXYZ(0, 1, -1);
        Point point5 = new Point();
        point5.setX(-1);
        point5.setY(5f / 7f);   //0.714285714
        point5.setZ(11f / 3f);  //3.666666667
        pipe5.setOffset(point5);
        bit5.setPipe(pipe5);

        Bit bit4 = new Bit(4);
        bit4.setTime(0);
        bit4.setVelocity((float) Math.sqrt(2));
        PipeVector pipe4 = new PipeVector();
        pipe4.setFactorXYZ(0, 1, -1);
        Point point4 = new Point();
        point4.setX(-1);
        point4.setY(5f / 7f);   //0.714285714
        point4.setZ(11f / 3f);  //3.666666667
        pipe4.setOffset(point4);
        bit4.setPipe(pipe4);

        Bit bit3 = new Bit(3);
        bit3.setTime(0);
        bit3.setVelocity((float) Math.sqrt(2));
        PipeVector pipe3 = new PipeVector();
        pipe3.setFactorXYZ(0, 1, -1);
        Point point3 = new Point();
        point3.setX(11f / 9f);  //1.222222222
        point3.setY(5f / 7f);   //0.714285714
        point3.setZ(11f / 3f);  //3.666666667
        pipe3.setOffset(point3);
        bit3.setPipe(pipe3);

        Bit bit2 = new Bit(2);
        bit2.setTime(0);
        bit2.setVelocity((float) Math.sqrt(2));
        PipeVector pipe2 = new PipeVector();
        pipe2.setFactorXYZ(0, 1, 1);
        Point point2 = new Point();
        point2.setX(5f / 7f);  //0.714285714
        point2.setY(3f / 8f);  //0.375
        point2.setZ(7f / 11f); //0.636363636
        pipe2.setOffset(point2);
        bit2.setPipe(pipe2);

        Bit bit1 = new Bit(1);
        bit1.setTime(0);
        bit1.setVelocity((float) Math.sqrt(2));
        PipeVector pipe1 = new PipeVector();
        pipe1.setFactorXYZ(0, 1, -1);
        Point point1 = new Point();
        point1.setX(5f / 9f);   //0.555555556
        point1.setY(11f / 7f);  //1.571428571
        point1.setZ(7f / 3f);   //2.333333333
        pipe1.setOffset(point1);
        bit1.setPipe(pipe1);

        BitsIndex scale = new KDTreeScaleHolder(100);
        scale.addBit(bit1, bit1.getPipe().getOffset());
        scale.addBit(bit2, bit2.getPipe().getOffset());
        scale.addBit(bit3, bit3.getPipe().getOffset());
        scale.addBit(bit4, bit4.getPipe().getOffset());
        scale.addBit(bit5, bit5.getPipe().getOffset());

        final Set<Bit> insiders = new HashSet();
        Iterable<Bit> bits = scale.findAcceptable(new Modificator() {
            @Override
            public void observe(float dt, World world, Iterable<Bit> bits) {
            }

            @Override
            public int getId() {
                return 0;
            }

            @Override
            public boolean isAcceptable(Bit bit) {
                return true;
            }

            @Override
            public Point getPosition() {
                return new Point(-3, 0, 1);
            }

            @Override
            public void flush(float time) {

            }

            @Override
            public void postAction() {

            }

            @Override
            public Point getImpuls() {
                return null;
            }

            @Override
            public boolean isMoveable() {
                return false;
            }

            @Override
            public void setPosition(Point point) {

            }

            @Override
            public List<String> getParametersNames() {
                return null;
            }

            @Override
            public List<ParameterValue> getParameterValues(String name) {
                return null;
            }

            @Override
            public float getSize() {
                return 6;
            }

            @Override
            public int compareTo(Object o) {
                return 0;
            }
        });
        double distance = 0;
        for (Bit bit : bits) {
            insiders.add(bit);
            distance = PointCan.getDistance(bit.getPipe().getOffset(), new Point(-3, 0, 1));
        }

        assertTrue(insiders.size() == 2);
        assertTrue(insiders.contains(bit4));
        assertTrue(insiders.contains(bit5));

        Set<Bit> result = new HashSet();
        for (Bit bit : scale) {
            result.add(bit);
        }

        assertTrue(result.size() == 5);
        assertTrue(result.contains(bit1));
        assertTrue(result.contains(bit2));
        assertTrue(result.contains(bit3));
        assertTrue(result.contains(bit4));
        assertTrue(result.contains(bit5));

    }


    public void test21() {
        Bit b = new Bit(1);
        b.getPipe().setOffset(new Point(21.510660679598413f, 0.1787480347553498f, 1.5612892129841187f));
        KDTreeScaleHolder index = new KDTreeScaleHolder(100);
        Modificator m = new Modificator() {
            @Override
            public void observe(float dt, World world, Iterable<Bit> bits) {

            }

            @Override
            public void flush(float time) {

            }

            @Override
            public void postAction() {

            }

            @Override
            public Point getImpuls() {
                return null;
            }

            @Override
            public boolean isMoveable() {
                return false;
            }

            @Override
            public void setPosition(Point point) {

            }

            @Override
            public boolean isAcceptable(Bit bit) {
                return true;
            }

            @Override
            public Point getPosition() {
                return new Point(0, 0, 0);
            }

            @Override
            public float getSize() {
                return 1;
            }

            @Override
            public int getId() {
                return 0;
            }

            @Override
            public int compareTo(Object o) {
                return 0;
            }

            @Override
            public List<String> getParametersNames() {
                return null;
            }

            @Override
            public List<ParameterValue> getParameterValues(String name) {
                return null;
            }
        };
        Set<Bit> result = new HashSet<Bit>();
        for (Bit bit : index.findAcceptable(m)) {
            result.add(bit);
        }

        assertTrue(result.isEmpty());
    }

    public void testFastClosest() {
        Set<Bit> result = new HashSet();
        BitsIndex scale = new KDTreeScaleHolder(100);

        float x = 3;
        float y = -3;
        float z = 3;
        Bit b = new Bit(55555);
        b.setPipe(new PipeVector(new Point(x, y, z), 0, 0, 0));

        for (int i = 0; i < 1000; i++) {
            Bit bit1 = new Bit(i);
            bit1.setTime(0);
            bit1.setVelocity((float) Math.sqrt(2));
            PipeVector pipe1 = new PipeVector();
            pipe1.setFactorXYZ(0, 1, -1);
            Point point1 = new Point();
            point1.setX((float) ((Math.random() - 0.5) * 2 + x));
            point1.setY((float) ((Math.random() - 0.5) * 2 + y));
            point1.setZ((float) ((Math.random() - 0.5) * 2 + z));
            pipe1.setOffset(point1);
            bit1.setPipe(pipe1);
            result.add(bit1);
            scale.addBit(bit1, bit1.getPipe().getOffset());
        }

        Set<Bit> closeBits = new HashSet();
        for (Bit bit : scale.getCloseBits(b, 0.3f)) {
            closeBits.add(bit);
        }
        System.out.println("closeBits size=" + closeBits.size());
        for (Bit bit : result) {
            assertEquals(isClose(b, bit, 0.3f), closeBits.contains(bit));
        }

    }

    public void testFastClosestShort() {
        for (float x = -3; x < 3; x++) {
            for (float y = -3; y < 3; y++) {
                for (float z = -3; z < 3; z++) {
                    doFastClosestStep(x, y, z);
                }
            }
        }

    }

    public void doFastClosestStep(float x,
                                  float y,
                                  float z) {
        Set<Bit> result = new HashSet();
        BitsIndex scale = new KDTreeScaleHolder(100);

        Bit b = new Bit(55555);
        b.setPipe(new PipeVector(new Point(x, y, z), 0, 0, 0));

        for (int i = 0; i < 1000; i++) {
            Bit bit1 = new Bit(i);
            bit1.setTime(0);
            bit1.setVelocity((float) Math.sqrt(2));
            PipeVector pipe1 = new PipeVector();
            pipe1.setFactorXYZ(0, 1, -1);
            Point point1 = new Point();
            point1.setX((float) ((Math.random() - 0.5) * 2 + x));
            point1.setY((float) ((Math.random() - 0.5) * 2 + y));
            point1.setZ((float) ((Math.random() - 0.5) * 2 + z));
            pipe1.setOffset(point1);
            bit1.setPipe(pipe1);
            result.add(bit1);
            scale.addBit(bit1, bit1.getPipe().getOffset());
        }

        Set<Bit> closeBits = new HashSet();
        for (Bit bit : scale.getCloseBits(b, 0.3f, 2)) {
            closeBits.add(bit);
        }
        //    System.out.println("closeBits size=" + closeBits.size());
        int count = 0;
        for (Bit bit : result) {
            if (isClose(b, bit, 0.3f)) {
                if (closeBits.contains(bit)) {
                    count++;
                }
            }
        }
        assertEquals(count, 2);

    }

    private boolean isClose(Bit b, Bit a, float bitSize) {
        return (Math.abs(b.getPipe().getOffset().getX() - a.getPipe().getOffset().getX()) <= bitSize) &&
                (Math.abs(b.getPipe().getOffset().getY() - a.getPipe().getOffset().getY()) <= bitSize) &&
                (Math.abs(b.getPipe().getOffset().getZ() - a.getPipe().getOffset().getZ()) <= bitSize);
    }

    public void testGetBitsinRange() {
        Set<Bit> result = new HashSet();
        BitsIndex scale = new KDTreeScaleHolder(100);

        float x = 3;
        float y = -3;
        float z = 3;
        Bit b = new Bit(55555);
        b.setPipe(new PipeVector(new Point(x, y, z), 0, 0, 0));

        for (int i = 0; i < 1000; i++) {
            Bit bit1 = new Bit(i);
            bit1.setTime(0);
            bit1.setVelocity((float) Math.sqrt(2));
            PipeVector pipe1 = new PipeVector();
            pipe1.setFactorXYZ(0, 1, -1);
            Point point1 = new Point();
            point1.setX((float) ((Math.random() - 0.5) * 10 + x));
            point1.setY((float) ((Math.random() - 0.5) * 10 + y));
            point1.setZ((float) ((Math.random() - 0.5) * 10 + z));
            pipe1.setOffset(point1);
            bit1.setPipe(pipe1);
            result.add(bit1);
            scale.addBit(bit1, bit1.getPipe().getOffset());
        }

        Set<Bit> closeBits1 = new HashSet();
        for (Bit bit : scale.getBitSetInX(x - 1, x + 1)) {
            closeBits1.add(bit);
        }
        Set<Bit> closeBits2 = new HashSet();
        for (Bit bit : scale.getBitSetInY(y - 1, y + 1)) {
            closeBits2.add(bit);
        }
        Set<Bit> closeBits3 = new HashSet();
        for (Bit bit : scale.getBitSetInZ(z - 1, z + 1)) {
            closeBits3.add(bit);
        }
        System.out.println("closeBits1 size=" + closeBits1.size());
        System.out.println("closeBits2 size=" + closeBits2.size());
        System.out.println("closeBits3 size=" + closeBits3.size());
        for (Bit bit : result) {
            assertEquals(isInXRange(bit, x - 1, x + 1), closeBits1.contains(bit));
            assertEquals(isInYRange(bit, y - 1, y + 1), closeBits2.contains(bit));
            assertEquals(isInZRange(bit, z - 1, z + 1), closeBits3.contains(bit));
        }

    }

    private boolean isInXRange(Bit bit, float v, float v1) {
        return bit.getPipe().getOffset().getX() >= v && bit.getPipe().getOffset().getX() <= v1;
    }

    private boolean isInYRange(Bit bit, float v, float v1) {
        return bit.getPipe().getOffset().getY() >= v && bit.getPipe().getOffset().getY() <= v1;
    }

    private boolean isInZRange(Bit bit, float v, float v1) {
        return bit.getPipe().getOffset().getZ() >= v && bit.getPipe().getOffset().getZ() <= v1;
    }


    public void testFindInBody() {
        Set<Bit> result = new HashSet();
        BitsIndex scale = new KDTreeScaleHolder(100);

        float x = 3;
        float y = -3;
        float z = 3;
        Bit b = new Bit(55555);
        b.setPipe(new PipeVector(new Point(x, y, z), 0, 0, 0));

        for (int i = 0; i < 1000; i++) {
            Bit bit1 = new Bit(i);
            bit1.setTime(0);
            bit1.setVelocity((float) Math.sqrt(2));
            PipeVector pipe1 = new PipeVector();
            pipe1.setFactorXYZ(0, 1, -1);
            Point point1 = new Point();
            point1.setX((float) ((Math.random() - 0.5) * 10 + x));
            point1.setY((float) ((Math.random() - 0.5) * 10 + y));
            point1.setZ((float) ((Math.random() - 0.5) * 10 + z));
            pipe1.setOffset(point1);
            bit1.setPipe(pipe1);
            result.add(bit1);
            scale.addBit(bit1, bit1.getPipe().getOffset());
        }
        Body body = new TestBody(x, y, z, 2);
        Set<Bit> closeBits1 = new HashSet();
        for (Bit bit : scale.findAcceptable(body)) {
            closeBits1.add(bit);
        }

        System.out.println("closeBits1 size=" + closeBits1.size());
        for (Bit bit : result) {
            assertEquals(body.isAcceptable(bit), closeBits1.contains(bit));
        }
    }

    private class TestBody implements Body {
        private final Point position;
        private final float radius;

        public TestBody(float x, float y, float z, float radius) {
            position = new Point(x,y,z);
            this.radius = radius;
        }

        @Override
        public boolean isAcceptable(Bit bit) {
            Point point = bit.getPipe().getOffset();

            double dist = PointCan.getDistance(point, position);

            return dist < radius;

        }

        @Override
        public Point getPosition() {
            return position;
        }

        @Override
        public float getSize() {
            return radius*2;
        }

        @Override
        public int getId() {
            return 1;
        }
    }
}
