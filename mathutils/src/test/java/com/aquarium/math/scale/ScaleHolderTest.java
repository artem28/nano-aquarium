package com.aquarium.math.scale;

import com.aquarium.entities.Bit;
import com.aquarium.entities.PipeVector;
import com.aquarium.entities.Point;
import junit.framework.TestCase;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by artem on 5/14/14.
 */
public class ScaleHolderTest extends TestCase {

    public void test() {
        Bit bit3 = new Bit(3);
        bit3.setTime(0);
        bit3.setVelocity((float)Math.sqrt(2));
        PipeVector pipe3 = new PipeVector();
        pipe3.setFactorXYZ(0, 1, -1);
        Point point3 = new Point();
        point3.setX(11f / 9f);  //1.222222222
        point3.setY(5f / 7f);   //0.714285714
        point3.setZ(11f / 3f);  //3.666666667
        pipe3.setOffset(point3);
        bit3.setPipe(pipe3);

        Bit bit2 = new Bit(2);
        bit2.setTime(0);
        bit2.setVelocity((float)Math.sqrt(2));
        PipeVector pipe2 = new PipeVector();
        pipe2.setFactorXYZ(0, 1, 1);
        Point point2 = new Point();
        point2.setX(5f / 7f);  //0.714285714
        point2.setY(3f / 8f);  //0.375
        point2.setZ(7f / 11f); //0.636363636
        pipe2.setOffset(point2);
        bit2.setPipe(pipe2);

        Bit bit1 = new Bit(1);
        bit1.setTime(0);
        bit1.setVelocity((float)Math.sqrt(2));
        PipeVector pipe1 = new PipeVector();
        pipe1.setFactorXYZ(0, 1, -1);
        Point point1 = new Point();
        point1.setX(5f / 9f);   //0.555555556
        point1.setY(11f / 7f);  //1.571428571
        point1.setZ(7f / 3f);   //2.333333333
        pipe1.setOffset(point1);
        bit1.setPipe(pipe1);

        ScaleHolder scale = new ScaleHolder();
        scale.addBit(bit1, bit1.getPipe().getOffset());
        scale.addBit(bit2, bit2.getPipe().getOffset());
        scale.addBit(bit3, bit3.getPipe().getOffset());

        Set<Bit> bits = scale.getBitSetInX(0.7f, 2);
        assertTrue(bits.contains(new Bit(3)));
        assertTrue(bits.contains(new Bit(2)));
        assertTrue(!bits.contains(new Bit(1)));

        bits = scale.getBitSetInZ(2, 3);
        assertTrue(!bits.contains(new Bit(3)));
        assertTrue(bits.contains(new Bit(1)));
        assertTrue(!bits.contains(new Bit(2)));

        scale.modifyBit(bit2, bit2.getPipe().getOffset(), new Point(1.22222f, 3.22222f, 1.234345f));

        bits = scale.getBitSetInY(3, 4);
        assertTrue(!bits.contains(new Bit(3)));
        assertTrue(bits.contains(new Bit(2)));
        assertTrue(!bits.contains(new Bit(1)));

    }

    public void testIterator() {
        Bit bit5 = new Bit(5);
        bit5.setTime(0);
        bit5.setVelocity((float)Math.sqrt(2));
        PipeVector pipe5 = new PipeVector();
        pipe5.setFactorXYZ(0, 1, -1);
        Point point5 = new Point();
        point5.setX(1);
        point5.setY(5f / 7f);   //0.714285714
        point5.setZ(11f / 3f);  //3.666666667
        pipe5.setOffset(point5);
        bit5.setPipe(pipe5);

        Bit bit4 = new Bit(4);
        bit4.setTime(0);
        bit4.setVelocity((float)Math.sqrt(2));
        PipeVector pipe4 = new PipeVector();
        pipe4.setFactorXYZ(0, 1, -1);
        Point point4 = new Point();
        point4.setX(1);
        point4.setY(5f / 7f);   //0.714285714
        point4.setZ(11f / 3f);  //3.666666667
        pipe4.setOffset(point4);
        bit4.setPipe(pipe4);

        Bit bit3 = new Bit(3);
        bit3.setTime(0);
        bit3.setVelocity((float)Math.sqrt(2));
        PipeVector pipe3 = new PipeVector();
        pipe3.setFactorXYZ(0, 1, -1);
        Point point3 = new Point();
        point3.setX(11f / 9f);  //1.222222222
        point3.setY(5f / 7f);   //0.714285714
        point3.setZ(11f / 3f);  //3.666666667
        pipe3.setOffset(point3);
        bit3.setPipe(pipe3);

        Bit bit2 = new Bit(2);
        bit2.setTime(0);
        bit2.setVelocity((float)Math.sqrt(2));
        PipeVector pipe2 = new PipeVector();
        pipe2.setFactorXYZ(0, 1, 1);
        Point point2 = new Point();
        point2.setX(5f / 7f);  //0.714285714
        point2.setY(3f / 8f);  //0.375
        point2.setZ(7f / 11f); //0.636363636
        pipe2.setOffset(point2);
        bit2.setPipe(pipe2);

        Bit bit1 = new Bit(1);
        bit1.setTime(0);
        bit1.setVelocity((float)Math.sqrt(2));
        PipeVector pipe1 = new PipeVector();
        pipe1.setFactorXYZ(0, 1, -1);
        Point point1 = new Point();
        point1.setX(5f / 9f);   //0.555555556
        point1.setY(11f / 7f);  //1.571428571
        point1.setZ(7f / 3f);   //2.333333333
        pipe1.setOffset(point1);
        bit1.setPipe(pipe1);

        ScaleHolder scale = new ScaleHolder();
        scale.addBit(bit1, bit1.getPipe().getOffset());
        scale.addBit(bit2, bit2.getPipe().getOffset());
        scale.addBit(bit3, bit3.getPipe().getOffset());
        scale.addBit(bit4, bit4.getPipe().getOffset());
        scale.addBit(bit5, bit5.getPipe().getOffset());

        Set<Bit> result = new HashSet();
        for (Bit bit : scale) {
            result.add(bit);
        }

        assertTrue(result.size() == 5);
        assertTrue(result.contains(bit1));
        assertTrue(result.contains(bit2));
        assertTrue(result.contains(bit3));
        assertTrue(result.contains(bit4));
        assertTrue(result.contains(bit5));

    }

    public void testIterator2() {
        Bit bit5 = new Bit(5);
        bit5.setTime(0);
        bit5.setVelocity((float)Math.sqrt(2));
        PipeVector pipe5 = new PipeVector();
        pipe5.setFactorXYZ(0, 1, -1);
        Point point5 = new Point();
        point5.setX(100);
        point5.setY(5f / 7f);   //0.714285714
        point5.setZ(11f / 3f);  //3.666666667
        pipe5.setOffset(point5);
        bit5.setPipe(pipe5);

        Bit bit4 = new Bit(4);
        bit4.setTime(0);
        bit4.setVelocity((float)Math.sqrt(2));
        PipeVector pipe4 = new PipeVector();
        pipe4.setFactorXYZ(0, 1, -1);
        Point point4 = new Point();
        point4.setX(100);
        point4.setY(5f / 7f);   //0.714285714
        point4.setZ(11f / 3f);  //3.666666667
        pipe4.setOffset(point4);
        bit4.setPipe(pipe4);

        Bit bit3 = new Bit(3);
        bit3.setTime(0);
        bit3.setVelocity((float)Math.sqrt(2));
        PipeVector pipe3 = new PipeVector();
        pipe3.setFactorXYZ(0, 1, -1);
        Point point3 = new Point();
        point3.setX(11f / 9f);  //1.222222222
        point3.setY(5f / 7f);   //0.714285714
        point3.setZ(11f / 3f);  //3.666666667
        pipe3.setOffset(point3);
        bit3.setPipe(pipe3);

        Bit bit2 = new Bit(2);
        bit2.setTime(0);
        bit2.setVelocity((float)Math.sqrt(2));
        PipeVector pipe2 = new PipeVector();
        pipe2.setFactorXYZ(0, 1, 1);
        Point point2 = new Point();
        point2.setX(5f / 7f);  //0.714285714
        point2.setY(3f / 8f);  //0.375
        point2.setZ(7f / 11f); //0.636363636
        pipe2.setOffset(point2);
        bit2.setPipe(pipe2);

        Bit bit1 = new Bit(1);
        bit1.setTime(0);
        bit1.setVelocity((float)Math.sqrt(2));
        PipeVector pipe1 = new PipeVector();
        pipe1.setFactorXYZ(0, 1, -1);
        Point point1 = new Point();
        point1.setX(5f / 9f);   //0.555555556
        point1.setY(11f / 7f);  //1.571428571
        point1.setZ(7f / 3f);   //2.333333333
        pipe1.setOffset(point1);
        bit1.setPipe(pipe1);

        ScaleHolder scale = new ScaleHolder();
        scale.addBit(bit1, bit1.getPipe().getOffset());
        scale.addBit(bit2, bit2.getPipe().getOffset());
        scale.addBit(bit3, bit3.getPipe().getOffset());
        scale.addBit(bit4, bit4.getPipe().getOffset());
        scale.addBit(bit5, bit5.getPipe().getOffset());

        Set<Bit> result = new HashSet();
        for (Bit bit : scale) {
            result.add(bit);
        }

        assertTrue(result.size() == 5);
        assertTrue(result.contains(bit1));
        assertTrue(result.contains(bit2));
        assertTrue(result.contains(bit3));
        assertTrue(result.contains(bit4));
        assertTrue(result.contains(bit5));

    }

    public void testIterator3() {
        Bit bit5 = new Bit(5);
        bit5.setTime(0);
        bit5.setVelocity((float)Math.sqrt(2));
        PipeVector pipe5 = new PipeVector();
        pipe5.setFactorXYZ(0, 1, -1);
        Point point5 = new Point();
        point5.setX(-1);
        point5.setY(5f / 7f);   //0.714285714
        point5.setZ(11f / 3f);  //3.666666667
        pipe5.setOffset(point5);
        bit5.setPipe(pipe5);

        Bit bit4 = new Bit(4);
        bit4.setTime(0);
        bit4.setVelocity((float)Math.sqrt(2));
        PipeVector pipe4 = new PipeVector();
        pipe4.setFactorXYZ(0, 1, -1);
        Point point4 = new Point();
        point4.setX(-1);
        point4.setY(5f / 7f);   //0.714285714
        point4.setZ(11f / 3f);  //3.666666667
        pipe4.setOffset(point4);
        bit4.setPipe(pipe4);

        Bit bit3 = new Bit(3);
        bit3.setTime(0);
        bit3.setVelocity((float)Math.sqrt(2));
        PipeVector pipe3 = new PipeVector();
        pipe3.setFactorXYZ(0, 1, -1);
        Point point3 = new Point();
        point3.setX(11f / 9f);  //1.222222222
        point3.setY(5f / 7f);   //0.714285714
        point3.setZ(11f / 3f);  //3.666666667
        pipe3.setOffset(point3);
        bit3.setPipe(pipe3);

        Bit bit2 = new Bit(2);
        bit2.setTime(0);
        bit2.setVelocity((float)Math.sqrt(2));
        PipeVector pipe2 = new PipeVector();
        pipe2.setFactorXYZ(0, 1, 1);
        Point point2 = new Point();
        point2.setX(5f / 7f);  //0.714285714
        point2.setY(3f / 8f);  //0.375
        point2.setZ(7f / 11f); //0.636363636
        pipe2.setOffset(point2);
        bit2.setPipe(pipe2);

        Bit bit1 = new Bit(1);
        bit1.setTime(0);
        bit1.setVelocity((float)Math.sqrt(2));
        PipeVector pipe1 = new PipeVector();
        pipe1.setFactorXYZ(0, 1, -1);
        Point point1 = new Point();
        point1.setX(5f / 9f);   //0.555555556
        point1.setY(11f / 7f);  //1.571428571
        point1.setZ(7f / 3f);   //2.333333333
        pipe1.setOffset(point1);
        bit1.setPipe(pipe1);

        ScaleHolder scale = new ScaleHolder();
        scale.addBit(bit1, bit1.getPipe().getOffset());
        scale.addBit(bit2, bit2.getPipe().getOffset());
        scale.addBit(bit3, bit3.getPipe().getOffset());
        scale.addBit(bit4, bit4.getPipe().getOffset());
        scale.addBit(bit5, bit5.getPipe().getOffset());

        Set<Bit> result = new HashSet();
        for (Bit bit : scale) {
            result.add(bit);
        }

        assertTrue(result.size() == 5);
        assertTrue(result.contains(bit1));
        assertTrue(result.contains(bit2));
        assertTrue(result.contains(bit3));
        assertTrue(result.contains(bit4));
        assertTrue(result.contains(bit5));

    }
}
