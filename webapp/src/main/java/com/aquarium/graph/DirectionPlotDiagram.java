package com.aquarium.graph;

import com.aquarium.entities.*;
import com.aquarium.math.DoubleCan;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.util.Set;

/**
 * Created by artem on 10/13/14.
 */
public class DirectionPlotDiagram {

    static final Logger log = LogManager.getLogger("IncrementalEngine");
    private int id;
    private int xSize;
    private int ySize;
    private double factor;
    private Set<PipeVector> vectors;

    public DirectionPlotDiagram(int id, double size, double factor, Set<PipeVector> vectors) {
        this.id = id;
        this.xSize = (int)Math.round(size*factor);
        this.ySize = (int)Math.round(size*factor);
        this.factor = factor;
        this.vectors = vectors;
    }

    public void drawInFile() {
        BufferedImage image = drawImage();
        try {
            writeImageInFile(image);
        } catch (Exception e) {
            log.error("!!! Can not save the picture: " + e.getMessage());
        }
    }

    private void writeImageInFile(BufferedImage image) throws Exception {
        ImageIO.write(image, "jpg", new FileOutputStream("direction_" + id + ".jpg"));
    }

    public BufferedImage drawImage() {
        BufferedImage bufferedImage = new BufferedImage(xSize, ySize,
                BufferedImage.TYPE_INT_ARGB);

        //Draw an oval
        Graphics g = bufferedImage.getGraphics();
        g.setColor(Color.blue);
        for (PipeVector vector : vectors) {
            g.drawLine((int) Math.round(vector.getOffset().getX() * factor),
                    ySize - ((int) Math.round(vector.getOffset().getY() * factor)),
                    (int) Math.round((vector.getOffset().getX()) * factor + vector.getFactorX()*15),
                    ySize - ((int) Math.round((vector.getOffset().getY()) * factor + vector.getFactorY()*15)));

            g.fillOval((int) Math.round(vector.getOffset().getX() * factor) -2,
                    ySize - ((int) Math.round(vector.getOffset().getY() * factor)+2),
                    5, 5);
        }

        g.dispose();

        return bufferedImage;
    }

    static public  PipeVector getPipeVectorForImage(Bit bit, PipeVector surfaceOrientation, World world) {
        if (DoubleCan.areEqual(surfaceOrientation.getFactorX(), 0) && DoubleCan.areEqual(surfaceOrientation.getFactorY(), 0)) {
            return new PipeVector(new com.aquarium.entities.Point(bit.getPipe().getOffset().getX() + world.getSide() / 2,
                    bit.getPipe().getOffset().getY() + world.getSide() / 2, 0),
                    bit.getPipe().getFactorX(), bit.getPipe().getFactorY(), 0);
        } else if (DoubleCan.areEqual(surfaceOrientation.getFactorX(), 0) && DoubleCan.areEqual(surfaceOrientation.getFactorZ(), 0)) {
            return new PipeVector(new com.aquarium.entities.Point(bit.getPipe().getOffset().getX() + world.getSide() / 2,
                    bit.getPipe().getOffset().getZ() + world.getSide() / 2, 0),
                    bit.getPipe().getFactorX(), bit.getPipe().getFactorZ(), 0);
        }
        return new PipeVector(new com.aquarium.entities.Point(bit.getPipe().getOffset().getY() + world.getSide() / 2,
                bit.getPipe().getOffset().getZ() + world.getSide() / 2, 0),
                bit.getPipe().getFactorY(), bit.getPipe().getFactorZ(), 0);
    }
}
