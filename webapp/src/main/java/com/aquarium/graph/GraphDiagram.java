package com.aquarium.graph;

import com.aquarium.entities.Modificator;
import com.aquarium.entities.Observer;
import com.aquarium.entities.Point;
import com.aquarium.entities.support.ParameterValue;
import com.aquarium.entities.support.ParametersHolder;
import com.aquarium.entities.support.WorldHolder;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.awt.image.BufferedImage;

/**
 * Created by artem on 03.01.15.
 */
public class GraphDiagram {

    public  BufferedImage getGraph(int id, String paramName, String type) {

        ParametersHolder holder = getParametersHolder(id);

        XYDataset line_chart_dataset = getDataset(paramName, holder, type);

        JFreeChart lineChartObject = ChartFactory.createXYLineChart("---", "time", paramName, line_chart_dataset, PlotOrientation.VERTICAL, true, true, false);
        return lineChartObject.createBufferedImage(640, 480);
    }

    private  XYDataset getDataset(String paramName, ParametersHolder holder, String type) {
        XYSeriesCollection line_chart_dataset = new XYSeriesCollection();
        final XYSeries series = new XYSeries("Graph");
        for (ParameterValue value : holder.getParameterValues(paramName)) {
            series.add(value.getTime(), getValue(value, type));
        }
        line_chart_dataset.addSeries(series);
        return line_chart_dataset;
    }

    private  Number getValue(ParameterValue value, String type) {
        if(type == null) {
            return ((Number) value.getValue());
        } else if(type.equals("X")){
            return (((Point) value.getValue()).getX());
        } else if(type.equals("Y")){
            return (((Point) value.getValue()).getY());
        } else if(type.equals("Z")){
            return (((Point) value.getValue()).getZ());
        }
        return ((Number) value.getValue());
    }

    protected  ParametersHolder getParametersHolder(int id) {
        ParametersHolder holder = null;
        for (Modificator modificator : WorldHolder.world.getModificators()) {
            if (id == modificator.getId()) {
                holder = modificator;
            }
        }
        for (Observer observer : WorldHolder.world.getObservers()) {
            if (id == observer.getId()) {
                holder = observer;
            }
        }
        return holder;
    }
}
