package com.aquarium.graph;

import com.aquarium.entities.*;
import com.aquarium.entities.Point;
import com.aquarium.entities.support.ObservableParameter;
import com.aquarium.math.DoubleCan;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.Set;

/**
 * Created by artem on 10/13/14.
 */
public class ObserversPlotDiagram {

    static final Logger log = LogManager.getLogger("IncrementalEngine");
    private int xSize;
    private int ySize;
    private double factor;
    private Set<Observer> observers;
    private World world;
    private Point orientation;

    public ObserversPlotDiagram(Point orientation, World world, double factor, Set<Observer> observers) {
        this.xSize = (int) Math.round(world.getSide() * factor);
        this.ySize = (int) Math.round(world.getSide() * factor);
        this.factor = factor;
        this.observers = observers;
        this.world = world;
        this.orientation = orientation;
    }

    public void drawInFile(String type) {
        BufferedImage image = drawImage(type);
        try {
            writeImageInFile(image);
        } catch (Exception e) {
            log.error("!!! Can not save the picture: " + e.getMessage());
        }
    }

    private void writeImageInFile(BufferedImage image) throws Exception {
        ImageIO.write(image, "jpg", new FileOutputStream("observers.jpg"));
    }

    public BufferedImage drawImage(String type) {
        BufferedImage bufferedImage = new BufferedImage(xSize, ySize,
                BufferedImage.TYPE_INT_ARGB);

        //Draw an oval
        Graphics g = bufferedImage.getGraphics();
        g.setColor(Color.blue);
        for (Observer observer : observers) {
            if(observer instanceof SurfaceBody){
                continue;
            }
            Point position =getPointForImage(observer.getPosition(), orientation, world);

            g.fillOval((int) Math.round((position.getX() - observer.getSize() / 2) * factor),
                    (int) Math.round((position.getY() - observer.getSize() / 2) * factor),
                    (int) Math.round(observer.getSize() * factor), (int) Math.round(observer.getSize() * factor));

            Object value = getParameter(observer, type);
            if(value instanceof Point) {
                drawParameterAsVector(g, getPointForImage(observer.getPosition(), orientation, world),
                        getPointForImage((Point) value, orientation, world));
            } else {
                DecimalFormat df = new DecimalFormat("-0000000.00");
                String text = df.format(value);
                drawParameter(g, getPointForImage(observer.getPosition(), orientation, world), text);
            }
        }

        g.dispose();

        return bufferedImage;
    }

    private void drawParameterAsVector(Graphics g, Point observerPosition, Point vector) {

            g.drawLine((int) Math.round(observerPosition.getX() * factor),
                    (int) Math.round(observerPosition.getY() * factor),
                    (int) Math.round((observerPosition.getX()) * factor + vector.getX()),
                    (int) Math.round((observerPosition.getY()) * factor + vector.getY()));
    }

    private Object getParameter(Observer observer, String type) {
        for (ObservableParameter param : observer.getParameters()) {
            if (param.getClass().getSimpleName().equals(type)) {
                return param.getPrevious();
            }
        }
        return null;
    }

    public void drawParameter(Graphics g, Point observerParameter, String text) {
        g.drawString(text,
                (int) Math.round(observerParameter.getX() * factor),
                (int) Math.round(observerParameter.getX() * factor));
    }

    static public Point getPointForImage(Point point, Point surfaceOrientation, World world) {
        if (DoubleCan.areEqual(surfaceOrientation.getX(), 0) && DoubleCan.areEqual(surfaceOrientation.getY(), 0)) {
            return new com.aquarium.entities.Point(point.getX() + world.getSide() / 2,
                    point.getY() + world.getSide() / 2, 0);
        } else if (DoubleCan.areEqual(surfaceOrientation.getX(), 0) && DoubleCan.areEqual(surfaceOrientation.getZ(), 0)) {
            return new com.aquarium.entities.Point(point.getX() + world.getSide() / 2,
                    point.getZ() + world.getSide() / 2, 0);
        }
        return new com.aquarium.entities.Point(point.getY() + world.getSide() / 2,
                point.getZ() + world.getSide() / 2, 0);
    }
}
