package com.aquarium.webapp;

import com.aquarium.webapp.handler.TailResourceHandler;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.webapp.WebAppContext;

import java.io.File;


public class NAServer {

    private Server server;

    public void init() throws Exception {
        server = new Server(8080);

        String app_home =  getResourcesRootFolder();

        WebAppContext webapp = initiateWebAppContext(app_home);

        ContextHandler resContext = initiateResourceHandler(app_home);


        ContextHandlerCollection handlers = new ContextHandlerCollection();
        handlers.setHandlers(new Handler[]
                {webapp, resContext});

        server.setHandler(handlers);

        server.start();
    }

    public void stop() throws Exception{
        server.stop();
    }

    public WebAppContext initiateWebAppContext(String app_home) {
        WebAppContext webapp = new WebAppContext();
        webapp.setContextPath("/");
        webapp.setWar(app_home + "/webapp/target/webapp-1.0-SNAPSHOT.war");
        webapp.setClassLoader(getClass().getClassLoader());
        return webapp;
    }

    public ContextHandler initiateResourceHandler(String app_home) {
        ResourceHandler resource_handler = new TailResourceHandler();
        resource_handler.setDirectoriesListed(true);
        resource_handler.setWelcomeFiles(new String[]{"index.jsp"});
        resource_handler.setResourceBase(app_home);
        resource_handler.setCacheControl("no-cache");
        ContextHandler resContext = new ContextHandler("/res");
        resContext.setHandler(resource_handler);
        return resContext;
    }

    private String getResourcesRootFolder() {
        String currentDir = System.getProperty("user.dir");
        int count = 0;
        while (!currentDir.endsWith("nano-aquarium")) {
            currentDir = new File(currentDir).getParent();
            count++;
            if (currentDir == null || count > 100) {
                throw new RuntimeException("Cannot find Resources Root Folder");
            }
        }
        return currentDir;
    }
}