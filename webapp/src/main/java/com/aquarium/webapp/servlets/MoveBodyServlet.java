package com.aquarium.webapp.servlets;

import com.aquarium.entities.*;
import com.aquarium.entities.support.WorldHolder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by artem on 10/19/14.
 */
public class MoveBodyServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        float x = Float.parseFloat(req.getParameter("x"));
        float y = Float.parseFloat(req.getParameter("y"));
        float z = Float.parseFloat(req.getParameter("z"));
        String type = req.getParameter("type");
        String id = req.getParameter("id");
        boolean isObserver = true;
        Body ob = getObserver(type, id);
        if (ob == null) {
            isObserver = false;
            ob = getModificator(type, id);
        }

        if (ob != null) {
            ob.getPosition().setX(x);
            ob.getPosition().setY(y);
            ob.getPosition().setZ(z);
            if (isObserver) {
                req.getRequestDispatcher("observers.jsp").forward(req, resp);
            } else {
                req.getRequestDispatcher("modifiers.jsp").forward(req, resp);
            }
        } else {
            throw new IllegalStateException("No such body type:" + type + " id:" + id);
        }
    }

    private Body getModificator(String type, String id) {
        for (final Modificator modificator: getWorld().getModificators()) {
            if (modificator.getClass().getSimpleName().equals(type) && Integer.valueOf(id).equals(modificator.getId())) {
                return modificator;
            }
        }
        return null;
    }

    private Body getObserver(String type, String id) {
        for (final Observer observer : getWorld().getObservers()) {
            if (observer.getClass().getSimpleName().equals(type) && Integer.valueOf(id).equals(observer.getId())) {
                return observer;
            }
        }
        return null;
    }

    private World getWorld() {
        return WorldHolder.world;
    }
}
