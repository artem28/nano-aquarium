package com.aquarium.webapp.servlets;

import com.aquarium.entities.support.WorldHolder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by artem on 11/3/14.
 */
public class ReloadWorldServlet extends HttpServlet {

    static final public String CONFIG_FILE = "world.configuration";

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String configuration = req.getParameter("config").trim();
        File tempDir = new File(System.getProperty("java.io.tmpdir"));
        File tempFile = File.createTempFile("worldConfig", ".tmp", tempDir);
        FileWriter fileWriter = new FileWriter(tempFile, true);
        fileWriter.write(configuration);
        fileWriter.close();

        System.setProperty(CONFIG_FILE, tempFile.getPath());

        WorldHolder.world.needToBeRestarted(true);

        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }
}
