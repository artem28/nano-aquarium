package com.aquarium.webapp.servlets;


import com.aquarium.graph.GraphDiagram;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by artem on 02.01.15.
 */
public class ShowDiagramServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("image/jpeg");
        GraphDiagram engine = new GraphDiagram();
        ImageIO.write(engine.getGraph(Integer.valueOf(req.getParameter("id")), req.getParameter("paramName"), req.getParameter("type")),
                "jpg",
                resp.getOutputStream());
    }

}
