package com.aquarium.webapp.servlets;

import com.aquarium.entities.*;
import com.aquarium.entities.support.WorldHolder;
import com.aquarium.graph.DirectionPlotDiagram;
import com.aquarium.math.DoubleCan;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by artem on 10/19/14.
 */
public class SurfaceObserverServlet extends HttpServlet {
    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        SurfaceBody surfaceBody = getSurfaceBody();
        if (surfaceBody == null) {
            res.getWriter().append("No Surface observer");
            return;
        }
        final Set<PipeVector> vectors = getBitPipeVectors(surfaceBody);
        DirectionPlotDiagram canvas = new DirectionPlotDiagram(((Observer) surfaceBody).getId(), getWorld().getSide(), 100, vectors);
        BufferedImage buffer = canvas.drawImage();
        res.setContentType("image/jpeg");
        ImageIO.write(buffer, "jpg", res.getOutputStream());
    }

    public Set<PipeVector> getBitPipeVectors(SurfaceBody surfaceBody) {
        final Set<PipeVector> vectors = new HashSet<PipeVector>(10000);
        int trycount = 0;
        boolean success = false;
        while (!success && trycount < 3) {
            try {
                for (Bit bit : getWorld().getBits().findAcceptable(surfaceBody)) {
                    PipeVector vector = DirectionPlotDiagram.getPipeVectorForImage(bit, surfaceBody.getSurfaceOrientation(), getWorld());
                    vectors.add(vector);
                }
            } catch (ConcurrentModificationException e) {
                log("SERVLET: error " + e.getMessage(), e);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ie) {
                    log("SERVLET: error " + ie.getMessage(), ie);
                    trycount = 1000;
                }
            }
            trycount++;
        }
        return vectors;
    }

    private World getWorld() {
        return WorldHolder.world;
    }

    private SurfaceBody getSurfaceBody() {
        for (final Observer observer : getWorld().getObservers()) {
            if (observer instanceof SurfaceBody) {
                return (SurfaceBody) observer;
            }
        }
        return null;
    }
}
