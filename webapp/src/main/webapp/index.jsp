<%@ page language="java" %>
<html>
<head><title>A Comment Test</title></head>
<body>
<h2>Nano Aquarium</h2>
<ul>
    <li><a href="statistics.jsp">Statistics</a></li>
    <li><a href="observers.jsp">Observers</a></li>
    <li><a href="modifiers.jsp">Modifiers</a></li>
    <li><a href="uploadNewConfig.jsp">Upload New Configuration</a></li>
    <li><a href="/jmx/">JMX</a></li>
</ul>

<p><a href="/res/engine/log/data.log">data.log</a> (just <a href="/res/engine/log/data.log?tail">tail</a>)</p>
<p><a href="/res/engine/log/info.log">info.log</a> (just <a href="/res/engine/log/info.log?tail">tail</a>)</p>
<p><a href="/res/engine/direction_4.jpg">Image</a> (new <a href="/surfimage">image</a>)</p>
</body>
</html>