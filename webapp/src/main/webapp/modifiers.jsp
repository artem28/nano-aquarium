<%@ page import="com.aquarium.entities.support.WorldHolder" %>
<%@ page import="com.aquarium.entities.Observer" %>
<%@ page import="com.aquarium.entities.Modificator" %>
<%--
  Created by IntelliJ IDEA.
  User: artem
  Date: 10/19/14
  Time: 3:37 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Modificators</title>
</head>
<body>
<h2>Nano Aquarium</h2>

<h3>Modifiers</h3>
<p><a href="/">Home</a> </p>

<ul>

    <%
        for (Modificator modificator : WorldHolder.world.getModificators()) {
    %>
    <li>
        <%=modificator.toString()%> [<a
            href="move.jsp?id=<%=modificator.getId()%>&type=<%=modificator.getClass().getSimpleName()%>">move</a>]
            [<a href="parameters.jsp?id=<%=modificator.getId()%>">parameters</a>]
    </li>
    <%
        }
    %>
</ul>

</body>
</html>
