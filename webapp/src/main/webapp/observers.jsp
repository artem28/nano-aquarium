<%@ page import="com.aquarium.entities.support.WorldHolder" %>
<%@ page import="com.aquarium.entities.Observer" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Observers</title>
</head>
<body>
<h2>Nano Aquarium</h2>

<h3>Observers</h3>
<p><a href="/">Home</a> </p>

<ul>

<%
    for (Observer observer : WorldHolder.world.getObservers()) {
%>
        <li>
            <%=observer.toString()%>   [<a href="move.jsp?id=<%=observer.getId()%>&type=<%=observer.getClass().getSimpleName()%>">move</a>]
                        [<a href="parameters.jsp?id=<%=observer.getId()%>">parameters</a>]

        </li>
<%
    }
%>
    </ul>
</body>
</html>
