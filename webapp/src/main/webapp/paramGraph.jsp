<%@ page import="com.aquarium.entities.support.WorldHolder" %>
<%@ page import="com.aquarium.entities.Observer" %>
<%@ page import="com.aquarium.entities.Modificator" %>
<%@ page import="com.aquarium.entities.support.ParametersHolder" %>
<%@ page import="com.aquarium.entities.support.ParameterValue" %>
<%@ page import="com.aquarium.entities.Point" %>
<%@ page import="java.util.List" %>

<%--
  Created by IntelliJ IDEA.
  User: artem
  Date: 10/19/14
  Time: 3:37 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Parameter Graph</title>
</head>
<body>
<h2>Nano Aquarium</h2>



    <%
        ParametersHolder holder = null;
        for (Modificator modificator : WorldHolder.world.getModificators()) {
            if(request.getParameter("id").equals(String.valueOf(modificator.getId()))){
                holder = modificator ;
            }
        }
        for (Observer observer : WorldHolder.world.getObservers()) {
            if(request.getParameter("id").equals(String.valueOf(observer.getId()))){
                holder = observer ;
            }
        }
        String paramName = request.getParameter("paramName");
        List values = holder.getParameterValues(paramName);
        boolean noData = values.size()==0;
        boolean isPoint = false;
        if(!noData ){
            isPoint = ((ParameterValue)values.get(0)).getValue() instanceof Point;
        }
    %>

<h3>Parameter graph for holder <%=request.getParameter("id")%> param name <%=paramName %></h3>
<p><a href="/">Home</a> </p>

<p><a href="#" onclick="window.history.back();">Back</a> </p>


    <%
        if (noData) {
    %>
        <h2>No data</h2>
    <%
        } else if (!isPoint) {
    %>

<img src="graphImg?id=<%=request.getParameter("id")%>&paramName=<%=paramName%>">

    <%
        } else {
    %>
    <p>X</p>
<img src="graphImg?id=<%=request.getParameter("id")%>&paramName=<%=paramName%>&type=X">
    <p>Y</p>
<img src="graphImg?id=<%=request.getParameter("id")%>&paramName=<%=paramName%>&type=Y">
    <p>Z</p>
<img src="graphImg?id=<%=request.getParameter("id")%>&paramName=<%=paramName%>&type=Z">

    <%
        }
    %>

</body>
</html>
