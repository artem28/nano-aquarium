<%@ page import="com.aquarium.entities.support.WorldHolder" %>
<%@ page import="com.aquarium.entities.Observer" %>
<%@ page import="com.aquarium.entities.Modificator" %>
<%@ page import="com.aquarium.entities.support.ParametersHolder" %>
<%--
  Created by IntelliJ IDEA.
  User: artem
  Date: 10/19/14
  Time: 3:37 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Parameters</title>
</head>
<body>
<h2>Nano Aquarium</h2>



    <%
        ParametersHolder holder = null;
        for (Modificator modificator : WorldHolder.world.getModificators()) {
            if(request.getParameter("id").equals(String.valueOf(modificator.getId()))){
                holder = modificator ;
            }
        }
        for (Observer observer : WorldHolder.world.getObservers()) {
            if(request.getParameter("id").equals(String.valueOf(observer.getId()))){
                holder = observer ;
            }
        }

    %>



<h3>Parameters for parameter holder <%=request.getParameter("id")%></h3>
<p><a href="/">Home</a> </p>

<p><a href="#" onclick="window.history.back();">Back</a> </p>

<ul>

    <%
    if( holder.getParametersNames() !=null){
        for (String paramNames : holder.getParametersNames()) {
    %>
    <li>
        <a
            href="paramGraph.jsp?id=<%=request.getParameter("id")%>&paramName=<%=paramNames%>"><%=paramNames%></a>
    </li>
    <%
        }
    } else {
    %>
    <H3>No Parameters</H3>
    <%
    }
    %>
</ul>

</body>
</html>
