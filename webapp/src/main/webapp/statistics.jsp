<%@ page import="com.aquarium.entities.support.WorldHolder" %>
<%--
  Created by IntelliJ IDEA.
  User: artem
  Date: 10/19/14
  Time: 2:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Statistics</title>
</head>
<body>
<h2>Nano Aquarium</h2>
<h3>Statistics</h3>
<p><a href="/">Home</a> </p>

   <p>World side: <%=WorldHolder.world.getSide()%> </p>
   <p>Average bits energy: <%=WorldHolder.world.getAverageBitEnergy()%></p>
   <p>Bits amount: <%=WorldHolder.world.getBitsAmount()%></p>
   <p>Bit size: <%=WorldHolder.world.getBitSize()%></p>
   <p>Time: <%=WorldHolder.world.getTime()%></p>
   <p>Delta time: </p>
   <p>Observers amount: <%=WorldHolder.world.getObservers().size()%></p>
   <p>Modifiers amount: <%=WorldHolder.world.getModificators().size()%></p>
</body>
</html>
