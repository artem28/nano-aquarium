<%@ page import="java.io.FileReader" %>
<%@ page import="com.aquarium.webapp.servlets.ReloadWorldServlet" %>
<%@ page import="java.io.File" %>
<%@ page import="java.util.Scanner" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Upload configuration</title>
</head>
<body>
<p><a href="/">Home</a> </p>

<form action="uploadConfig" method="post">
    <textarea name="config" cols="150" rows="40"><%= new Scanner(new File(System.getProperty(ReloadWorldServlet.CONFIG_FILE))).useDelimiter("\\A").next()%>
    </textarea>
    <p>
        <input type="submit" value="Reload the world"/>
    </p>
</form>
</body>
</html>
