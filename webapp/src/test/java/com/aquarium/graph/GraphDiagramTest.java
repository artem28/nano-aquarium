package com.aquarium.graph;

import com.aquarium.entities.support.ParameterValue;
import com.aquarium.entities.support.ParametersHolder;

import javax.imageio.ImageIO;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;


public class GraphDiagramTest {
    public static void main(String[] args) throws Exception {
        GraphDiagram engine = new GraphDiagram() {
            @Override
            protected ParametersHolder getParametersHolder(int id) {
                return new ParametersHolder() {
                    @Override
                    public List<String> getParametersNames() {
                        return null;
                    }

                    @Override
                    public List<ParameterValue> getParameterValues(String name) {
                        return getData();
                    }
                };
            }
        };

        FileOutputStream file = new FileOutputStream("test.jpg");
        ImageIO.write(engine.getGraph(1, "test", null),
                "jpg",
                file);
        file.close();
    }

    private static List<ParameterValue> getData() {
        List<ParameterValue> result = new ArrayList<ParameterValue>();
        for (float i = 0.0f; i <= 1000; i = i + 0.01f) {
            result.add(new ParameterValue(i, i / 10));
        }
        return result;
    }
}
